# <!-- coding: utf-8 -->
#
# traitements pour l'enquête rapces nocturnes
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
#
# - extraction des carrés du département
# - habillation des cartes des carrés
#
mga  <- function() {
  source("geo/scripts/rapaces.R");
}
#
# le programme principal
# ======================
Drive <- substr( getwd(),1,2)
Drive <- "D:"
print(sprintf("Drive:%s", Drive))
baseDir <- sprintf("%s/web", Drive)
cfgDir <- sprintf("%s/web/geo/RAPACES", Drive)
varDir <- sprintf("%s/web.var/geo/RAPACES", Drive)
varDir <- sprintf("%s/bvi35/CouchesRapaces", Drive)
leafletDir <- sprintf("%s/web.heb/bv/rapaces", Drive)
scan25Dir <- sprintf("%s/bvi35/CouchesIGN/SCAN25", Drive)
texDir <- cfgDir
dir.create(cfgDir, showWarnings = FALSE, recursive = TRUE)
dir.create(varDir, showWarnings = FALSE, recursive = TRUE)
setwd(baseDir)
DEBUG <- FALSE
source("geo/scripts/mga.R")
source("geo/scripts/misc.R")
source("geo/scripts/misc_apivn.R")
source("geo/scripts/misc_biolo.R")
source("geo/scripts/misc_biolo2.R"); # la version site miroir
source("geo/scripts/misc_couches.R")
source("geo/scripts/misc_faune.R")
source("geo/scripts/misc_fonds.R")
source("geo/scripts/misc_ftp.R");
source("geo/scripts/misc_geo.R")
source("geo/scripts/misc_ggplot.R")
source("geo/scripts/misc_json.R"); # les fonctions pour convertir du json
source("geo/scripts/misc_leaflet.R"); # pour affichage dans navigateur local
source("geo/scripts/misc_tex.R")
source("geo/scripts/misc_schedule.R")
source("geo/scripts/rapaces_apivn.R")
source("geo/scripts/rapaces_atlas.R")
source("geo/scripts/rapaces_carres.R")
source("geo/scripts/rapaces_couches.R")
source("geo/scripts/rapaces_faune.R")
source("geo/scripts/rapaces_mouaze.R")
source("geo/scripts/rapaces_rapaces.R")
if ( interactive() ) {
  DEBUG <- TRUE
} else {
  args = commandArgs(trailingOnly=TRUE)
  if (length(args)==0) {
    stop("At least one argument must be supplied (input file).n", call.=FALSE)
  } else if (length(args)==1) {
    print(sprintf("arg:%s", args[1]))
  }
  atlas_jour(force = TRUE)
}
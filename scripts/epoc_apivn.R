# <!-- coding: utf-8 -->
#
# les traitements sur les données en base postgresql
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# source("geo/scripts/epoc.R"); df <- apivn_jour()
apivn_jour <- function(annee = "2023", force = TRUE, force_api = TRUE) {
  library(tidyverse)
  library(lubridate)
  carp()
# pour avoir le tirage de l'année
  odf_2x2km_carres(annee = "2023")
# les observations avec le projet epoc_odf
  df <- apivn_extract_epoc_odf(user = user1, force = force_api) %>%
    mutate(aaaa = strftime(date, format="%Y")) %>%
    filter(aaaa == annee)
# récup des infos des lieudits des données
  apivn_faune_lieudits(df, annee = annee, force = force)
  nc <- apivn_faune_lieudits_lire(annee = annee, force = force) %>%
    glimpse()
  apivn_verif_tex(annee = annee, force = force, force_api = force_api)
}
# source("geo/scripts/epoc.R"); df <- apivn_extract_lire(annee = "2023", force = TRUE, force_api = TRUE) %>% glimpse()
apivn_extract_lire <- function(annee = "2022", force = FALSE, force_api = FALSE) {
  library(tidyverse)
  library(lubridate)
  carp()
  df <- misc_cache_lire(sprintf("apivn_extract_%s", annee))
  if (!is.logical(df) && force == FALSE) {
    return(invisible(df))
  }
# les observations avec le projet epoc_odf
  diviseur <- 10000
  df <- apivn_extract_epoc_odf(user = user1, force = force_api)
#  df0 <- df %>%
#    filter(!grepl("^\\d\\d\\:\\d\\d\\:\\d\\d$", time_start)) %>%
#    glimpse()
#  stop("*****")
  df <- df %>%
# ça ne devrait jamais arrivé
    filter(! is.na(id_form_universal)) %>%
    mutate(aaaa = strftime(date, format="%Y")) %>%
    filter(aaaa == annee) %>%
    mutate(aaaammjj = strftime(date, format="%Y-%m-%d")) %>%
    mutate(heure_debut = hms(time_start)) %>%
    mutate(heure_fin = hms(time_stop)) %>%
    mutate(duree = as.numeric(as.duration(heure_fin - heure_debut))) %>%
    mutate(mm = strftime(date, format="%m")) %>%
    mutate(t_start = sprintf("%s %s", date_start, time_start)) %>%
    mutate(t_start = lubridate::ymd_hms(t_start, tz = "Europe/Paris")) %>%
    mutate(t_stop = sprintf("%s %s", date_stop, time_stop)) %>%
    mutate(t_stop = lubridate::ymd_hms(t_stop, tz = "Europe/Paris")) %>%
    mutate(X = floor(coord_x_local / diviseur), Y = floor(coord_y_local / diviseur)) %>%
    mutate(Carre = sprintf("E%03dN%s", X, Y)) %>%
    rowwise() %>%
    mutate(passage = apivn_passage_date(date))
  observateur.df <- mga_faune_fb_observateurs() %>%
    dplyr::select(universal_id_observer, observateur) %>%
    mutate(universal_id_observer = as.integer(universal_id_observer))
  df1 <- df %>%
    left_join(observateur.df, by = c("observer_uid" = "universal_id_observer"))
#
# détermination du numéro de réplica
  df2 <- df1 %>%
    group_by(id_place, aaaammjj, time_start, id_form) %>%
    summarize(nb = n()) %>%
    group_by(id_place, aaaammjj) %>%
    mutate(replica = row_number()) %>%
    ungroup() %>%
    glimpse()
#
# détermination du nombre de réplicas
  df3 <- df1 %>%
    group_by(id_place, aaaammjj, time_start, id_form) %>%
    summarize(nb = n()) %>%
    group_by(id_place, aaaammjj) %>%
    summarize(replicas = n()) %>%
    ungroup() %>%
    glimpse()
  df5 <- df1 %>%
    left_join(df2, by = c("id_place", "aaaammjj", "time_start", "id_form")) %>%
    left_join(df3, by = c("id_place", "aaaammjj"))
  misc_cache_ecrire(df5, sprintf("apivn_extract_%s", annee))
  return(invisible(df5))
}
#
# quelques statitistiques
# cf scripts/epoc_odf_faune.R
# source("geo/scripts/epoc.R");apivn_stat_tex(annee = "2023", force = FALSE)
apivn_stat_tex <- function(annee = odf_annee, force = TRUE) {
  library(tidyverse)
  carp("début")
  apivn_stat(annee = annee, force = force)
  tpl <- sprintf("%s/apivn_stat_tpl.tex", texDir)
  carp("tpl: %s", tpl)
  tex <- readLines(tpl)
  variables <- list(
    annee = annee
  )
  tex <- misc_list2tpl(variables, tex)
  tpl <- sprintf("%s/apivn_stat_%s.tex", texDir, annee)
  writeLines(tex, tpl)
  tex_pdflatex(sprintf("apivn_stat_%s.tex", annee))
}
#
# source("geo/scripts/epoc.R"); df <- apivn_stat()
apivn_stat <- function(annee = "2023", force = FALSE) {
  carp()
  library(janitor)
  library(tidyverse)
  verif.df <- data.frame()
  dossier <- sprintf("apivn_%s", annee)
  df <- apivn_extract_lire(annee = annee, force = force)
  df <- apivn_recode_precise(df)
#
# les données prises en compte
  df <- df %>%
    filter(! is.na(observateur)) %>%
    filter(grepl("^EPOC\\-ODF_\\d{5,6}_(Officiel|Reserve)$", place)) %>%
    filter(passage != "0") %>%
    filter(replicas == 3)
#
# le nombre d'oiseaux par passage/replica
  df1 <- df %>%
    group_by(passage, replica) %>%
    summarize(nb_oiseaux = sum(count)) %>%
    pivot_wider(names_from = replica, values_from = nb_oiseaux, values_fill = list(nb_oiseaux = 0)) %>%
    adorn_totals(c("col", "row"), fill = "-", na.rm = TRUE, name = "Total")
  tex_df2kable(df1, dossier = dossier, suffixe = "passage_nb_oiseaux")
#
# en pourcentage
  df1 <- df1 %>%
    adorn_percentages("all")
  tex_df2kable(df1, dossier = dossier, suffixe = "passage_pourcentage", digits = 2)
#
# le nombre d'oiseaux par source
  df1 <- df %>%
    group_by(source, precision) %>%
    summarize(nb_oiseaux = sum(count)) %>%
    pivot_wider(names_from = precision, values_from = nb_oiseaux, values_fill = list(nb_oiseaux = 0)) %>%
    adorn_totals(c("col", "row"), fill = "-", na.rm = TRUE, name = "Total")
  tex_df2kable(df1, dossier = dossier, suffixe = "source_nb_oiseaux")
#
# en pourcentage
  df1 <- df1 %>%
    adorn_percentages("all")
  tex_df2kable(df1, dossier = dossier, suffixe = "source_pourcentage", digits = 2)
#
# le nombre d'oiseaux par Distance
  df2 <- df %>%
    filter(! is.na(Distance))
  df1 <- df2 %>%
    group_by(replica, Distance) %>%
    summarize(nb_oiseaux = sum(count)) %>%
    pivot_wider(names_from = Distance, values_from = nb_oiseaux, values_fill = list(nb_oiseaux = 0)) %>%
    adorn_totals(c("col", "row"), fill = "-", na.rm = TRUE, name = "Total") %>%
    adorn_percentages("all") %>%
    glimpse()
  tex_df2kable(df1, dossier = dossier, suffixe = "Distance_nb_oiseaux", digits = 2)
#
# le nombre d'oiseaux par details
  df3 <- df %>%
    filter(! is.na(details)) %>%
    filter(source == "WEB")
  df3 <- apivn_recode_details(df3, mode = "separate") %>%
    glimpse()
  df1 <- df3 %>%
    group_by(replica, distance.y) %>%
    summarize(nb_oiseaux = sum(count.y)) %>%
    pivot_wider(names_from = distance.y, values_from = nb_oiseaux, values_fill = list(nb_oiseaux = 0)) %>%
    adorn_totals(c("col", "row"), fill = "-", na.rm = TRUE, name = "Total") %>%
    adorn_percentages("all") %>%
    glimpse()
  tex_df2kable(df1, dossier = dossier, suffixe = "details_nb_oiseaux", digits = 2)
#
# le nombre d'oiseaux par observateur
  df3 <- df3 %>%
    mutate(Distance = distance.y) %>%
    mutate(count = count.y)
  df4 <- bind_rows(df2, df3)
  df1 <- df4 %>%
    group_by(observateur, Carre, precision, Distance) %>%
    summarize(nb_oiseaux = sum(count)) %>%
    pivot_wider(names_from = Distance, values_from = nb_oiseaux, values_fill = list(nb_oiseaux = 0)) %>%
    adorn_totals(c("col", "row"), fill = "-", na.rm = TRUE, name = "Total") %>%
    glimpse()
  tex_df2kable(df1, dossier = dossier, suffixe = "observateur_nb_oiseaux")
#
# en pourcentage
  df1 <- df1 %>%
    adorn_percentages("row")
  tex_df2kable(df1, dossier = dossier, suffixe = "observateur_pourcentage", digits = 2)
}
apivn_stat_ <- function(force = FALSE) {
  carp("début")
  library(tidyverse)
  df <- misc_lire("apivn_extract_epoc_odf") %>%
    glimpse()
  df1 <- df %>%
    filter(date_year == 2023) %>%
    group_by(observer_uid, id_form_universal, date_start, place, source) %>%
    summarize(nb_donnees = n()) %>%
    ungroup()
  misc_print(df1)
}
#
# les différentes vérifications
# cf scripts/epoc_odf_faune.R
# source("geo/scripts/epoc.R");apivn_verif_tex(annee = "2023", force = FALSE)
apivn_verif_tex <- function(annee = odf_annee, force = TRUE, force_api = TRUE) {
  library(tidyverse)
  carp("début")
  apivn_verif(annee = annee, force = force, force_api = force_api)
  tpl <- sprintf("%s/apivn_verif_tpl.tex", texDir)
  carp("tpl: %s", tpl)
  tex <- readLines(tpl)
  variables <- list(
    annee = annee
  )
  tex <- misc_list2tpl(variables, tex)
  tpl <- sprintf("%s/apivn_verif_%s.tex", texDir, annee)
  writeLines(tex, tpl)
  tex_pdflatex(sprintf("apivn_verif_%s.tex", annee))
}
#
# source("geo/scripts/epoc.R"); df <- apivn_verif()
apivn_verif <- function(annee = "2023", force = FALSE, force_api = FALSE) {
  carp()
  library(suncalc)
  library(lubridate)
  library(tidyverse)
  verif.df <- data.frame()
  dossier <- sprintf("apivn_%s", annee)
  df <- apivn_extract_lire(annee = annee, force = force, force_api = force_api)
  df <- apivn_recode_precise(df)
#
# les observateurs inconnus
  df21 <- df %>%
    filter(is.na(observateur)) %>%
    group_by(id_form, observer_uid) %>%
    summarize(nb = n()) %>%
    mutate(id.form = sprintf("\\href{https://www.faune-bretagne.org/index.php?m_id=1380&fid=%s}{%s}", id_form, id_form)) %>%
    glimpse()
  tex_df2kable(df21, dossier = dossier, suffixe = "observateurs", escape = FALSE)
#
# pour les données en localisation précise, la localisation du point d'écoute
  df31 <- df %>%
    filter(grepl("^EPOC\\-ODF_\\d{5,6}_(Officiel|Reserve)$", place)) %>%
    filter(passage != "0") %>%
    filter(source != "WEB") %>%
    filter(replicas == 3) %>%
    distinct(id_place, place, id_form, form_lon, form_lat, aaaammjj, replica, observateur)
  nc31 <- st_as_sf(df31, coords = c("form_lon", "form_lat"), crs = 4326, remove=FALSE) %>%
    st_transform(2154) %>%
    glimpse()
  df32 <- df31 %>%
    distinct(id_place, place, aaaammjj, observateur) %>%
    glimpse()
  for (i32 in 1:nrow(df32)) {
    nc33 <- nc31 %>%
      filter(id_place == df32[i32, "id_place"]) %>%
      filter(aaaammjj == df32[i32, "aaaammjj"]) %>%
      arrange(replica)
    if (nrow(nc33) != 3) {
      misc_print(nc33)
      confess("nb de forms")
    }
    mx1 <- st_distance(nc33, nc33)
    longueurs <- as.numeric(c(mx1[1, 2], mx1[1, 3], mx1[2, 3]))
    longueur_max <- max(longueurs)
#    glimpse(longueurs)
    df32[i32, "longueur_max"] <- longueur_max
  }
  df34 <- df32 %>%
    filter(longueur_max > 25)
#  misc_print(df34); stop("******")
  tex_df2kable(df34, dossier = dossier, suffixe = "localisation_replicas")
#
# les données hors carrés
  df1 <- df %>%
    filter(! grepl("^EPOC\\-ODF_\\d{5,6}_(Officiel|Reserve)$", place)) %>%
    group_by(observer_uid, observateur, id_place, place, aaaammjj) %>%
    summarize(nb = n())
  misc_print(df1)
  tex_df2kable(df1, dossier = dossier, suffixe = "hors_carres")
  df <- df %>%
    filter(grepl("^EPOC\\-ODF_\\d{5,6}_(Officiel|Reserve)$", place))
#
# les données hors période de passage "nicheurs"
  df2 <- df %>%
    filter(passage == "0") %>%
    group_by(observer_uid, observateur, id_place, place, aaaammjj, passage) %>%
    summarize(nb = n())
  misc_print(df2)
  tex_df2kable(df2, dossier = dossier, suffixe = "periode_passage")
  df3 <- df %>%
    filter(passage != "0")
#
# les données hors horaire de passage
  dates.df <- df %>%
    distinct(id_form, date = date_start, lat = form_lat, lon = form_lon)
#  misc_print(dates.df)
  df4 <- getSunlightTimes(data = dates.df, tz = "MET", keep = c("sunrise")) %>%
    mutate(sun_start = sunrise + hm("0:30")) %>%
    mutate(sun_stop = sunrise + hm("4:00"))
  df5 <- cbind("id_form" = dates.df[, "id_form"], df4)
  df11 <- df3 %>%
    left_join(df5, by = c("id_form" = "id_form", "date" = "date")) %>%
    dplyr::distinct(id_form, observateur, place, aaaammjj, t_start, sun_start, t_stop, sun_stop)
#  glimpse(df11);  misc_print(df11); stop("*****")
  df12 <- df11 %>%
    mutate(delta = as.integer(difftime(t_stop, sun_stop, units = "mins"))) %>%
    dplyr::select(-t_start, -sun_start) %>%
    filter(t_stop > sun_stop)
  tex_df2kable(df12, dossier = dossier, suffixe = "horaire_passage_tard")
  df13 <- df11 %>%
    mutate(delta = as.integer(difftime(t_start, sun_start, units = "mins"))) %>%
    dplyr::select(-t_stop, -sun_stop) %>%
    filter(t_start < sun_start)
  tex_df2kable(df13, dossier = dossier, suffixe = "horaire_passage_tot")
#
# trois formulaires pour un point
  df15 <- df3 %>%
    group_by(observateur, id_place, place, date, id_form, source) %>%
    summarize(nb = n()) %>%
    group_by(observateur, id_place, place, date) %>%
    summarize(nb = n(), forms = paste(id_form, collapse = " ")) %>%
    filter(nb != 3)
  tex_df2kable(df15, dossier = dossier, suffixe = "saisie_formulaire")
# 15 formulaires pour un carré
  df16 <- df3 %>%
    group_by(observateur, Carre, date, id_form, source) %>%
    summarize(nb = n()) %>%
    group_by(observateur, Carre, date) %>%
    summarize(nb = n()) %>%
    filter(nb != 15)
  tex_df2kable(df16, dossier = dossier, suffixe = "saisie_carre_formulaires")
# 5 points pour un carré
  df17 <- df3 %>%
    group_by(observateur, Carre, date, id_place, place) %>%
    summarize(nb = n()) %>%
    group_by(observateur, Carre, date) %>%
    summarize(nb = n()) %>%
    filter(nb != 5)
  tex_df2kable(df17, dossier = dossier, suffixe = "saisie_carre_points")
#
# plusieurs sources pour un formulaire
  df18 <- df3 %>%
    group_by(observateur, Carre, date, id_place, place, id_form, source) %>%
    summarize(nb = n()) %>%
    group_by(observateur, Carre, date, id_place, place, id_form) %>%
    summarize(nb = n()) %>%
    filter(nb != 1)
  tex_df2kable(df18, dossier = dossier, suffixe = "saisie_formulaire_source")
#
# pour les données en localisation précise
  df21 <- df3 %>%
    filter(! is.na(observateur)) %>%
    dplyr::select(id_form, id_sighting, observateur, espece, distance, source) %>%
    mutate(source = escapeLatexSpecials(source)) %>%
    arrange(id_form, id_sighting, observateur, espece, distance) %>%
    mutate(id.form = sprintf("\\href{https://www.faune-bretagne.org/index.php?m_id=1380&fid=%s}{%s}", id_form, id_form))

  df22 <- df21 %>%
    filter(distance == 0)
  tex_df2kable(df22, dossier = dossier, suffixe = "distance_0", escape = FALSE)
  df23 <- df21 %>%
    filter(distance > 500)
  tex_df2kable(df23, dossier = dossier, suffixe = "distance_500", escape = FALSE)

}
#
# les coordonnées des lieudits d'après faune-bretagne
#
apivn_faune_lieudits <- function(df, annee, force = FALSE) {
  carp("début")
  library(tidyverse)
  library(sf)
  nc <- misc_cache_lire("apivn_faune_lieudits")
  if (!is.logical(nc) && force  == FALSE) {
    return(invisible(nc))
  }
#  glimpse(misc_cache.list); stop("#*****")
  df <- df %>%
    filter(grepl("^EPOC\\-ODF", place)) %>%
    distinct(place) %>%
    glimpse()
#  View(df);  stop("****")
  biolo_fb()
  biolo_handle(TRUE)
  df1 <- data.frame()
  for (i in 1:nrow(df)) {
    carp("place: %s grid_name: %s", df[[i, "place"]], df[[i, "grid_name"]])
    df2 <- biolo98_lieudit_get(lieudit = df[[i, "place"]], force = force)
    df1 <- rbind(df1, df2)
  }
  df2 <- df1 %>%
    tidyr::extract(note, c("m_id", "lon", "lat", "id_place"), "m_id=(\\d+).*click=(.*),(.*)&id_place=(\\d+)", remove = FALSE) %>%
    mutate(lon = as.numeric(lon)) %>%
    mutate(lat = as.numeric(lat))
  df3 <- df2 %>%
    filter(is.na(lon))
  if (nrow(df3) > 0) {
    misc_print(df3)
    stop("lon na")
  }
  nc <- st_as_sf(df2, coords = c("lon", "lat"), crs = 4326, remove = FALSE)
  dsn <- sprintf("%s/faune_lieudits_%s.geojson", odfDir, annee)
  carp('dsn: %s nrow: %s', dsn, nrow(nc))
  st_write(nc, dsn, delete_dsn = TRUE, driver = "GeoJSON")
  misc_cache_ecrire(nc, "apivn_faune_lieudits")
  return(invisible(nc))
}
#
# il y a des doublons dans le fichier ???
# source("geo/scripts/epoc.R"); apivn_faune_lieudits_lire(annee = "2023", force = TRUE) %>% glimpse()
apivn_faune_lieudits_lire <- function(annee = "2023", force = FALSE) {
  carp("début")
  library(tidyverse)
  library(sf)
  dsn <- sprintf("%s/faune_lieudits_%s.geojson", odfDir, annee)
  nc1 <- st_read(dsn) %>%
    st_transform(2154)
  carp('dsn: %s nrow: %s', dsn, nrow(nc1))
  nc1 <- nc1 %>%
    distinct() %>%
    glimpse()
  nc2 <- odf_2x2km_carres_lire(annee = annee)
#  glimpse(nc2); stop("=====")
  nc <- nc1 %>%
    st_join(nc2)
  carp("les lieudits en double")
  df1 <- nc %>%
    st_drop_geometry() %>%
    group_by(lieudit) %>%
    summarize(nb = n()) %>%
    filter(nb > 1)
  df2 <- data.frame()
  if (nrow(df1) > 0) {
    carp("lieudits en double")
    df2 <- nc %>%
      st_drop_geometry() %>%
      filter(lieudit %in% df1$lieudit) %>%
      dplyr::select(id_place, lieudit, Name, ID_10km)
    misc_print(df2)
  }
  tex_df2kable(df2)
  df3 <- nc %>%
    st_drop_geometry() %>%
    dplyr::select(id_place, lieudit, Name, ID_10km, commune, maj)
  misc_print(df3)
  return(invisible(nc))
}
#
# détermination du passage en fonction de la date
apivn_passage_date <- function(date) {
  Passage <- "0"
  annee <- lubridate::year(date)
  int1 <- interval(ymd(sprintf("%s-03-01", annee)), ymd(sprintf("%s-03-31", annee)))
  if(date %within% int1) {
    Passage <- "1"
  }
  int2 <- interval(ymd(sprintf("%s-04-01", annee)), ymd(sprintf("%s-05-08", annee)))
  if(date %within% int2) {
    Passage <- "2"
  }
  int3 <- interval(ymd(sprintf("%s-05-09", annee)), ymd(sprintf("%s-06-15", annee)))
  if(date %within% int3) {
    Passage <- "3"
  }
#  carp("date: %s Passage: %s", date, Passage);stop("*****")
  return(invisible(Passage))
}

#
# avec le format json
#
# source("geo/scripts/epoc.R"); mga <- apivn_json_jour()
apivn_json_jour <- function(force = FALSE) {
  library(dbplyr)
  library(tidyverse)
  library(lubridate)
  library(jsonlite)
  carp()
#  apivn_extract_table_observations_json_epoc_odf()
  apivn_json_observations_df()
}
#
# conversion en dataframe
# source("geo/scripts/epoc.R"); mga <- apivn_json_observations_df()
apivn_json_observations_df <- function(force = FALSE) {
  library(tidyverse)
  library(lubridate)
  library(jsonlite)
  carp()
  df <- misc_cache_lire("apivn_json_observations_df")
  if (!is.logical(df)) {
    return(invisible(df))
  }
  df1 <- misc_lire("apivn_extract_table_observations_json_epoc_odf") %>%
    glimpse()
  df <- tibble()
  for (i in 1:nrow(df1)) {
    if (i %% 100 == 0) {
      carp("i: %s/%s", i, nrow(df1))
    }
#  for (i in 1:10000) {
    df2 <- json_json2df(df1[i, "item"])
    df <- bind_rows(df, df2)
#    df <- bind_rows(df, jsonlite::fromJSON(df1[[i, "item"]], simplifyDataFrame = FALSE))
  }
  glimpse(df)
  misc_ecrire(df, "apivn_json_observations_df")
  misc_cache_ecrire(df, "apivn_json_observations_df")
  return(invisible(df))
}
#
# conversion en dataframe - suite
# source("geo/scripts/epoc.R"); mga <- apivn_json_observations_df2()
# mga %>% filter(observers.source != "WEB") %>% glimpse()
apivn_json_observations_df2 <- function(force = FALSE) {
  library(tidyverse)
  library(lubridate)
  library(jsonlite)
  carp()
  df1 <- misc_lire("apivn_json_observations_df") %>%
#    top_n(10) %>%
    glimpse()
  df <- df1$observers.timing %>%
    clean_names() %>%
    mutate(date = as_datetime(as.numeric(timestamp) + as.numeric(offset) )) %>%
    rename_with( ~ paste0("observers.timing.", .x)) %>%
    glimpse()
  df <- cbind(df1, df)
  df$detail <- ""
  for (i in 1:nrow(df)) {
    if (i %% 100 == 0) {
      carp("i: %s/%s", i, nrow(df))
    }
    df2 <- bind_rows(df$observers.details[i])
    if (nrow(df2) == 0) {
      next
    }
#    glimpse(df[i, ])
    if ( "distance" %in% colnames(df2)) {
     df2 <- df2 %>%
       mutate(distance = dplyr::recode(distance,
         "LESS25" = "<25m",
         "LESS100" = "<100m",
         "LESS200" = "<200m",
         "MORE200" = ">=200m"
       )) %>%
       mutate(detail = sprintf("%s%s %s", count, sex, distance))
    } else {
      df2 <- df2 %>%
        mutate(detail = sprintf("%s%s", count, sex))
    }
    detail <- paste(df2$detail, collapse = ", ")
#    carp("detail: %s",detail)
    df[i, "detail"] <- detail
#    misc_print(df2);stop("****")
  }
  glimpse(df)
  misc_ecrire(df, "apivn_json_observations_df2")
  df2 <- df %>%
    filter(observers.precision == "precise") %>%
    dplyr::select(observers.id_sighting, place.lat, place.lon, observers.coord_lat, observers.coord_lon) %>%
    mutate(across(matches("l..$"), as.numeric)) %>%
    glimpse()
  nc2 <- st_as_sf(df2, coords = c("place.lon", "place.lat"), crs = 4326, remove = FALSE)
  nc3 <- st_as_sf(df2, coords = c("observers.coord_lon", "observers.coord_lat"), crs = 4326, remove = FALSE)
  nc2$place <- nc3$geometry
  nc2$distance <- as.integer(st_distance(nc2$geometry, nc2$place, by_element = TRUE))
  glimpse(nc2)
  return(invisible(df))
}
#
## production des pdf avec les données et les cartes
#
# source("geo/scripts/epoc.R");apivn_cartes_lieudits_observateurs()
apivn_cartes_lieudits_observateurs <- function(annee = "2023", test = 2, force = FALSE) {
  library(sf)
  carp()
#  odf_drive_tex_get()
  especes.df <- drive_codes_lire("especes")
  df <- apivn_extract_lire(annee, force = force)
#  carp("passage en spatial")
#  nc <- st_as_sf(df, coords = c("coord_lon", "coord_lat"), crs = 4326, remove=FALSE) %>%
#    st_transform(2154)
  df1 <- df %>%
#    filter(grepl("Maout", observateur)) %>%
    filter(source != "WEB") %>%
    glimpse() %>%
    left_join(especes.df) %>%
    mutate(label = code)
  if (nrow(df1) == 0) {
    confess("***** pas de données")
  }
  df1 <- apivn_recode_details(df1) %>%
    replace_na(list(Details = "")) %>%
    glimpse()
#  stop("****")
  df <- df1 %>%
#    st_drop_geometry() %>%
    group_by(observer_uid) %>%
    summarize(nb = n()) %>%
    glimpse()
  for (i in 1:nrow(df)) {
    df2 <- df1 %>%
      filter(observer_uid == df[[i, "observer_uid"]])
    if (nrow(df2) == 0) {
      glimpse(df[i, ])
      stop("******")
    }
    apivn_cartes_lieudits(df2, test = test)
  }
}
#
apivn_cartes_lieudits <- function(nc, test = 2, test_ogc = 2, force = FALSE, type  = "apivn") {
  observateur <- nc[[1, "observateur"]]
  annee <- nc[[1, "aaaa"]]
  obs <- sprintf("_%s", misc_fichier(observateur))
  cartesDir <- sprintf("%s/%s", texDir, type)
  dir.create(cartesDir, showWarnings = FALSE)
  texFic <- sprintf("%s/%s.tex", cartesDir, obs)
  carp("texFic: %s", texFic)
  TEX <- file(texFic)
  tex <- "% <!-- coding: utf-8 -->"

  carp("annee: %s observateur: %s", annee, observateur)
  df <- nc %>%
    st_drop_geometry() %>%
    group_by(aaaa, date_start, observateur, place, time_start, time_stop, id_form) %>%
    summarize(nb = n()) %>%
    mutate(place = tex_regex_escape(place)) %>%
    mutate(session = sprintf("%s %s", place, date_start)) %>%
    mutate(id.form = sprintf("\\\\href{http://www.faune-bretagne.org/index.php?m_id=1380&fid=%s}{%s}", id_form, id_form)) %>%
    glimpse()
  nc <- nc %>%
    mutate(id_sighting = sprintf("\\href{http://www.faune-bretagne.org/index.php?m_id=54&id=%s}{fb}", id_sighting)) %>%
    glimpse()
#  misc_print(df)
  tex <- list()
  template_session <- "\\subsection{{{place}} {{date_start}}}"
  template_form <- "\\subsubsection{{{place}} {{date_start}} {{time_start}} {{time_stop}} {{id.form}} nb:{{nb}}}"
  old_session <- ""
  session.df <- data.frame()
  for (i in 1:nrow(df)) {
    session <- df[[i ,"session"]]
    if (session != old_session) {
      if (nrow(session.df) > 0) {
        df2 <- session.df %>%
          dplyr::select(espece, time_start, Details) %>%
          arrange(espece, time_start)
#        misc_print(df2)
        df2 <- df2 %>%
          pivot_wider(names_from = time_start, values_from = Details, values_fill = list(Details = "")) %>%
          select(order(colnames(.))) %>%
          relocate(espece)
#        misc_print(df2)
        form <- tex_df2kable(df2)
        tex <- append(tex, form)
#        stop("******")
      }
      form <- tex_df2tpl(df, i, template_session)
      tex <- append(tex, form)
      session.df <- data.frame()
    }
    old_session <- session
    form <- tex_df2tpl(df, i, template_form)
    tex <- append(tex, form)
    df1 <- nc %>%
      st_drop_geometry() %>%
      filter(id_form == df[[i, "id_form"]]) %>%
      arrange(espece)
    session.df <- rbind(session.df, df1)
    for (i1 in 1:nrow(df1)) {
      details <- ""
      if (! is.na(df1[i1, "Details"])) {
        details <- sprintf(" (%s)", df1[i1, "Details"])
      }
      form <- sprintf("%s %s %s%s", df1[i1, "id_sighting"], df1[i1, "count"], df1[i1, "espece"], details)
      tex <- append(tex, form)
    }
  }
  if (nrow(session.df) > 0) {
    df2 <- session.df %>%
      dplyr::select(espece, time_start, Details) %>%
      arrange(espece, time_start) %>%
      pivot_wider(names_from = time_start, values_from = Details, values_fill = list(Details = "")) %>%
      select(order(colnames(.))) %>%
      relocate(espece) %>%
      glimpse()
    form <- tex_df2kable(df2)
    tex <- append(tex, form)
  }
  txt <- paste(tex,  collapse = "\n")
  write(txt, file = TEX, append = FALSE)
  close(TEX)
  carp("texFic: %s", texFic)
#
# le template tex
  dsn <- sprintf("%s/apivn_cartes_lieudits_%s_tpl.tex", texDir, type)
  template <- readLines(dsn)
  variables <- list(
    obs = obs
  )
  tex <- misc_list2tpl(variables, template)
  fn <- sprintf("apivn_cartes_lieudits_%s%s.tex", type, obs)
  texFic <- sprintf("%s/%s", texDir, fn)
  TEX <- file(texFic)
  write(tex, file = TEX, append = FALSE)
  close(TEX)
  tex_pdflatex(fn)
}
#
## bug sur la localisation des replicas
#
# EPOC-ODF_108580_Reserve
# id_form 122563 122564 122565
# source("geo/scripts/epoc.R");apivn_bug()
apivn_bug <- function() {
  carp()
  library(tidyverse)
  id_forms <- c(122563, 122564, 122565)
  id_forms <- c(122563)
  df1 <- data.frame()
  for (id_form in id_forms) {
    df2 <- apivn_extract_table_observations_json_form(id = id_form)
    df1 <- rbind(df1, df2)
  }
  df3 <- data.frame()
  for (i1 in 1:nrow(df1)) {
    df4 <- jsonlite::fromJSON(df1[i1, "item"])$observers
    df3 <- bind_rows(df3, df4)
  }
  df5 <- df3 %>%
    dplyr::select(id_form, coord_x_local, coord_y_local, id_sighting)
  misc_print(df5)

}
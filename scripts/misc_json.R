# <!-- coding: utf-8 -->
#
# quelques fonctions pour le json
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
# fichier obtenu par export json
# source("geo/scripts/rapaces.R"); j <- json_test1()
json_test1 <- function() {
  library(tidyverse)
  library(jsonlite)
  page <- 1
  dsn <- sprintf("%s/export_13921_70867_26022025_071653.json", logBioloDir)
  df1 <- faune_export_lire_json_obs(dsn) %>%
    glimpse()
}
# fichier obtenu en consultant une page web
# source("geo/scripts/rapaces.R"); j <- json_test2()
json_test2 <- function() {
  library(tidyverse)
  library(jsonlite)
  page <- 1
  dsn <- sprintf("%s/misc_biolo2_test_%s.json", logBioloDir, page)
  j <<- read_json(dsn)
  df1 <- json_json2df_bzh(j) %>%
    glimpse()
}
#
# conversion en dataframe
# toutes les listes sont converties
json_json2df <- function(json) {
  df1 <<- json %>% as_tibble()
  df3 <- data.frame()
  df2 <<- sapply(df1, class) %>%
    as.data.frame() %>%
    rename(class = 1) %>%
    rownames_to_column(var = "col") %>%
    glimpse()
  df5 <- tibble()
  for (i2 in 1:nrow(df2)) {
    carp("class i2:%s %s %s", i2, df2[[i2, "col"]], df2[[i2, "class"]])
    if (df2[[i2, "class"]] != "list") {
      next
    }
    df4 <- tibble()
    for (i1 in 1:nrow(df1)) {
      df3 <- df1[i1, i2] %>% unlist()
      df4 <- bind_rows(df4, df3)
    }
    if (nrow(df5) == 0) {
      df5 <- df4
    } else {
      df5 <- bind_cols(df5, df4)
    }
#    glimpse(df4)
  }
  glimpse(df5)
  return(invisible(df5))
}
#
# conversion en dataframe
# que certaines listes
json_json2df_export <- function(j) {
  if (length(j$data) == 0) {
    stop("******")
  }
#
# peut comporter plusieurs listes
  forms <<- j$data$forms
  sightings <<- j$data$sightings
  data <- tibble(data = sightings)
  df1 <<- data %>%
    unnest_wider(data)
  stop("****")
  misc_df2hoist(df1)
  df2 <- df1 %>%
    glimpse()
  return(invisible(df2))
}
#
# conversion en dataframe
# que certaines listes
json_json2df_web <- function(j) {
  if (length(j$data) == 0) {
    stop("******")
  }
  data <- tibble(data = j$data)
  df1 <- data %>%
    unnest_wider(data)
#  misc_df2hoist(df1)
  df2 <- df1 %>%
    hoist(listTop, listTop_title = "title") |>
#    select(-listTop) |>
    hoist(listSubmenu, listSubmenu.title = "title") |>
    hoist(listSubmenu, listSubmenu.href = "href") |>
    hoist(listSubmenu, listSubmenu.href_ajax = "href_ajax") |>
#    select(-listSubmenu) |>
    hoist(species_array, species_array.id = "id") |>
    hoist(species_array, species_array.name = "name") |>
    hoist(species_array, species_array.name_plur = "name_plur") |>
    hoist(species_array, species_array.latin_name = "latin_name") |>
    hoist(species_array, species_array.atlas_start = "atlas_start") |>
    hoist(species_array, species_array.atlas_end = "atlas_end") |>
    hoist(species_array, species_array.category = "category") |>
    hoist(species_array, species_array.taxo_group = "taxo_group") |>
    hoist(species_array, species_array.sys_order = "sys_order") |>
    hoist(species_array, species_array.rarity = "rarity") |>
    separate_wider_regex(listSubmenu.href, c(".*id=", id = "\\d+$"), cols_remove = FALSE) %>%
    glimpse()
  return(invisible(df2))
}
# https://github.com/TheDigitalCatOnline/thedigitalcatonline.github.com/issues/15
json_flatten <- function(x) {
  if (!inherits(x, "list")) return(list(x))
  else return(unlist(c(lapply(x, json_flatten)), recursive = FALSE))
}
json_col_flatten <- function(df, colonne, ligne = 0) {
  if(colonne %in% colnames(df)) {
    if (ligne == 0) {
      liste <- df[, colonne]
    } else {
      liste <- df[, colonne][[ligne]]
    }
    carp("liste");glimpse(liste);liste <<- liste;stop("****")
    df3 <- as_tibble(liste)
    df3 <- df3 %>%
      rename_with( ~ paste(colonne, .x, sep = "."))
    df <- cbind(df, df3)
  }
  return(invisible(df))
}
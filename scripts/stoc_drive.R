# <!-- coding: utf-8 -->
#
# quelques fonctions pour le protocole STOC
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# la partie Google Drive
# ===============================================================
# https://googledrive.tidyverse.org/

#
# une fenêtre firefox doit s"ouvrir !
# source("geo/scripts/stoc.R"); drive_jour_get()
drive_jour_get <- function(test=1) {
  carp()
  library(googledrive)
  drive_auth("univasso35@gmail.com")
  drive_fichier_get()
  drive_tex_get()
}

drive_acp_upload <- function(test = 1) {
  carp()
  library(googledrive)
  drive_auth("univasso35@gmail.com")
  drive_fichier_upload("acp.pdf")
}
#
# source("geo/scripts/stoc.R"); drive_fichier_get()
drive_fichier_get <- function(fichier = "observateurs_mga.xlsx", test = 1) {
  carp()
  dsn <- sprintf("%s/%s", cfgDir, fichier)
  drive_download(
    fichier,
    path = dsn,
    overwrite = TRUE
  )
}
#
# source("geo/scripts/stoc.R"); drive_fichier_upload()
drive_fichier_upload <- function(fichier = "matthieu_fiche_kilometrique.xlsx", dossier = "MarcGauthier", test = 1) {
  carp()
  library(tidyverse)
  dsn <- sprintf("%s/%s", texDir, fichier)
  path <- sprintf("~/STOC/%s/%s", dossier, fichier)
  drive_upload(
    dsn,
    path = path,
    overwrite = TRUE
  )
}
#
# source("geo/scripts/stoc.R"); drive_tex_get()
drive_tex_get <- function(path = "STOC", test = 1) {
  carp()
  library(tidyverse)
  files <- drive_ls(path = path, pattern = ".tex", recursive = TRUE) %>%
    glimpse()
  for(i in 1:nrow(files)) {
    file <- files[i, "name"]
    carp("file: %s", file)
    fichier <- gsub("^.*\\- ", "", file)
    fichier <- gsub(".tex$", ".pdf", fichier)
    carp("fichier: %s", fichier)
    dsn <- sprintf("%s/%s", cfgDir, fichier)
    drive_download(
      files[i, ],
      path = dsn,
      overwrite = TRUE
    )
  }
}

# <!-- coding: utf-8 -->
#
# traitements pour les chevêches du 35
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
#
# le gpx : format des données saisies sur un GPS
#
# je prospectais au début avec un GPS Garmin Oregon 400t pour enregistrer la trace
# les données étaient en format GPX
# j'ai conservé ce format pour le site internet, la biblothèque OpenLayers le lisant
# j'ai ensuite utilisé une application GPS sur téléphone mobile qui utilisait aussi ce format GPX
# lors du passage avec l'application Naturalist, j'ai conservé de format
#
# ce format GPX n'est que lu par Qgis, pas de possibilité d"édition
#
# de 2009 à 2018, deux fichiers par sortie
#
# source("geo/scripts/cheveche35.R"); gpx_jour()
#
# lecture avec sf
gpx_lire <- function(file) {
  library(sf)
  carp("file: %s", file)
  nc <- st_read(file, layer = "waypoints", stringsAsFactors = FALSE, quiet = TRUE) %>%
    mutate(dsn = file) %>%
    dplyr::select(name, sym, desc, time, dsn)
}
#
# conversion d'un dataframe en format gpx "chevêche"
# source("geo/scripts/cheveche35.R"); gpx_df2gpx()
gpx_df2gpx <- function(df, dsn = FALSE) {
  library(sf)
  library(stringr)
  library(lubridate)
  carp()
  if (!"lat" %in% colnames(df)) {
    df <- df %>%
      mutate(lon = st_coordinates(.)[,'X']) %>%
      mutate(lat = st_coordinates(.)[,'Y']) %>%
      mutate(lon = sprintf("%0.6f", lon)) %>%
      mutate(lat = sprintf("%0.6f", lat))
  }
  if (!"time" %in% colnames(df)) {
    df$time <- NA
  }
  fmt_wpt <- '  <wpt lat="%s" lon="%s"><time>%s</time><name>%s</name><desc>%s</desc><sym>%s</sym></wpt>'
  fmt_reponse <- '  <wpt lat="%s" lon="%s"><name>%s</name><sym>%s</sym></wpt>'

  df <- df %>%
    mutate(wpt = ifelse(is.na(time),
      sprintf(fmt_reponse, lat, lon, name, sym),
      sprintf(fmt_wpt, lat, lon, time, name, desc, sym))
    ) %>%
    glimpse()
  if (!is.logical(dsn)) {
    gpx_ecrire(df$wpt, dsn)
  }
  return(invisible(df$wpt))
}
#
# conversion d'un dataframe en format geojson "chevêche"
# source("geo/scripts/cheveche35.R"); gpx_df2gpx()
gpx_df2geojson <- function(df, dsn = FALSE) {
  library(sf)
  library(stringr)
  library(lubridate)
  carp()
  nc <- st_as_sf(df, coords = c("lon", "lat"), crs = 4326, remove = TRUE)
  st_write(nc, dsn, delete_dsn = TRUE, driver = 'GeoJSON')
}
#
# conversion d'un geojson en format gpx chevêche
# source("geo/scripts/cheveche35.R"); gpx_sf2gpx(dsn = "D:/web.heb/bv/gml/cheveche2019.qgis/mga-190128.geojson")
# source("geo/scripts/cheveche35.R"); gpx_sf2gpx(dsn = "D:/web.heb/bv/cheveche/cheveche.geojson")
gpx_sf2gpx <- function(dsn) {
  library(sf)
  library(stringr)
  library(lubridate)
  carp()
  nc1 <- st_read(dsn, stringsAsFactors = FALSE, quiet = TRUE)
  dsn <- gsub("\\.geojson$", ".gpx", dsn)
  gpx_df2gpx(nc1, dsn)
}
# source("geo/scripts/cheveche35.R"); gpx_dir2gpx(dir = "D:/web.heb/bv/gml/cheveche2019.qgis")
gpx_dir2gpx <- function(dir) {
  library(sf)
  library(stringr)
  library(lubridate)
  files <- list.files(dir, pattern = ".geojson$", full.names = TRUE, ignore.case = FALSE, recursive = FALSE) %>%
    glimpse()
  for (file in files) {
    carp("file: %s", file)
    gpx_sf2gpx(dsn = file)
  }
}
# source("geo/scripts/cheveche35.R"); gpx_dir_valid(dir = "D:/web.heb/bv/gml/cheveche2019.qgis")
# source("geo/scripts/cheveche35.R"); gpx_dir_valid(dir = "D:/web.heb/bv/gml/cheveche2024")
gpx_dir_valid <- function(dir) {
  library(sf)
  library(stringr)
  library(lubridate)
  files <- list.files(dir, pattern = "marc.?\\d{6}.gpx$", full.names = TRUE, ignore.case = FALSE, recursive = FALSE) %>%
    glimpse()
  nc2 <- data.frame()
  for (file in files) {
    carp("file: %s", file)
    nc1 <- gpx_lire(file = file)
    nc2 <- rbind(nc2, nc1)
  }
  gpx_valid(nc2)
  gpx_stat(nc2)
  return(invisible(nc2))
}
# source("geo/scripts/cheveche35.R"); gpx_stat()
gpx_stat <- function(nc1) {
  library(tidyverse)
  library(sf)
  library(stringi)
  carp()
  df1 <- nc1 %>%
    st_drop_geometry() %>%
    tidyr::extract(dsn, c("aammjj"), "(\\d{6})\\.gpx$", remove = FALSE) %>%
    filter(!grepl("(Grey)", sym)) %>%
    glimpse()
  df0 <- df1 %>%
    filter(!grepl("(Blue|Green|Orange)", sym)) %>%
    glimpse()
  if (nrow(df0) > 0) {
    stop("****")
  }
  df3 <- df1 %>%
    filter(grepl("(Blue|Green)", sym)) %>%
    group_by(aammjj) %>%
    summarize(repasse = n())
  df4 <- df1 %>%
    filter(grepl("(Orange)", sym)) %>%
    group_by(aammjj) %>%
    summarize(reponse = n())
  df5 <- df3 %>%
    left_join(df4, by = c("aammjj"))
  misc_print(df5)
}
#
# validation en format spatial
# source("geo/scripts/cheveche35.R"); gpx_valid_coord()
gpx_valid_coord <- function(nc1) {
  library(sf)
  library(stringr)
  library(lubridate)
  carp()
  nc1 <- misc_lire("prospection_annees")
  df1 <- nc1 %>%
    mutate(lon = st_coordinates(.)[,'X']) %>%
    mutate(lat = st_coordinates(.)[,'Y']) %>%
    st_drop_geometry()
  df2 <- df1 %>%
    filter(lon <  -1.628423)
  misc_print(df2)
}
#
# test de la validation
# source("geo/scripts/cheveche35.R"); gpx_valid_test()
gpx_valid_test <- function(url = "cheveche2017/marc170218.gpx") {
  dsn <- sprintf("%s/%s", gmlDir, url)
  carp("dsn: %s", dsn)
  nc1 <- gpx_lire(file = dsn) %>%
    arrange(name)
  gpx_valid(nc1)
  glimpse(nc1)
}

#
# validation en format spatial
gpx_valid <- function(nc1) {
  library(sf)
  library(stringr)
  library(stringi)
  library(lubridate)
  carp()
  flags <- c("Flag, Blue", "Flag, Green", "Flag, Orange")
  nc1 <- nc1 %>%
    filter(! grepl("^Flag, (Grey)", sym))  # les autres espèces
  df1 <- nc1 %>%
    st_drop_geometry()
  df2 <- df1 %>%
    filter(! grepl("^[A-Za-h]", name))
  if (nrow(df2) > 0) {
    misc_print(df2)
    stop("nom du point")
  }
# les flags possibles
  df3 <- df1 %>%
    filter(! grepl("^Flag, (Blue|Orange|Green|Grey)$", sym))
  if (nrow(df3) > 0) {
    misc_print(df3)
    stop("flag")
  }
# les flags possibles pour le point de repasse
  df4 <- df1 %>%
    filter(! grepl("^Flag, (Blue|Green)$", sym)) %>%
    filter(grepl("^[A-Za-h]\\d\\d$", name))
  if (nrow(df4) > 0) {
    misc_print(df4)
    stop("flag point de repasse")
  }
# les flags possibles pour les contacts
  df5 <- df1 %>%
    filter(! grepl("^Flag, Orange$", sym)) %>%
    filter(grepl("^[A-Za-h]\\d\\d[a-z]$", name))
  if (nrow(df5) > 0) {
    misc_print(df5)
    stop("flag contact Orange")
  }
# le format du name
  df6 <- df1 %>%
    filter(! grepl("^[A-Za-h]\\d\\d[a-z]?$", name))
  if (nrow(df6) > 0) {
    misc_print(df6)
    stop("format name")
  }
# répartition entre point de repasse et réponse/contact
  df1 <- df1 %>%
    tidyr::extract(dsn, c("aammjj"), "(\\d{6})\\.gpx$", remove = FALSE) %>%
    mutate(point = sprintf("%s_%s", aammjj, name)) %>%
    mutate(repasse = sprintf("%s_%s", aammjj, stri_sub(name, 1, 3)))
  repasses.df <- df1 %>%
    filter(grepl("^[A-Za-h]\\d\\d$", name))
  contacts.df <- df1 %>%
    filter(! grepl("^[A-Za-h]\\d\\d$", name))
# les contacts sans repasse
  df11 <- contacts.df %>%
    filter(repasse %notin% repasses.df$repasse)
  if (nrow(df11) > 0) {
    misc_print(df11)
    stop("les contacts sans repasse")
  }
#
# les points en double
  df12 <- df1 %>%
    filter(duplicated(cbind(point)))
  df13 <- df1 %>%
    filter(point %in% df12$point) %>%
    arrange(point)
# données identiques ?
  df14 <- df13 %>%
    filter(duplicated(cbind(point, name, sym))) %>%
    arrange(point)
  df15 <- df13 %>%
    filter(point %notin% df14$point) %>%
    arrange(point)
  if (nrow(df15) > 0) {
    misc_print(df15)
    stop("points en double")
  }
#
# vérification de la couleur du point de repasse
  df21 <- df1 %>%
    group_by(repasse) %>%
    summarize(nb = n())
  df22 <- df1 %>%
    filter(repasse == point) %>%
    left_join(df21, by = c("repasse"))
  df23 <- df22 %>%
    filter(sym == "Flag, Blue") %>%
    filter(nb != 1)
  if (nrow(df23) > 0) {
    misc_print(df23)
    stop("repasse avec contact")
  }
  df24 <- df22 %>%
    filter(sym != "Flag, Blue") %>%
    filter(nb == 1)
  if (nrow(df24) > 0) {
    misc_print(df24)
    stop("repasse sans contact")
  }
#
# vérifications spatiales
  # répartition entre point de repasse et réponse/contact
  nc1 <- nc1 %>%
    mutate(aammjj = gsub(".gpx$", "", dsn)) %>%
    mutate(aammjj = gsub("^.*.\\D+\\-?", "", aammjj)) %>%
    mutate(point = sprintf("%s_%s", aammjj, name)) %>%
    mutate(repasse = sprintf("%s_%s", aammjj, stri_sub(name, 1, 3)))
#  stop("*****")
  repasses.sf <- nc1 %>%
    filter(grepl("^[A-Za-h]\\d\\d$", name))
  contacts.sf <- nc1 %>%
    filter(! grepl("^[A-Za-h]\\d\\d$", name))
  repasses <- sort(unique(nc1$repasse))
  for (repasse in repasses) {
    carp("repasse: %s", repasse)
    r.sf <- repasses.sf %>%
      filter(repasse == !!repasse)
    c.sf <- contacts.sf %>%
      filter(repasse == !!repasse)
    if (nrow(c.sf) == 0) {
      next
    }
    nc2 <- st_proches(c.sf, r.sf)
    df2 <- nc2 %>%
      st_drop_geometry() %>%
      filter(as.numeric(dist) > 1200)
    if (nrow(df2) > 0) {
      df2 <- df2 %>%
        dplyr::select(name, name.1, dsn, dist)
      misc_print(df2)
      stop("distance repasse contact")
    }
  }
}
# lecture du gpx en mode texte
# source("geo/scripts/cheveche35.R"); gpx_lire_txt()
gpx_lire_txt <- function(url = "cheveche2017/marc170218.gpx", debug = FALSE) {
  library(tidyverse)
  dsn <- sprintf("%s/%s", gmlDir, url)
  dsn <- url
  carp("dsn: %s", dsn)
  l <- read_lines(dsn)
  if ( debug) {
    writeLines(l)
  }
  gpx.df <- tibble(ligne = l) %>%
    rowid_to_column("index")
#   <wpt lat="48.111490" lon="-1.584866"><time>2019-01-28T18:49:29.835Z</time><name>A01</name><desc>desc</desc><sym>Flag, Blue</sym></wpt>
  re_wpt <- 'lat="([^"]+)".*lon="([^"]+)".*<time>(.*)</time><name>(.*)</name><desc>(.*)</desc><sym>(.*)</sym>'
# sans time et desc
  re_reponse <- 'lat="([^"]+)".*lon="([^"]+)".*<name>(.*)</name><sym>(.*)</sym>'
  df1 <- tibble(
    lat = character(),
    lon = character(),
    time = character(),
    name = character(),
    desc = character(),
    sym = character()
  )
  for (ig in 1:nrow(gpx.df)) {
    ligne <- gpx.df[[ig, "ligne"]]
    if (debug) {
      Carp("ig: %s ligne: %s", ig, ligne)
    }
    if (grepl(re_wpt, ligne)) {
#        Carp("wpt: %s", ligne)
      champs <- stri_match_all_regex(ligne, re_wpt)[[1]]
      lat <- champs[2]
      lon <- champs[3]
      time <- champs[4]
      name <- champs[5]
      desc <- champs[6]
      sym <- champs[7]
      df1 <- add_row(df1,
        lat = lat,
        lon = lon,
        time = time,
        name = name,
        desc = desc,
        sym = sym
      )
      next
    }
    if (grepl(re_reponse, ligne)) {
#        Carp("wpt: %s", ligne)
      champs <- stri_match_all_regex(ligne, re_reponse)[[1]]
      lat <- champs[2]
      lon <- champs[3]
      name <- champs[4]
      sym <- champs[5]
       df1 <- add_row(df1,
        lat = lat,
        lon = lon,
        name = name,
        sym = sym
      )
      next
    }
    if (grepl("<wpt", ligne)) {
      confess("format wpt: %s", ligne)
    }
  }
#  misc_print(df1)
#  dsn <- sprintf("%s.gpx", dsn)
#  gpx_df2gpx(df1, dsn)
  return(invisible(df1))
}
# source("geo/scripts/cheveche35.R"); gpx_browse()
gpx_browse <- function(dsn = "cheveche2017/mga1700209.gpx") {
  url <- "http://localhost/cheveche/cheveche.html?z=%s"
  url <- sprintf(url, dsn)
  browseURL(url)
}
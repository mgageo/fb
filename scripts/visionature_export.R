# <!-- coding: utf-8 -->
#
# quelques fonctions pour VisioNature
# auteur: Marc Gauthier
#
# comparaison des exports entre faune-bretagne et faune-france
#  rm(list=ls());  source("geo/scripts/visionature.R");export_fb_ff()

export_fb_ff <- function() {
  library(tidyverse)
  library(readxl)
  library(janitor)
  carp()
  fb_dsn <- sprintf("%s/export_63_56185_03012024_100036.xlsx", varDir)
  ff_dsn <- sprintf("%s/export_14025_50385_03012024_095929.xlsx", varDir)
  fb.df <- read_excel(fb_dsn, skip = 1) %>%
    clean_names() %>%
    glimpse()
  ff.df <- read_excel(ff_dsn, skip = 1) %>%
    clean_names() %>%
    glimpse()
  fb1 <- fb.df %>%
    group_by(date) %>%
    summarize(nb = n())
  ff1 <- ff.df %>%
    group_by(date) %>%
    summarize(nb = n())
  df1 <- fb1 %>%
    full_join(ff1, by = c("date"), suffix = c(".fb", ".ff")) %>%
    replace_na(list(nb.fb = 0, nb.ff = 0)) %>%
    glimpse() %>%
    filter(nb.fb != nb.ff) %>%
    filter(nb.fb > nb.ff) %>%
    glimpse()
  misc_print(df1)
  fb2 <- fb.df %>%
    filter(date %in% df1$date) %>%
    dplyr::select(nom_espece) %>%
    glimpse()
  ff2 <- ff.df %>%
    filter(date %in% df1$date) %>%
    dplyr::select(nom_espece) %>%
    glimpse()
  misc_print(fb2)
  misc_print(ff2)
}

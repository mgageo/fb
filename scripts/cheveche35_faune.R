# <!-- coding: utf-8 -->
#
# la partie faune-bretagne
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# la totale
# source("geo/scripts/chevechE35.R");faune_jour();
faune_jour <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  df <- faune_donnees_get()
  faune_geojson(df)
}
#
# les données
# source("geo/scripts/cheveche35.R");faune_donnees_get();
faune_donnees_get <- function(force = TRUE) {
  carp()
  library(tidyverse)
  library(janitor)
  library(rio)
  df <- biolo_export_espece(espece = 320 , dsn = donneesDsn, force = force)
  return(invisible(df))
}
# en format geojson
# source("geo/scripts/cheveche35.R");faune_geojson(force = TRUE)
faune_geojson <- function(force = TRUE) {
  library(tidyverse)
  library(rgdal)
  carp("dsn: %s", donneesDsn)
  nc <- faune_export_lire_sf(dsn = donneesDsn, force = force)
  nc1 <- nc %>%
    glimpse() %>%
    filter(grepl('^35', insee)) %>%
    filter(date_year >= 2024) %>%
    filter(total_count > 0) %>%
    mutate(name = sprintf("%s/%s", date_year, municipality)) %>%
    mutate(comment = sprintf("%s/%s", total_count, comment)) %>%
    dplyr::select(id_sighting, place, name, comment, observateur, aaaammjj)
  dsn <- sprintf('%s/faune.geojson', webDir)
  st_write(nc1, dsn, delete_dsn = TRUE)
  carp('dsn: %s', dsn)
}
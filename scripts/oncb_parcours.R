# <!-- coding: utf-8 -->
#
# quelques fonctions pour l'oncb
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
#
## les vérifications sur le parcours
#
# source("geo/scripts/oncb.R"); df <- parcours_verif(depts = c("22", "29", "35", "44", "56"), force = TRUE)
# source("geo/scripts/oncb.R"); df <- parcours_verif(depts = "35", force = TRUE)
parcours_verif <- function(annees = "2022", depts = "35", force = FALSE, test = 2) {
  library(tidyverse)
  library(stringi)
  carp("début")
  excl <- TRUE
  dossier <- sprintf("api_form")
  abscisses <- list(
	  "3" = "U",
	  "4" = "V",
	  "5" = "W",
	  "6" = "X",
	  "7" = "Y",
	  "8" = "Z"
  );
  ordonnees <- list(
	  "51" = "S",
	  "52" = "T",
	  "53" = "U",
	  "54" = "V",
	  "55" = "W",
	  "56" = "X"
  );
  df1 <- api_lire_tm_cpl(annees = annees, depts = depts, force = force) %>%
    glimpse()
#  df1 <- df1 %>%
#    dplyr::select(annee, dept, site_code, id_place, dept, municipality, coord_lat, coord_lon)
  nc1 <- st_as_sf(df1, coords = c("coord_lon", "coord_lat"), crs = 4326, remove = FALSE)
  nc2 <- nc1 %>%
    group_by(annee, dept, municipality, site_code, id_place, place) %>%
    summarize(nb = n()) %>%
    filter(grepl("^[WX]", site_code)) %>%
# WGS 84 / UTM zone 30N
    st_transform(32630) %>%
    mutate(centroid = st_centroid(geometry)) %>%
    ungroup() %>%
    mutate(X = st_coordinates(centroid)[,'X']) %>%
    mutate(Y = st_coordinates(centroid)[,'Y']) %>%
    dplyr::select(-centroid) %>%
    mutate(X = floor(X / 1000)) %>%
    mutate(Y = floor(Y / 1000)) %>%
    mutate(a = as.character(floor(X / 100))) %>%
    mutate(o = as.character(floor(Y / 100))) %>%
    mutate(a = abscisses[a]) %>%
    mutate(o = ordonnees[o]) %>%
    mutate(x = stri_sub(as.character(X), -2)) %>%
    mutate(y = stri_sub(as.character(Y), -2)) %>%
    mutate(carre = sprintf("%s%s%s%s", a, o, x, y)) %>%
    mutate(X = X * 1000 + 500) %>%
    mutate(Y = Y * 1000 + 500)
  nc3 <- st_as_sf(st_drop_geometry(nc2), coords = c("X", "Y"), crs = 32630, remove = FALSE)
  nc5 <- nc3 %>%
    mutate(geometry = st_buffer(geometry, dist = 500, nQuadSegs = 4, endCapStyle = "SQUARE"))
#
# les parcours extraits avec l'apivn en format json
  places.sf <<- misc_lire("extract_places_json", apivnDir) %>%
    st_transform(32630)
  parcoursDir <- sprintf("%s/parcours", texDir)
  dir.create(parcoursDir, showWarnings = FALSE)
  texFic <- sprintf("%s/parcours_tex.tex", texDir)
  TEX <- file(texFic)
  tex <- "% <!-- coding: utf-8 -->"
#
# le template tex
  dsn <- sprintf("%s/parcours_place_tpl.tex", texDir)
  template <- readLines(dsn)
  nc <- nc5 %>%
    mutate(oncb = sprintf("%s(%s %s) %s/%s - %s - %s", site_code, place, id_place, municipality, dept, annee, carre)) %>%
    mutate(oncb = escapeLatexSpecials(oncb)) %>%
    arrange(site_code) %>%
    glimpse()
#  stop("****")
  df1 <- tibble()
  for (i in 1:nrow(nc)) {
    site <- nc[[i, "id_place"]]
    carp("i: %s site: %s", i,  site)
    tpl <- template
    tpl <- tex_df2tpl(nc, i, tpl)
    tex <- append(tex, tpl)
    dsn <- sprintf("%s/%s.pdf", parcoursDir, site)
    stat <- parcours_carte(nc[i, ])
    df1 <- bind_rows(df1, as_tibble(stat))
    dev.copy(pdf, dsn, width = par("din")[1], height = par("din")[2])
    carp("dsn: %s %sx%s", dsn, par("din")[1], height = par("din")[2])
    graphics.off()
  }
  write(tex, file = TEX, append = FALSE)
  close(TEX)
  carp("texFic: %s", texFic)
  df1 <- df1 %>%
    arrange(`form.protocol.site_code`)
  tex_df2kable(df1)
# production du pdf
  dsn <- sprintf("parcours_%s_%s.tex", paste(annees, collapse = "-"), paste(depts, collapse = "-"))
  from <- sprintf("%s/parcours_ans_depts__tpl.tex", texDir)
  to <- sprintf("%s/%s", texDir, dsn)
  file.copy(from, to, overwrite = TRUE)
  tex_pdflatex(dsn)
  return()

}
#
#
parcours_carte <- function(carres.sf) {
  library(tidyverse)
  library(stringi)
  carp("début")
  glimpse(carres.sf)
  site <- carres.sf[[1, "id_place"]]
  nc1 <- places.sf %>%
    filter(id == carres.sf[[1, "id_place"]])
  nc2 <- st_convex_hull(nc1)
  nc200 <- nc1 %>%
    st_buffer(200)
  nc150 <- nc1 %>%
    st_buffer(150)
  carre_200 <- st_intersection(carres.sf, nc200)
  carp("la zone non prospectée")
  carre_200 <- st_difference(carres.sf, carre_200) %>%
      glimpse()
# https://r-charts.com/base-r/line-types/
  plot(st_geometry(nc200), lwd = 1, lty = 1, col = "red")
  plot(st_geometry(nc150), add = TRUE, lwd = 1, lty = 1, col = "green")
  plot(st_geometry(carres.sf), add = TRUE, lwd = 2, lty = 2, border = "darkblue")
  plot(st_geometry(carre_200), add = TRUE, col = "yellow")
  plot(st_geometry(nc1), add = TRUE, lwd = 3)
  geo_echelle()
  legend("topright", legend = site, cex = 2, text.col = "darkblue", box.lwd = 0, box.col = "transparent", bg = "transparent")
  stat <- list()
  stat["form.protocol.site_code"] <- carres.sf[[1, "site_code"]]
  stat["observation.place"] <- carres.sf[[1, "place"]]
  stat["id_place"] <- carres.sf[[1, "id_place"]]
  stat["carré UTM"] <- carres.sf[[1, "carre"]]
  stat["lg"] <- as.numeric(round(st_length(nc1), 0))
  stat["carré"] <- as.numeric(round(st_area(carres.sf) / 10000, 0))
  stat["150m"] <- as.numeric(round(st_area(nc150) / 10000, 0))
  stat["200m"] <- as.numeric(round(st_area(nc200) / 10000, 0))
  if (nrow(carre_200) > 0) {
    stat["% hors 200"] <- as.numeric(round(st_area(carre_200) / 100, 0)) /  as.numeric(round(st_area(carres.sf) / 10000, 0))
  } else {
    stat["% hors 200"] <- 0
  }
  glimpse(stat)
#  stop("*****")
  return(invisible(stat))
}
# <!-- coding: utf-8 -->
#
# traitements pour les chevêches du 35
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
#
# la carte interactive avec tmap
# https://rpubs.com/quarcs-lab/tutorial-maps-in-r
#
# source("geo/scripts/cheveche35.R"); tmap_annee()
tmap_annee <- function(annee = "2024") {
  library(sf)
  library(tmap)
  tmap_mode("view")
  nc1 <- misc_lire("prospection_annees") %>%
    filter(annee == !!annee)
  sfc1 <- nc1 %>%
    st_union() %>%
    st_convex_hull() %>%
    st_buffer(1500)
  nc2 <- st_sf(data.frame(Name = "prospections", geom=sfc1)) %>%
    glimpse()
  tm_shape(nc2) +
    tm_markers(nc1)
}

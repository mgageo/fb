# <!-- coding: utf-8 -->
#
# quelques fonctions pour le suivi gravelot à collier interrompu
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
## les bagues
#
#
bagues_annees <- c(2007:2023)
# source("geo/scripts/gci35.R"); df <- bagues_jour()
bagues_jour <- function(annee = "2024") {
  library(tidyverse)
  library(lubridate)
  carp()
  dsn <- sprintf("%s/gci35_%s.json", webDir, annee)
  url <- sprintf("https://bretagne-vivante-dev.org/gci35/gci35.php?action=raw&annee=%s", annee)
  download.file(url, dsn)
  dest <- sprintf("www/gci35/gci35_%s.json", annee)
  ftp_upload(site = "always_mga", local = dsn, dest = dest)
  return(invisible())
}
bagues_tex <- function() {
  tex_pdflatex("bagues.tex")
}
#
# source("geo/scripts/gci35.R"); df <- bagues_annees_get()
bagues_annees_get <- function() {
  carp()
  for (annee in bagues_annees) {
    dsn <- sprintf("%s/gci35_%s.json", webDir, annee)
    url <- sprintf("https://bretagne-vivante-dev.org/gci35/gci35.php?action=raw&annee=%s", annee)
    download.file(url, dsn)
  }
  return(invisible())
}
#
# par export en format geojson avec mon script gci35.php
# source("geo/scripts/gci35.R"); df <- bagues_table_get()
bagues_table_get <- function() {
  carp()
  dsn <- sprintf("%s/gci35_table.json", webDir)
  url <- sprintf("https://bretagne-vivante-dev.org/gci35/gci35.php?action=raw&annees")
  download.file(url, dsn)
  return(invisible())
}
# source("geo/scripts/gci35.R"); df <- bagues_table_lire()
bagues_table_lire <- function() {
  library(tidyverse)
  library(sf)
  carp()
  dsn <- sprintf("%s/gci35_table.json", webDir)
  nc <- st_read(dsn) %>%
    glimpse()
  return(invisible())
}
#
# par export en format tsv avec adminer
# source("geo/scripts/gci35.R"); df <- bagues_bvgravelot_lire()
bagues_bvgravelot_lire <- function() {
  library(tidyverse)
  library(lubridate)
  library(readr)
  carp()
  dsn <- sprintf("%s/bvgravelot.csv", webDir)
  df <-  readr::read_tsv(dsn, col_types = cols(.default = "c")) %>%
    mutate(Date = ymd(gradate)) %>%
    mutate(Annee = format(Date, "%Y")) %>%
    mutate(Lat = as.numeric(gralatitude)) %>%
    mutate(Lon = as.numeric(gralongitude)) %>%
    mutate(Combinaison = toupper(gracombinaison)) %>%
    filter(! is.na(Lon)) %>%
    filter(! is.na(Lat)) %>%
    glimpse()
#  df3 <- df %>%
#    filter(is.na(Date)) %>%
#    glimpse()
  return(invisible(df))
}
# source("geo/scripts/gci35.R"); df <- bagues_bvgravelot_bugs()
bagues_bvgravelot_bugs <- function() {
  library(tidyverse)
  library(lubridate)
  carp()
  df <- bagues_bvgravelot_lire()
  carp("erreur action B")
  df2 <- df %>%
    filter(graaction == "B") %>%
    filter(is.na(grabagueur) | is.na(Date))
  carp("erreur de date")
  df3 <- df %>%
    filter(is.na(Date)) %>%
    glimpse()
  carp("absence bague")
  df4 <- df %>%
    filter(is.na(grabague)) %>%
    glimpse() %>%
    filter(! grepl("#", gracombinaison)) %>%
    group_by(gracombinaison) %>%
    summarize(nb = n()) %>%
    arrange(desc(nb)) %>%
    glimpse()
  carp("latitude/longitude")
  df5 <- df %>%
    filter(Lon > 10 | Lon < -15 | Lat < 40) %>%
    dplyr::select(gradate, grasecteur, grapays, gramail, gracombinaison, Lat, Lon) %>%
    glimpse()
  misc_print(df5)
  return(invisible(df))
}
#
# source("geo/scripts/gci35.R"); df <- bagues_stat_bagueur()
bagues_stat_bagueur <- function() {
  library(tidyverse)
  library(janitor)
  carp()
  df <- bagues_bvgravelot_lire()
  df1 <- df %>%
    filter(graaction == "B") %>%
    group_by(grabagueur, Annee, dept = gradepartement) %>%
    summarize(nb = n()) %>%
    arrange(Annee) %>%
    pivot_wider(names_from = Annee, values_from = nb, values_fill = list(nb = 0), names_repair = "unique") %>%
    adorn_totals(where=c('row', 'col'), name='Total') %>%
    glimpse()
  misc_print(df1)
  df1 <- df %>%
    filter(graaction == "B") %>%
    group_by(grasecteur, Annee) %>%
    summarize(nb = n()) %>%
    arrange(Annee) %>%
    pivot_wider(names_from = Annee, values_from = nb, values_fill = list(nb = 0), names_repair = "unique") %>%
    adorn_totals(where=c('row', 'col'), name='Total') %>%
    glimpse()
  misc_print(df1)
  return(invisible(df))
}
#
# source("geo/scripts/gci35.R"); df <- bagues_carte_baguage()
bagues_carte_baguage <- function() {
  library(tidyverse)
  library(janitor)
  carp()
  crs <- 2154
  df <- bagues_bvgravelot_lire()
  df1 <- df %>%
    filter(graaction == "B")
  nc1 <- st_as_sf(df1, coords = c("Lon", "Lat"), crs = 4326, remove = TRUE) %>%
    st_transform(crs) %>%
    glimpse()
  carp("la bbox")
  bbox1 <- st_bbox(st_buffer(nc1, 25000)) %>%
    glimpse()
  bbox1.sfc <- st_as_sfc(bbox1, crs = crs) %>%
    glimpse()
  plot(bbox1.sfc)
  carp("naturalearth")
# https://github.com/r-spatial/sf/issues/1759
#  sf_use_s2(FALSE)
  countries.sf <- ne_countries(country = "France", scale = 10, returnclass = 'sf') %>%
    st_transform(crs = crs) %>%
    st_crop(bbox1.sfc)
  gg <- ggplot() +
    geom_sf(data = countries.sf, aes(fill = "brown"), show.legend = F) +
    geom_sf(data = nc1, aes(size = 10), show.legend = F)
#
# pour avoir le nom des secteurs
  nc2 <- nc1 %>%
    group_by(grasecteur) %>%
    summarize(nb = n()) %>%
    st_centroid() %>%
    ungroup() %>%
    glimpse()
  misc_print(nc2)
  df2 <- nc2 %>%
    sf::st_centroid() %>%
    sf::st_coordinates() %>%
    as_tibble() %>%
    glimpse()
#  nc2 <- bind_rows(nc2, df2)
  gg <- gg +
    geom_sf_text(data = nc2, aes(label = grasecteur))

  gg <- gg + coord_sf(datum = NA) + ggplot_gci35()
  print(gg)
  return(invisible(df))
}
#
# source("geo/scripts/gci35.R"); df <- bagues_secteurs()
bagues_secteurs <- function() {
  library(tidyverse)
  library(janitor)
  carp()
  crs <- 2154
  df1 <- bagues_bvgravelot_lire()
  nc1 <- st_as_sf(df1, coords = c("Lon", "Lat"), crs = 4326, remove = TRUE) %>%
    st_transform(crs) %>%
    glimpse()
#
# pour avoir le nom des secteurs
  nc2 <- nc1 %>%
    group_by(grasecteur) %>%
    summarize(nb = n()) %>%
    st_centroid() %>%
    ungroup() %>%
    glimpse()
  misc_print(nc2)
  return(invisible(df))
}
#
# source("geo/scripts/gci35.R");  bagues_leaflet_baguage()
bagues_leaflet_baguage <- function() {
  library(tidyverse)
  library(leaflet)
  carp()
  crs <- 4326; # pour leaflet
  df <- bagues_bvgravelot_lire()
  df1 <- df %>%
    filter(! is.na(Lon)) %>%
    filter(! is.na(Lat)) %>%
    filter(graaction != "B")
  nc1 <- st_as_sf(df1, coords = c("Lon", "Lat"), crs = 4326, remove = TRUE) %>%
    st_transform(crs) %>%
    glimpse()
# https://cran.r-project.org/web/packages/geofi/vignettes/geofi_making_maps.html
  labels <- sprintf(
    "<strong>%s</strong> %s",
    nc1$grasecteur, nc1$graid
  ) %>% lapply(htmltools::HTML)
  m <- leaflet() %>%
    addTiles() %>%  # Add default OpenStreetMap map tiles
    addMarkers(
      data = nc1,
      label = labels,
      clusterOptions = markerClusterOptions()
    )
  return(m)
}
#
# source("geo/scripts/gci35.R"); df <- bagues_stat_dept(dept = "35")
bagues_stat_dept <- function(dept = "35") {
  library(tidyverse)
  library(janitor)
  library(sf)
  library(rnaturalearth)
  library(rnaturalearthdata)
  library(ggplot2)
  carp()
# en Europe : 3035
  crs <- 4326
  df <- bagues_bvgravelot_lire( )%>%
    filter(! grepl("#", gracombinaison)) %>%
    filter(! is.na(Lat)) %>%
    filter(! is.na(Lon)) %>%
    filter(Lon < 10)
  df1 <- df  %>%
    filter(gradepartement == !!dept) %>%
    group_by(Combinaison) %>%
    summarize(nb = n()) %>%
    arrange(desc(nb)) %>%
    glimpse()
  df2 <- df %>%
    filter(Combinaison %in% df1$Combinaison)
  nc1 <- st_as_sf(df2, coords = c("Lon", "Lat"), crs = 4326, remove = TRUE) %>%
    st_transform(crs) %>%
    glimpse()
  carp("la bbox")
  bbox1 <- st_bbox(st_buffer(nc1, 5)) %>%
    glimpse()
  bbox1.sfc <- st_as_sfc(bbox1, crs = crs) %>%
    glimpse()
  plot(bbox1.sfc)
  carp("naturalearth")
# https://github.com/r-spatial/sf/issues/1759
  sf_use_s2(FALSE)
  countries.sf <- ne_countries(scale = 50, returnclass = 'sf') %>%
    st_transform(crs = crs) %>%
    st_crop(bbox1.sfc) %>%
    glimpse()
  gg <- ggplot() +
    geom_sf(data = countries.sf, aes(fill = continent)) +
    geom_sf(data = nc1, aes(size = 10))
  print(gg)
  return(invisible(df))
}
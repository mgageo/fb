# <!-- coding: utf-8 -->
#
# les traitements sur les données en base postgresql
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# source("geo/scripts/apivn.R"); extract_jour()
extract_jour <- function() {
  library(dbplyr)
  library(tidyverse)
  library(lubridate)
  carp()
  user1 <- "apivn32"
  psql <- extract_psql()
  extract_epoc(psql = psql)
  extract_epoc_odf(psql = psql)
  extract_stoc(psql = psql)
}
#
# source("geo/scripts/apivn.R"); extract_psql()
extract_psql <- function() {
  carp()
  psql <- "
SELECT src_vn.observations.id_form_universal, id_sighting, id_species, french_name, date, admin_hidden
  , precision, estimation_code, count
  , src_vn.observations.project_code
  , src_vn.forms.coord_lat, src_vn.forms.coord_lon
  , src_vn.forms.date_start, src_vn.forms.date_stop
  , src_vn.forms.time_start, src_vn.forms.time_stop
  , src_vn.forms.full_form
  , src_vn.forms.protocol_name
  FROM src_vn.observations
  LEFT JOIN src_vn.species ON
    src_vn.observations.id_species = src_vn.species.id
    AND src_vn.observations.taxonomy = src_vn.species.id_taxo_group
  LEFT JOIN src_vn.forms ON
    src_vn.observations.id_form_universal = src_vn.forms.id_form_universal
  WHERE src_vn.observations.taxonomy = 1
    AND %s
"
  return(invisible(psql))
}
#
# source("geo/scripts/apivn.R"); extract_psql()
extract_psql2 <- function() {
  carp()
  psql <- "
SELECT src_vn.observations.id_form_universal, id_sighting, id_species, french_name, date, admin_hidden
  , insert_date, update_date
  , src_vn.forms.protocol_name
  FROM src_vn.observations
  LEFT JOIN src_vn.species ON
    src_vn.observations.id_species = src_vn.species.id
    AND src_vn.observations.taxonomy = src_vn.species.id_taxo_group
  LEFT JOIN src_vn.forms ON
    src_vn.observations.id_form_universal = src_vn.forms.id_form_universal
  WHERE src_vn.observations.taxonomy = 1
    AND %s
"
  return(invisible(psql))
}
#
# source("geo/scripts/apivn.R"); extract_psql()
extract_psql3 <- function() {
  carp()
  psql <- "
SELECT src_vn.observations.id_form_universal, id_sighting, id_species, french_name, date, admin_hidden
  , insert_date, update_date
  , src_vn.forms.id as id_form, src_vn.forms.protocol_name, src_vn.forms.date_start, src_vn.forms.time_start, src_vn.forms.date_stop, src_vn.forms.time_stop
  FROM src_vn.observations
  LEFT JOIN src_vn.species ON
    src_vn.observations.id_species = src_vn.species.id
    AND src_vn.observations.taxonomy = src_vn.species.id_taxo_group
  LEFT JOIN src_vn.forms ON
    src_vn.observations.id_form_universal = src_vn.forms.id_form_universal
  WHERE src_vn.observations.taxonomy = 1
    AND %s
"
  return(invisible(psql))
}
#
# source("geo/scripts/apivn.R"); extract_psql_shoc()
extract_psql_shoc <- function() {
  carp()
  psql <- "
  SELECT id_sighting, id_species, french_name, date, timing
  ,  id_place, place, precision, src_vn.observations.coord_lat, src_vn.observations.coord_lon
  , insert_date, update_date, src_vn.observations.observer_uid
  , src_vn.forms.id_form_universal, src_vn.forms.id as id_form, src_vn.forms.protocol_name, src_vn.forms.date_start, src_vn.forms.time_start, src_vn.forms.date_stop, src_vn.forms.time_stop
  , src_vn.forms.observer_uid, src_vn.forms.protocol
  FROM src_vn.observations
  LEFT JOIN src_vn.species ON
    src_vn.observations.id_species = src_vn.species.id
    AND src_vn.observations.taxonomy = src_vn.species.id_taxo_group
  LEFT JOIN src_vn.forms ON
    src_vn.observations.id_form_universal = src_vn.forms.id_form_universal
  WHERE src_vn.observations.taxonomy = 1
    AND %s
"
  return(invisible(psql))
}
#
# pour les oncb
extract_psql_tm <- function() {
  carp()
  psql <- "
  SELECT src_vn.observations.*
  , src_vn.forms.id as id_form, src_vn.forms.protocol_name
  , src_vn.forms.date_start, src_vn.forms.time_start, src_vn.forms.date_stop, src_vn.forms.time_stop
  , src_vn.forms.comments, src_vn.forms.protocol
  , src_vn.forms.coord_lat as form_lat, src_vn.forms.coord_lon as form_lon
  , src_vn.places.id_commune, src_vn.places.name
  , src_vn.local_admin_units.name as municipality, src_vn.local_admin_units.insee
  , src_vn.species.french_name as espece
  FROM src_vn.observations
  LEFT JOIN src_vn.species ON
    src_vn.observations.id_species = src_vn.species.id
    AND src_vn.observations.taxonomy = src_vn.species.id_taxo_group
  LEFT JOIN src_vn.forms ON
    src_vn.observations.id_form_universal = src_vn.forms.id_form_universal
  LEFT JOIN src_vn.places ON
    src_vn.observations.id_place = src_vn.places.id
  LEFT JOIN src_vn.local_admin_units ON
    src_vn.local_admin_units.id = src_vn.places.id_commune
  WHERE src_vn.observations.taxonomy = 1
    AND %s
"
  return(invisible(psql))
}
#
# source("geo/scripts/apivn.R"); extract_form()
extract_form <- function(user = "apivn32") {
  carp()
  psql_connect(user = user)
  psql <- extract_psql2()
  psql <- sprintf(psql, "src_vn.forms.id_form_universal = '65_2207477'")

  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  misc_print(res)
}

#
# source("geo/scripts/apivn.R"); extract_epoc()
extract_epoc <- function(psql, user = "apivn32") {
  carp()
  psql_connect(user = user)
  psql <- sprintf(psql, "src_vn.observations.project_code = 'EPOC'")
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  misc_ecrire(res, "extract_epoc")
}
#
# source("geo/scripts/apivn.R"); extract_epoc_odf()
extract_epoc_odf <- function(psql, user = "apivn32") {
  carp()
  psql_connect(user = user)
  psql <- sprintf(psql, "src_vn.observations.project_code = 'EPOC-ODF'")
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  misc_ecrire(res, "extract_epoc_odf")
}
#
# source("geo/scripts/apivn.R"); extract_oncb()
extract_oncb <- function(psql, user = "apivn32") {
  carp()
  psql_connect(user = user)
  psql <- extract_psql_shoc()
  psql <- sprintf(psql, "src_vn.observations.project_code = 'ONCB'")
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  misc_ecrire(res, "extract_oncb")
}
#
# source("geo/scripts/apivn.R"); extract_shoc()
extract_shoc <- function(psql, user = "apivn32") {
  carp()
  library(jsontools)
  psql_connect(user = user)
  psql <- extract_psql_shoc()
  psql <- sprintf(psql, "src_vn.forms.protocol_name = 'SHOC'")
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
#  unpacked <- jsonlite::fromJSON(res$protocol, simplifyDataFrame = TRUE) %>%
#    glimpse()
  res <- res %>%
    extract(protocol, c("site_code"), '"site_code"\\s*\\:\\s*"([^"]+)+"') %>%
    glimpse()
#  df <- data.frame()
#  for (i in 1:nrow(res)) {
#    carp("i: %s", i)
#    df <- bind_rows(df, jsonlite::fromJSON(json, simplifyDataFrame = FALSE))
#  }
#  glimpse(df)
  misc_ecrire(res, "extract_shoc")
}
#
# source("geo/scripts/apivn.R"); extract_species()
extract_species <- function(psql, user = "apivn144") {
  carp()
  library(jsontools)
  psql_connect(user = user)
  psql <- "
SELECT *
  FROM src_vn.species
;
"
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  return(invisible(res))
}
#
# source("geo/scripts/apivn.R"); extract_stoc()
extract_stoc <- function(psql, user = "apivn32") {
  carp()
  psql_connect(user = user)
  psql <- extract_psql2()
  psql <- sprintf(psql, "src_vn.forms.protocol_name = 'STOC_EPS'")

  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  misc_ecrire(res, "extract_stoc")
}
#
# source("geo/scripts/apivn.R"); extract_tm()
extract_tm <- function(user = "apivn144", force = TRUE) {
  carp()
  psql_connect(user = user)
  psql <- extract_psql_tm()
  psql <- sprintf(psql, "src_vn.forms.protocol_name = 'TERRITORY_MAPPING'")
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  misc_ecrire(res, "extract_tm")
#  export_df2xlsx(res)
}
#
# extraction d'un formulaire
# source("geo/scripts/apivn.R"); extract_tm_form()
extract_tm_form <- function(psql = "", user = "apivn32", force = TRUE) {
  carp()
  psql_connect(user = user)
  psql <- extract_psql_tm()
# https://www.faune-bretagne.org/index.php?m_id=1380&fid=100369
  psql <- sprintf(psql, "src_vn.forms.id = 100369")
  res <- db_SendQuery(db_con, psql) %>%
    dplyr::select(espece, timing, id_universal, id_form_universal, id_place, place) %>%
    slice_tail(n = 5) %>%
    glimpse()
  misc_print(res)
  return()
  psql <- extract_psql_tm()
  psql <- sprintf(psql, "src_vn.forms.id = 106093")
  res <- db_SendQuery(db_con, psql) %>%
    dplyr::select(id_universal, timing, id_form_universal, time_start) %>%
    arrange(id_universal) %>%
    glimpse()
  misc_print(res)
#  export_df2xlsx(res)
}

#
# source("geo/scripts/apivn.R"); extract_espece_gci()
extract_espece_gci <- function(psql, user = "apivn32") {
  carp()
  psql_connect(user = user)
  psql <- extract_psql2()
  psql <- sprintf(psql, "src_vn.observations.id_species = 219")
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  misc_ecrire(res, "extract_tm")
#  export_df2xlsx(res)
}
#
# source("geo/scripts/apivn.R"); extract_places()
extract_places <- function(psql, user = "apivn32", force = TRUE) {
  carp()
  psql_connect(user = user)
  psql <- "
  SELECT place_type, COUNT(*)
  FROM src_vn.places
  GROUP BY place_type;
"
#  psql <- sprintf(psql, "src_vn.forms.protocol_name = 'TERRITORY_MAPPING'")
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  misc_ecrire(res, "extract_places")
  export_df2xlsx(res)
}
#
# source("geo/scripts/apivn.R"); extract_places_place()
extract_places_place <- function(psql, user = "apivn32") {
  carp()
  psql_connect(user = user)
  psql <- "
  SELECT *
  FROM src_vn.places
  WHERE place_type = 'place' AND is_private = FALSE;
"
#  psql <- sprintf(psql, "src_vn.forms.protocol_name = 'TERRITORY_MAPPING'")
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  misc_ecrire(res, "extract_places_place")
}
#
# source("geo/scripts/apivn.R"); df <- extract_forms_protocol_json(protocol = "STOC_EPS"); glimpse(df)
# source("geo/scripts/apivn.R"); df <- extract_forms_protocol_json(protocol = "TERRITORY_MAPPING"); glimpse(df)
extract_forms_protocol_json <- function(protocol = "STOC_EPS", user = "apivn144", force = TRUE) {
  carp()
  library(jsonlite)
  dsn <- sprintf("extract_forms_protocol_json_%s", protocol)
  psql_connect(user = user)
  psql <- "
  SELECT *
  FROM import.forms_json
  WHERE (item::json ->'protocol') is not null
  AND  (item::json ->'protocol'->'protocol_name') is not null
  AND  item::json ->'protocol'->>'protocol_name' = '%s'
;
"
  psql <- sprintf(psql, protocol)
  res <- db_SendQuery(db_con, psql)
  df1 <- as.data.frame(res) %>%
    glimpse()
# https://themockup.blog/posts/2020-05-22-parsing-json-in-r-with-jsonlite/
#  raw_json <<- jsonlite::fromJSON(df1[1, "item"])
  df <- tibble()
  for (i in 1:nrow(df1)) {
#    carp("i: %s", i)
#    if (i == 3850) {
#      print(df1[i, "item"])
#    }
    df2 <- json_json2df(df1[i, "item"])
    df <- bind_rows(df, df2)
  }
  glimpse(df)
  misc_ecrire(df, dsn)
  return(invisible(df))
}
# source("geo/scripts/apivn.R"); df <- extract_forms_pretty(); glimpse(df)
extract_forms_pretty <- function(user = "apivn32", force = TRUE) {
  carp()
  library(jsonlite)
  psql_connect(user = user)
  psql <- "
  SELECT jsonb_pretty(item)
  FROM import.forms_json
  WHERE item::json->>'@id' = '106111'
;
"
  res <- db_SendQuery(db_con, psql)
  print(res)
}
# validation https://www.faune-bretagne.org/index.php?m_id=54&id=5184941
# WHERE item::json->'observers'->0->>'id_sighting' = '5184941'
# admin_hidden = 1, admin_hidden_type = incomplete
# chn
# WHERE item::json->'observers'->0->>'id_sighting' = '5317163'
#      "committees_validation": {
#        "chr": "REQUESTED"
#      }
# chn accepté
# https://www.faune-bretagne.org/index.php?m_id=54&id=3917311
#      "committees_validation": {
#        "chr": "ACCEPTED"
#      }
# source("geo/scripts/apivn.R"); df <- extract_observations_pretty(); glimpse(df)
extract_observations_pretty <- function(user = "apivn144", force = TRUE) {
  carp()
  library(jsonlite)
  psql_connect(user = user)
  psql <- "
  SELECT *
  FROM import.observations_json
  WHERE item::json->'observers'->0->>'id_sighting' = '5743693'
  LIMIT 1
;
"
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  df1 <- as.data.frame(res) %>%
    glimpse()
  for (i in 1:nrow(df1)) {
    carp("i: %s/%s", i, nrow(df1))
    txt <- jsonlite::prettify(df1[[i, "item"]], indent = 2)
    print(txt)
  }
}
# source("geo/scripts/apivn.R"); df <- extract_observations_hidden(); glimpse(df)
extract_observations_hidden <- function(user = "apivn32", force = TRUE) {
  carp()
  library(jsonlite)
  psql_connect(user = user)
  psql <- "
  SELECT *
  FROM import.observations_json
  WHERE (item::json ->'observers'->0->'admin_hidden') is not null
  LIMIT 10
;
"
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  df1 <- as.data.frame(res) %>%
    glimpse()
  for (i in 1:nrow(df1)) {
    carp("i: %s/%s", i, nrow(df1))
    txt <- jsonlite::prettify(df1[[i, "item"]], indent = 2)
    print(txt)
  }
}
# source("geo/scripts/apivn.R"); df <- extract_observations_hidden(); glimpse(df)
# admin_hidden_type <chr> "incomplete", "question", "refused"
extract_observations_hidden <- function(user = "apivn32", force = TRUE) {
  carp()
  library(jsonlite)
  psql_connect(user = user)
  psql <- "
  SELECT
    item::json->'observers'->0->>'id_sighting' as id_sighting
    , item::json->'observers'->0->>'admin_hidden' as admin_hidden
    , item::json->'observers'->0->>'admin_hidden_type' as admin_hidden_type
  FROM import.observations_json
  WHERE (item::json->'observers'->0->'admin_hidden') is not null
;
"
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  df1 <- as.data.frame(res) %>%
    group_by(admin_hidden_type) %>%
    summarize(nb = n()) %>%
    glimpse()
}
# source("geo/scripts/apivn.R"); df <- extract_observations_validation(); glimpse(df)
# $ chr <chr> "ACCEPTED", "REQUESTED"
extract_observations_validation <- function(user = "apivn32", force = TRUE) {
  carp()
  library(jsonlite)
  psql_connect(user = user)
# (item #>> '{observers,0,id_sighting}') as id_sighting
  psql <- "
  SELECT
    item::json->'observers'->0->>'id_sighting' as id_sighting
    , item::json->'observers'->0->'committees_validation'->>'chr' as chr
  FROM import.observations_json
  WHERE (item::json ->'observers'->0->'committees_validation'->'chr') is not null
;
"
  res <- db_SendQuery(db_con, psql)%>%
    glimpse()
  df1 <- as.data.frame(res) %>%
    group_by(chr) %>%
    summarize(nb = n()) %>%
    glimpse()
}
# source("geo/scripts/apivn.R"); df <- extract_observations_medias(); glimpse(df)
extract_observations_medias <- function(user = "apivn144", force = TRUE) {
  carp()
  library(jsonlite)
  psql_connect(user = user)
  psql <- "
  SELECT
    item::json->'observers'->0->>'id_sighting' as id_sighting
    , item
  FROM import.observations_json
  WHERE (item::json ->'observers'->0->'medias') is not null
;
"
  res <- db_SendQuery(db_con, psql)%>%
    glimpse()
  df1 <- as.data.frame(res) %>%
    glimpse()
}
#  WHERE item::json->'observers'->0->>'id_sighting' = '5317163'
#
# source("geo/scripts/apivn.R"); extract_places_json()
extract_places_json_v1 <- function(user = "apivn32") {
  carp()
  library(jsonlite)
  psql_connect(user = user)
  psql <- "
  SELECT *
  FROM import.places_json;
"
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  df1 <- as.data.frame(res) %>%
    glimpse()
  df <- data.frame()
  for (i in 1:nrow(df1)) {
    carp("i: %s/%s", i, nrow(df1))
    df <- bind_rows(df, jsonlite::fromJSON(df1[[i, "item"]], simplifyDataFrame = FALSE))
  }
  glimpse(df)
  misc_ecrire(df, "extract_places_json")
}
#
# source("geo/scripts/apivn.R"); extract_places_json()
extract_places_json <- function(user = user1, force = TRUE) {
  carp()
  library(jsonlite)
  library(sf)
  psql_connect(user = user)
  psql <- "
SELECT *
  FROM import.places_json
  WHERE;
"
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  df1 <- as.data.frame(res) %>%
    filter(wkt != "") %>%
    glimpse()
  nc1 <- st_as_sf(df1, geometry=st_as_sfc(df1$wkt, crs = st_crs(4326))) %>%
    glimpse()
  misc_ecrire(nc1, "extract_places_json")
  nc2 <- nc1 %>%
    filter(id == 301571) %>%
    glimpse()
  st_write(nc2, "d:/extract_places_json.geojson", delete_dsn = TRUE, driver = "GeoJSON")
}
# source("geo/scripts/apivn.R");extract_places_json_pretty()
extract_places_json_pretty <- function(user = "apivn144", force = TRUE) {
  carp()
  library(jsonlite)
  psql_connect(user = user)
  psql <- "
  SELECT *
  FROM import.places_json
  WHERE item::json->>'id' = '299405'
  LIMIT 10
;
"
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  df1 <- as.data.frame(res) %>%
    glimpse()
  for (i in 1:nrow(df1)) {
    carp("i: %s/%s", i, nrow(df1))
    txt <- jsonlite::prettify(df1[[i, "item"]], indent = 2)
    print(txt)
  }
}
#
# source("geo/scripts/apivn.R"); extract_lire()
extract_lire <- function(source = "extract_epoc_odf") {
  carp("source: %s", source)
  library(lubridate)
  df <- misc_lire(source) %>%
    mutate(aaaa = strftime(date, format="%Y")) %>%
    mutate(aaaamm = strftime(date, format="%Y-%m")) %>%
    mutate(aaaammjj = strftime(date, format="%Y-%m-%d")) %>%
    mutate(mm = strftime(date, format="%m"))
  return(invisible(df))
}
#
# source("geo/scripts/apivn.R"); extract_projects()
extract_projects <- function(user = "apivn32") {
  carp()
  psql_connect(user = user)
  psql <- "SELECT project_code, COUNT(*)
  FROM src_vn.observations
  GROUP BY project_code;
"
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  misc_print(res)
  misc_ecrire(res, "extract_projects")
}
#
# source("geo/scripts/apivn.R"); extract_protocols()
extract_protocols <- function(user = "apivn32") {
  carp()
  psql_connect(user = user)
  psql <- "SELECT protocol_name, COUNT(*)
  FROM src_vn.forms
  GROUP BY protocol_name;
"
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  misc_print(res)
  misc_ecrire(res, "extract_protocols")
}
#
# source("geo/scripts/apivn.R"); extract_stat()
extract_stat <- function() {
  carp()
  library(lubridate)
  library(ggplot2)
  library(ggthemes)
  debut <- as.Date("2021-03-01")
  fin <- as.Date("2021-06-30")
  horaire_debut <- hm("06h00")
  horaire_fin <- hm("12h00")
  epoc.df <- extract_lire("extract_epoc") %>%
    filter(full_form == "1") %>%
#    filter(observers.source == "MOBILE_FORM_LIVE" & observers.precision == "precise") %>%
    filter(duree > 270 & duree < 390) %>%
    filter(date >= debut & date <= fin) %>%
    filter(heure_fin >= horaire_debut & heure_fin <= horaire_fin) %>%
    mutate(source = "epoc") %>%
    glimpse()
  epoc_odf.df <- extract_lire("extract_epoc_odf") %>%
#    filter(full_form == "1") %>%
#    filter(observers.source == "MOBILE_FORM_LIVE" & observers.precision == "precise") %>%
    filter(duree > 270 & duree < 390) %>%
    filter(date >= debut & date <= fin) %>%
    filter(heure_fin >= horaire_debut & heure_fin <= horaire_fin) %>%
    mutate(source = "epoc_odf") %>%
    glimpse()
  stoc.df <- extract_lire("extract_stoc") %>%
#    filter(full_form == "1") %>%
#    filter(observers.source == "MOBILE_FORM_LIVE" & observers.precision == "precise") %>%
    filter(duree > 270 & duree < 390) %>%
    filter(date >= debut & date <= fin) %>%
    filter(heure_fin >= horaire_debut & heure_fin <= horaire_fin) %>%
    mutate(source = "stoc") %>%
    glimpse()
  df1 <- rbind(epoc.df, epoc_odf.df, stoc.df) %>%
    rename(id_form = id_form_universal) %>%
    rename(espece = french_name) %>%
    rename(nb = `count`) %>%
    glimpse()
  extract_stat_espece(df1, stat = "abondance")
#  extract_stat_abondance(df1)
#  extract_stat_diversite(df1)
}
#
# la présence/abondance d'une espèce sur un point
extract_stat_espece <- function(df1, stat = "presence") {
  carp()
  library(lubridate)
  library(janitor)
  library(ggplot2)
  library(ggthemes)

  if(stat == 'presence') {
    df3 <- df1 %>%
      group_by(source, id_form) %>%
      summarize(nb = n()) %>%
      group_by(source) %>%
      summarize(nb_forms = n()) %>%
      ungroup() %>%
      glimpse()
    df2 <- df1  %>%
      group_by(source, id_form, espece) %>%
      summarize(nb = n()) %>%
      group_by(source, espece) %>%
      summarize(nb = n()) %>%
      left_join(df3, by = c("source")) %>%
      mutate(nb = round((100 * nb) / nb_forms)) %>%
      arrange(espece, source) %>%
      glimpse()
# les 15 espèces avec le plus de présence
    df5 <- df2 %>%
      dplyr::select(-nb_forms) %>%
      pivot_wider(names_from = source, values_from = nb, values_fill = list(nb = 0)) %>%
      adorn_totals(where = c("col")) %>%
      arrange(-Total) %>%
      head(15)
    df4 <- df2 %>%
      filter(espece %in% df5$espece) %>%
      arrange(desc(espece)) %>%
      glimpse()
      ylab <- "% de présence"
  }
  if(stat == 'abondance_point') {
    df2 <- df1 %>%
      group_by(source, id_form, espece) %>%
      summarize(nb = sum(nb)) %>%
      group_by(source, espece) %>%
      summarize(nb = sum(nb), nb_forms = n()) %>%
      ungroup() %>%
      glimpse()
# les 15 espèces les plus abondantes globalement
    df5 <- df2 %>%
      dplyr::select(-nb_forms) %>%
      pivot_wider(names_from = source, values_from = nb, values_fill = list(nb = 0)) %>%
      adorn_totals(where = c("col")) %>%
      arrange(-Total) %>%
      head(15)
    df4 <- df2 %>%
      filter(espece %in% df5$espece) %>%
      arrange(desc(espece)) %>%
      mutate(nb = nb / nb_forms) %>%
      glimpse()
      ylab <- "nb oiseaux sur point de présence"
  }
  if(stat == 'abondance') {
    df3 <- df1 %>%
      group_by(source, id_form) %>%
      summarize(nb = n()) %>%
      group_by(source) %>%
      summarize(nb_forms = n()) %>%
      ungroup() %>%
      glimpse()
    df2 <- df1 %>%
      group_by(source, espece) %>%
      summarize(nb = sum(nb)) %>%
      ungroup() %>%
      glimpse() %>%
      left_join(df3, by = c("source")) %>%
      arrange(-nb) %>%
      mutate(nb = (nb * 10) / nb_forms) %>%
      glimpse()
# les 15 espèces les plus abondantes globalement
    df5 <- df2 %>%
      dplyr::select(-nb_forms) %>%
      pivot_wider(names_from = source, values_from = nb, values_fill = list(nb = 0)) %>%
      adorn_totals(where = c("col")) %>%
      arrange(-Total) %>%
      head(15)
    df4 <- df2 %>%
      filter(espece %in% df5$espece) %>%
      arrange(desc(espece)) %>%
      mutate(nb = nb / nb_forms) %>%
      glimpse()
      ylab <- "nb oiseaux par point"
  }
  gg <- ggplot(df4, aes(x = espece, y = nb, fill = source)) +
    geom_bar(stat = "identity", position = position_dodge()) +
    coord_flip()
  gg <- gg + theme(
    axis.title.y = element_blank(),
  )
  gg <- gg + ylab(ylab)
  print(gg)
}

#
# l'abondance (nombre d'oiseaux) sur un point
extract_stat_abondance <- function(df1) {
  carp()
  library(lubridate)
  library(ggplot2)
  library(ggthemes)
  df2 <- df1 %>%
    group_by(id_form, source) %>%
    summarize(nb_oiseaux = sum(nb)) %>%
    group_by(source, nb_oiseaux) %>%
    summarize(nb_form = n()) %>%
    arrange(nb_oiseaux) %>%
    ungroup() %>%
    glimpse()
  df3 <- df1 %>%
    group_by(source, id_form) %>%
    summarize(nb = n()) %>%
    group_by(source) %>%
    summarize(nb_forms = n()) %>%
    ungroup() %>%
    glimpse()
  df4 <- df2 %>%
    left_join(df3, by = c("source")) %>%
    mutate(nb_form = round((nb_form/nb_forms) * 1000)) %>%
    filter(nb_oiseaux < 40)
  gg <- ggplot(df4, aes(x = nb_oiseaux, y = nb_form, fill = source)) +
    geom_bar(stat = "identity", position = position_dodge())
  gg <- gg +
    theme_stata() + scale_fill_stata()
  df5 <- df4 %>%
    filter(source == "epoc")
  moy <- mean(df5$nb_oiseaux)
  ec <- sd(df5$nb_oiseaux)
  label <- sprintf('epoc : %.1f#%.1f', moy, ec)
  df5 <- df4 %>%
    filter(source == "epoc_odf")
  moy <- mean(df5$nb_oiseaux)
  ec <- sd(df5$nb_oiseaux)
  label <- sprintf('%s\nepoc_odf : %.1f#%.1f', label, moy, ec)
  df5 <- df4 %>%
    filter(source == "stoc")
  moy <- mean(df5$nb_oiseaux)
  ec <- sd(df5$nb_oiseaux)
  label <- sprintf('%s\nstoc : %.1f#%.1f', label, moy, ec)
  x_rng <- range(df4$nb_oiseaux)
  y_rng <- range(df4$nb_form)
  gg <- gg +
    annotate(
      geom = "text", y = y_rng[2], x = x_rng[2],
      label = label, hjust = 1, vjust = 1, size = 4
    )
  print(gg)
}
#
# la diversité (nombre d'espèces) sur un point
extract_stat_diversite <- function(df1) {
  carp()
  library(lubridate)
  library(ggplot2)
  library(ggthemes)
  df2 <- df1 %>%
    distinct(id_form, source, espece) %>%
    group_by(id_form, source) %>%
    summarize(nb_especes = n()) %>%
    group_by(source, nb_especes) %>%
    summarize(nb_form = n()) %>%
    arrange(nb_especes) %>%
    ungroup() %>%
    glimpse()
  df3 <- df1 %>%
    group_by(source, id_form) %>%
    summarize(nb = n()) %>%
    group_by(source) %>%
    summarize(nb_forms = n()) %>%
    ungroup() %>%
    glimpse()
  df4 <- df2 %>%
    left_join(df3, by = c("source")) %>%
    mutate(nb_form = round((nb_form/nb_forms) * 1000))
  gg <- ggplot(df4, aes(x = nb_especes, y = nb_form, fill = source)) +
    geom_bar(stat = "identity", position = position_dodge())
  gg <- gg +
    theme_stata() + scale_fill_stata()
  df5 <- df4 %>%
    filter(source == "epoc")
  moy <- mean(df5$nb_especes)
  ec <- sd(df5$nb_especes)
  label <- sprintf('epoc : %.1f#%.1f', moy, ec)
  df5 <- df4 %>%
    filter(source == "epoc_odf")
  moy <- mean(df5$nb_especes)
  ec <- sd(df5$nb_especes)
  label <- sprintf('%s\nepoc_odf : %.1f#%.1f', label, moy, ec)
  df5 <- df4 %>%
    filter(source == "stoc")
  moy <- mean(df5$nb_especes)
  ec <- sd(df5$nb_especes)
  label <- sprintf('%s\nstoc : %.1f#%.1f', label, moy, ec)
  x_rng <- range(df4$nb_especes)
  y_rng <- range(df4$nb_form)
  gg <- gg +
    annotate(
      geom = "text", y = y_rng[2], x = x_rng[2],
      label = label, hjust = 1, vjust = 1, size = 4
    )
  print(gg)
}

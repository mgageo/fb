# <!-- coding: utf-8 -->
#
# quelques fonctions pour le suivi martinet/hirondelle du geoca
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
mga  <- function() {
  source("geo/scripts/geoca.R");
}
# les traitements à enchainer suite à une mise à jour
# source("geo/scripts/geoca.R");jour()
jour <- function(force = TRUE) {
  carp()
  library(tidyverse)
}
#
# les variables globales
Drive <- substr( getwd(),1,2)
baseDir <- sprintf("%s/web", Drive)
cfgDir <- sprintf("%s/web/geo/%s", Drive, "GEOCA")
varDir <- sprintf("%s/bvi35/CouchesGeoca", Drive);
texDir <- sprintf("%s/web/geo/%s", Drive, "GEOCA")
dir.create(varDir, showWarnings = FALSE, recursive = TRUE)
dir.create(texDir, showWarnings = FALSE, recursive = TRUE)
ignDir <- sprintf("%s/bvi35/CouchesIGN", Drive);
webDir <- sprintf("%s/web.heb/bv/geoca", Drive);
dir.create(webDir, showWarnings = FALSE, recursive = TRUE)
#
# les bibliothèques
setwd(baseDir)
source("geo/scripts/mga.R");
source("geo/scripts/misc.R");
source("geo/scripts/misc_apivn.R");
source("geo/scripts/misc_biolo.R");
source("geo/scripts/misc_db.R");
source("geo/scripts/misc_ftp.R");
source("geo/scripts/misc_geocode.R");
source("geo/scripts/misc_json.R"); # de json en data frame
source("geo/scripts/misc_psql.R");
source("geo/scripts/misc_tex.R");
source("geo/scripts/geoca_apivn.R"); # les données en version api
source("geo/scripts/geoca_faune.R"); # les données version export en format json
#
# les commandes permettant le lancement
DEBUG <- FALSE
if ( interactive() ) {
  carp("interactive")
# un peu de nettoyage
  graphics.off()
  par(mar = c(0,0,0,0), oma = c(0,0,0,0))
} else {
  carp("console")
}

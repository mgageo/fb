# <!-- coding: utf-8 -->
#
# quelques fonctions pour cheveche35
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# la partie Google Drive
# ===============================================================
# https://googledrive.tidyverse.org/
# source("geo/scripts/cheveche35.R"); drive_jour_get()
drive_jour <- function(test=1) {
  carp()
  drive_jour_get()
}
#
# une fenêtre firefox doit s"ouvrir !
# source("geo/scripts/cheveche35.R"); drive_jour_get()
drive_jour_get <- function(test=1) {
  carp()
  library(googledrive)
  drive_auth("univasso35@gmail.com")
  drive_tex_get(path = "~/Chevêche/GoogleDrive", recursif = FALSE)
}

# <!-- coding: utf-8 -->
#
# la partie données faune-bretagne
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================

#
#
faune_secteur <- function() {
  Log(sprintf("faune_secteur()"))
  library(tidyr)
  spdf <- faune_lire()

  df <- spdf@data
  df$COMMENT <- as.character(df$COMMENT)
  df <- df[grepl("^BMSM2017", df$COMMENT),]
  df1 <- extract(df, COMMENT, c("code", "proto", "especes", "horaire", "point", "Secteur"), "(.*?);(.*?);(.*?);(.*?);(.*?);([^;]+)")
  print(df1[, c("secteur", "Secteur")])
}
#
# lecture des données, export de faune-bretagne
faune_lire <- function() {
  Log(sprintf("faune_lire()"))
  if ( exists("faune.spdf") ) {
#    return(faune.spdf)
  }
  spdf <- faune_lire_xls()
  spdf@data$observateur <- sprintf("%s %s", spdf@data$SURNAME, spdf@data$NAME)
# la date
  spdf@data$d <- as.Date(as.numeric(as.character(spdf@data$DATE)), origin="1899-12-30")
  inconnuDF <- subset(spdf@data, is.na(spdf@data$d))
  if ( length(inconnuDF$d) > 0 ) {
    Log(sprintf("faune_lire() date invalide"))
    print(head(inconnuDF))
    stop("faune_lire")
  }
  date_from <- as.Date("01/09/2017", "%d/%m/%Y")
  date_to <- as.Date("30/09/2017", "%d/%m/%Y")
  Log(sprintf("faunelire() date_from:%s date_to:%s", date_from, date_to))
  spdf <- spdf[spdf@data$d >= date_from & spdf@data$d <= date_to & spdf@data$NAME == "Beaufils",]
  spdf <- faune_lire_secteur(spdf)
  faune.spdf <<- spdf
  return(invisible(spdf))
}
#
# que les données secteur
faune_lire_secteur <- function(spdf) {
  library("raster")
  zone.spdf <- fonds_zone_lire()
  print(head(zone.spdf@data))
  spdf@data$secteur <- over(spdf, zone.spdf)$secteur
  df <- spdf@data
  inconnuDF <- subset(df, is.na(df$secteur))
  if ( length(inconnuDF$secteur) > 0 ) {
    Log(sprintf("faune_lire_secteur() hors secteur %s", nrow(inconnuDF)))
    print(head(inconnuDF, 100))
#    stop("faune_lire_atlas()")
  }
  spdf <- spdf[!(is.na(spdf@data$secteur)),]
  Log(sprintf("faune_lire_secteur() nrow: %d", nrow(spdf@data)))
#  stop("***")
  return(invisible(spdf))
}



faune_lire_xls <- function(fic="bmsm_donnees.xls") {
  library("xlsx")
  library("raster")
  dsn <- sprintf("%s/%s", fbDir, fic)
  Log(sprintf("faune_lire_xls() dsn: %s", dsn))
  df <- read.xlsx2(dsn, 1, header=TRUE, colClasses=NA)
#  print(head(df[,c('COORD_LAT', 'COORD_LON')]))
  df <- df[, c('ID_SIGHTING', 'ID_SPECIES', 'NAME_SPECIES', 'FAMILY_NAME', 'DATE', 'PLACE', 'MUNICIPALITY', 'INSEE', 'COORD_LAT', 'COORD_LON', 'COMMENT', 'ESTIMATION_CODE', 'TOTAL_COUNT', 'ATLAS_CODE', 'PRECISION', 'NAME', 'SURNAME', 'UPDATE_DATE')]
# la première ligne en moins
  df <- df[-1,]
# les noms d'espèces
  df$NAME_SPECIES <- gsub(" \\(.*", "", df$NAME_SPECIES)
  df$NAME_SPECIES[df$NAME_SPECIES == "Pigeon biset domestique"] <- "Pigeon biset"
# les données négatives
  df1 <- subset(df, TOTAL_COUNT == 0);
  print(sprintf("faune_lire_xls() TOTAL_COUNT==0 nb : %s", nrow(df1)))
#  print(head(df1[, c("NAME_SPECIES")], 20)); stop("***")
  df <- subset(df, TOTAL_COUNT != 0)
  df1 <- subset(df, ESTIMATION_CODE == "\xD7");
  print(sprintf("faune_lire_xls() ESTIMATION_CODE nb : %s", nrow(df1)))
  df <- subset(df, ESTIMATION_CODE != "\xD7")
  df1 <- subset(df, grepl("Chev.*Ath",NAME_SPECIES));
  if ( nrow(df1) > 0 ) {
    print(head(df1,20)); stop("***")
  }
  df$d <- as.Date(as.numeric(as.character(df$DATE)), origin="1899-12-30")
  inconnuDF <- subset(df, is.na(df$d))
  if ( length(inconnuDF$d) > 0 ) {
    Log(sprintf("faune_lire() date invalide"))
    print(head(inconnuDF))
    stop("faune_lire_xls")
  }

# transformation en spatial
  df [,"COORD_LAT"] <- sapply(df[,"COORD_LAT"], as.character)
  df [,"COORD_LAT"] <- sapply(df[,"COORD_LAT"], as.numeric)
  df [,"COORD_LON"] <- sapply(df[,"COORD_LON"], as.character)
  df [,"COORD_LON"] <- sapply(df[,"COORD_LON"], as.numeric)
  bug.df <- subset(df, COORD_LAT < 48)
  if ( nrow(bug.df) > 0 ) {
    print(head(bug.df))
    stop("***")
  }
  coordinates(df) = ~ COORD_LON + COORD_LAT
#  print(sapply(df, class))
  spdf <- SpatialPointsDataFrame(df,data.frame(df[,]))
  proj4string(spdf) <- CRS("+init=epsg:4326")
  spdf <- spTransform(spdf, CRS("+init=epsg:2154"))
  Log(sprintf("faune_lire_xls() nrow: %d", nrow(spdf@data)))

#  stop("====")
  return(spdf)
}

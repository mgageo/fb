# <!-- coding: utf-8 -->
#
# quelques fonctions pour gci35
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# la partie bilan mont-saint-michel, fichier de Régis Morel
# ===============================================================
# https://googledrive.tidyverse.org/
#
# source("geo/scripts/gci35.R"); bdmsm_jour()
bdmsm_jour <- function() {
  carp()
  library(readxl)
  library(writexl)
  library(tidyverse)
  library(janitor)
  dsn <- sprintf("%s/GCI_BDMSM.xlsx", texDir)
  sheet <- "Comptages concertés BDMSM"
  carp("sheet: %s", sheet)
  df <- read_excel(dsn, sheet) %>%
    clean_names() %>%
    arrange(date, secteur) %>%
    glimpse()
  dsn <- sprintf("%s/gci35_donnees.xlsx", texDir)
  sheet <- "201805"
  df1 <- read_excel(dsn, sheet) %>%
    glimpse()
  df1 <- df1[0, ]
  for (i in 1:nrow(df)) {
    df1 <- df1 %>%
      add_row(
        date = df[[i, "date"]]
        , libelle  = df[[i, "observateurs"]]
        , secteur  = sprintf("secteur %s", df[[i, "secteur"]])
      )
    df1 <- add_row(df1, libelle = "Total couples", nb = df[[i, "couples"]])
    df1 <- add_row(df1, libelle = "Total mâles seuls avec indice de repro", nb = df[[i, "m_avec"]])
    df1 <- add_row(df1, libelle = "Total femelles seules avec indice de repro", nb = df[[i, "f_avec"]])
    df1 <- add_row(df1, libelle = "Total femelles seules sans indice de repro", nb = df[[i, "f_sans"]])
    df1 <- add_row(df1, libelle = "Total mâles seuls sans indice de repro", nb = df[[i, "m_sans"]])
    df1 <- add_row(df1, libelle = "Total indéterminés", nb = df[[i, "inder"]])
    df1 <- add_row(df1, libelle = "Total poussins", nb = df[[i, "nombre_de_poussins"]])
  }
  misc_print(df1)
  dsn <- sprintf("%s/BDMSM.xlsx", texDir)
  writexl::write_xlsx(df1, dsn)
}

# <!-- coding: utf-8 -->
#
# quelques fonctions pour générer les fichiers tex
# auteur: Marc Gauthier
#
#
#
# carte choropleth
#
# http://stackoverflow.com/questions/13762793/plotting-choropleth-maps-from-kml-data-using-ggplot2
# en direct de https://r-forge.r-project.org/scm/viewvc.php/pkg/R/carte.qual.R?view=markup&root=rgrs
choroplethSP <- function(sp, varname, cut.breaks) {
  require(sp)
  classes <- cut.breaks
  couleurs <- heat.colors(length(classes))
#   pie(rep(1,6),col=heat.colors(6))
  couleurs <- rev(couleurs)
  sp@data$sym <- "white"
  classe <- vector()
  for (x in 1:(length(classes))) {
    sp@data[sp@data[, varname] > classes[x], "sym"] <- couleurs[x]
    classe[x] = sprintf("> %s", classes[x])
  }
  plot(sp, col=sp@data$sym, border = "black")
  legend ('bottomleft', legend=classe, col=couleurs, lty=1, lwd=10)
}
choropleth2 <- function(sp, data, varname, sp.key="id", data.key="id", cut.breaks) {
  require(sp)
# Création d'une variable temporaire pour récuperer l'ordre initial après le merge (Joel Gombin)
  sp@data$rgrs.temp.sort.var <- 1:nrow(sp@data)
  sp@data <- merge(x=sp@data, y=data, by.x=sp.key, by.y=data.key, all.x=TRUE, all.y=FALSE, sort=FALSE)
  sp@data <- sp@data[order(sp@data$rgrs.temp.sort.var, na.last = TRUE),]

#  sp@data$freq[is.na(sp@data$freq)] <- 0
  sp@data[, varname][is.na(sp@data[, varname])] <- 0
  classes <- cut.breaks
#  sp@data$classe <- factor(cut(sp@data$freq,classes,include.lowest=TRUE))
#  pal1 <- c("wheat1", "red3")
  couleurs <- heat.colors(length(classes))
#   pie(rep(1,6),col=heat.colors(6))

  couleurs <- rev(couleurs)
#  sp@data$SUPERFICIE <- as.numeric(as.character(sp@data$SUPERFICIE))
#  sp@data$densite <- sp@data$Freq*1000/sp@data$SUPERFICIE
  sp@data$sym <- "white"
  classe <- vector()
  for (x in 1:(length(classes))) {
    sp@data[sp@data[, varname] > classes[x], "sym"] <- couleurs[x]
    classe[x] = sprintf("> %s", classes[x])
  }
#  Log(head(sp@data))
  plot(sp, col=sp@data$sym, border = "black")
  legend ('bottomleft', legend=classe, col=couleurs, lty=1, lwd=10)
}
choropleth3 <- function(sp, data, varname, sp.key="id", data.key="id") {
  require(sp)
  require(RColorBrewer)
  require(classInt)
# Création d'une variable temporaire pour récuperer l'ordre initial après le merge (Joel Gombin)
  sp@data$rgrs.temp.sort.var <- 1:nrow(sp@data)
  sp@data <- merge(x=sp@data, y=data, by.x=sp.key, by.y=data.key, all.x=TRUE, all.y=FALSE, sort=FALSE)
  sp@data <- sp@data[order(sp@data$rgrs.temp.sort.var, na.last = TRUE),]
#
#  sp@data$Freq[is.na(sp@data$Freq)] <- 0
#  classes <- c(0,5, 10, 50, 100, 500)
#  sp@data$classe <- factor(cut(sp@data$Freq,classes,include.lowest=TRUE))
  pal1 <- c("wheat1", "red3")
# discretisation de la variable variable
  tmp <- data[,c(data.key, varname)]
  tmp.var <- na.omit(sp@data[,varname])
  intervals <- classIntervals(tmp.var, 6, "sd")
# la palette de couleurs
  couleurs <- colorRampPalette(brewer.pal(6, "Greys"))(length(intervals$brks)-1)
#  couleurs <- rev(couleurs)
  plot(sp, col=couleurs, border = "white")
#  sp@data$SUPERFICIE <- as.numeric(as.character(sp@data$SUPERFICIE))
#  sp@data$densite <- sp@data$Freq*1000/sp@data$SUPERFICIE
#  sp@data$sym <- "white"
#  sp@data[sp@data$Freq > 1000,"sym"] <- "green"
#  plot(sp, col=sp@data$sym)
}

choropleth <- function (sp, data, varname, sp.key="id", data.key="id", cut.nb = 6, cut.method= "sd", cut.breaks, palette='Greys') {
  require(sp)
  require(RColorBrewer)
  require(classInt)
  tmp <- data[,c(data.key, varname)]
## Creation d'une variable temporaire pour recuperer l'ordre initial apres le merge (Joel Gombin)
  sp@data$rgrs.temp.sort.var <- 1:nrow(sp@data)
  sp@data <- merge(sp@data, tmp, by.x=sp.key, by.y=data.key, all.x=TRUE, all.y=FALSE, sort=FALSE)
  sp@data <- sp@data[order(sp@data$rgrs.temp.sort.var, na.last = TRUE),]
# on enlève les lignes avec na
  tmp.var <- na.omit(sp@data[,varname])
# discretisation de la variable variable
  intervals <- classIntervals(tmp.var, cut.nb, cut.method, fixedBreaks=cut.breaks)
  value <- findInterval(sp@data[, varname], intervals$brks, all.inside = TRUE)
# la palette de couleurs
  couleurs <- colorRampPalette(brewer.pal(6, palette))(length(intervals$brks)-1)
  couleurs <- rev(couleurs)
  plot(sp, col=couleurs, border = "white")
}

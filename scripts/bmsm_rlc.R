# <!-- coding: utf-8 -->
#
# quelques fonctions pour le suivi RLC BMSM
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
# http://stackoverflow.com/questions/19226816/how-can-i-view-the-source-code-for-a-function
# http://informatique-mia.inra.fr/r4ciam/node/197 la ligne de commande
# https://rpubs.com/markpayne/132491 fonctions spatiales
mga  <- function() {
  source("geo/scripts/bmsm_rlc.R");
}

#
#
# les commandes permettant le lancement
Drive <- substr( getwd(),1,2)
#
# quelques variables globales
baseDir <- sprintf("%s/web", Drive)
bviDir <- sprintf("%s/bvi35", Drive);
bmsmDir <- sprintf("%s/bvi35/CouchesBMSM", Drive);
varDir <- sprintf("%s/bvi35/CouchesBMSM", Drive);
pdfDir <- sprintf("%s/bvi35/CouchesBMSM", Drive);
#dir.create(cfgDir, showWarnings = FALSE, recursive = TRUE)
dir.create(varDir, showWarnings = FALSE, recursive = TRUE)
setwd(baseDir)
source("geo/scripts/mga.R");
source("geo/scripts/misc.R");
source("geo/scripts/misc_biolo.R");
source("geo/scripts/misc_fb.R");
source("geo/scripts/misc_fonds.R");
source("geo/scripts/misc_happign.R");
source("geo/scripts/misc_tex.R");
source("geo/scripts/couches_gdal.R");
source("geo/scripts/rlc_carto.R");
source("geo/scripts/rlc_faune.R");
source("geo/scripts/rlc_fonds.R");
source("geo/scripts/rlc_secteur45.R"); # la jointure des secteurs 4 & 5
source("geo/scripts/rlc_vanneaux.R"); # pour les vanneaux en 2025
DEBUG <- FALSE
if ( interactive() ) {
  Log(sprintf("bmsm_rlc.R interactive"))
# un peu de nettoyage
  graphics.off()
} else {
  Log(sprintf("bmsm_rlc.R console"))
#  fiches();
}

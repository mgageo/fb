# <!-- coding: utf-8 -->
#
# parallel
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# https://affini-tech.com/blog/r-package-doparallel/
#
parallel_download <- function(dsn, force = FALSE) {
  library(tidyverse)
  library(doParallel)
  carp("dsn: %s", dsn)
  cl <- detectCores() %>% -1 %>% makeCluster
  registerDoParallel(cores=cl)
  files.df <- rio::import(dsn)
  carp("nrow: %s", nrow(files.df))
  for (i in 1:nrow(files.df)) {
    carp("i: %s/%s", i, nrow(files.df))
    parallel_download_file(files.df[i, "url"], files.df[i, "dsn"], force = force)
  }
}
parallel_download_file <- function(url, dsn, force) {
  download_retry(url = url, dsn = dsn, force = force)
}
#
# en direct de https://rdrr.io/bioc/recount/src/R/download_retry.R
download_retry <- function(url, dsn = basename(url), force = FALSE, mode = "wb", N.TRIES = 3L, ...) {
  N.TRIES <- as.integer(N.TRIES)
  stopifnot(length(N.TRIES) == 1L, !is.na(N.TRIES))
  stopifnot(N.TRIES > 0L)
  if (file.exists(dsn) & file.info(dsn)$size == 0) {
    file.remove(dsn)
  }
  if( file.exists(dsn) & force == FALSE) {
    return()
  }
  while (N.TRIES > 0L) {
    result <- tryCatch(
#      suppressWarnings(download.file(url, destfile, mode = "wb")),
      download.file(url, dsn, mode = "wb", quiet = TRUE),
      error = identity
    )
    if (!inherits(result, "error")) {
      break
    }
    carp("retry: %s url: %s", N.TRIES, url)
## Wait between 0 and 2 seconds between retries
    Sys.sleep(runif(n = 1, min = 2, max = 5))
    N.TRIES <- N.TRIES - 1L
  }
  if (N.TRIES == 0L) {
    if (file.exists(dsn)) {
      file.remove(dsn)
    }
  }
  invisible(result)
}

# <!-- coding: utf-8 -->
#
# quelques fonctions de Distance Sampling
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# http://rstudio-pubs-static.s3.amazonaws.com/131401_731f33bca563490082d28554d564ae4d.html
# https://github.com/r-spatial/sf/issues/1233
#
#
#
# source("geo/scripts/ds.R"); spatstat_rmatern()
spatstat_rmatern <- function() {
  carp()
  if(!require("pacman")) install.packages("pacman")
  pacman::p_load(sf, dplyr, mapview, spatstat, maptools, devtools)
# génération aléatoire identique d'une fois sur l'autre
  set.seed(4567)
  win <- owin(c(-50, 50), c(-25, 25))
# 1 à l'hectare
  Y <- rMaternII(kappa = 1, win = win, r = 5.6, stationary = TRUE)
# 10 à l'hectare
#  Y <- rMaternII(kappa = 1, win = win, r = 1.803, stationary = TRUE)
  nc <- st_as_sf(Y) %>%
    glimpse()
  plot(st_geometry(nc))
  points.sf <- nc %>%
    filter(st_geometry_type(.) %in% c("POINT")) %>%
    glimpse()
  points.df <- sf::st_coordinates(points.sf) %>%
    as_tibble() %>%
    setNames(c("x","y"))
  dsn <- sprintf("%s/%s.Rds", varDir, "spatstat_rmatern")
  carp("dsn: %s", dsn)
  saveRDS(points.df, file = dsn)
}
# source("geo/scripts/ds.R"); spatstat_zone()
spatstat_zone <- function() {
  library(ggplot2)
  library(ggrepel)
# https://semba-blog.netlify.app/10/29/2018/animating-oceanographic-data-in-r-with-ggplot2-and-gganimate/
#  library(gganimate)
#  library(transformr)
  library(sf)
#  library(hrbrthemes)
#  theme_set(theme_ipsum())
  carp()
  set.seed(4567)
# 1 hectare = 10 000 m2
  xsize <- 500
  ysize <- 250
# 500 * 250 * 4 => 50 hectares
  x <- st_point(c(0, 0))
  bbox <- st_bbox(x)
  bbox <- bbox + c(xsize, ysize, -xsize, -ysize)
  bbox.sfc <- st_as_sfc(bbox)
  gg <- ggplot() +
    geom_sf(data = bbox.sfc)
#  gg <- gg +
#    geom_sf(data = x)
#
# les bandes et disques
  classes <- c(25, 100, 200)
  disques.sf <- tibble()
  bandes.sf <- tibble()
  for (classe in classes) {
# pour les disques
    classe.sfg <- st_buffer(x, classe)
    df <- tibble(classe = classe, methode = "point")
    nc <- st_as_sf(df, geom = st_sfc(classe.sfg))
    disques.sf <- rbind(disques.sf, nc)
# pour les bandes
    bb <- st_bbox(x)
    bb <- bb + c(xsize, classe, -xsize, -classe)
    bb.sfc <- st_as_sfc(bb)
    df <- tibble(classe = classe, methode = "transect")
    nc <- st_as_sf(df, geom = bb.sfc)
    bandes.sf <- rbind(bandes.sf, nc)
  }
#
# les oiseaux
  dsn <- sprintf("%s/%s.Rds", varDir, "spatstat_rmatern")
  sigma <- 100
  points.df <- readRDS(file = dsn) %>%
    mutate(x = x * 10) %>%
    mutate(y = y * 10) %>%
    as_tibble() %>%
    rowwise() %>%
    mutate(alea = runif(1))

#
# pour les transects
  gg1 <- gg +
    geom_sf(data = bandes.sf, colour = "blue", fill = NA)
  bb <- st_bbox(x)
    bb <- bb + c(xsize, 0, -xsize, 0)
  transect.sfc <- st_as_sfc(bb)
  gg1 <- gg1 +
    geom_sf(data = transect.sfc, color = "green", lwd = 2)

# la distance au transect
  df1 <- points.df %>%
    mutate(d = abs(y)) %>%
    mutate(cp = exp(-d^2 / (2 * sigma^2))) %>%
# les oiseaux détectés
    mutate(couleur = ifelse(cp >= alea, "red", "black")) %>%
    glimpse()
  nc1 <- st_as_sf(df1, coords = c("x", "y"), remove = FALSE)
  glimpse(df1)
  gg1 <- gg1 +
    geom_sf(data = nc1, col = nc1$couleur, fill = NA)
  plot(gg1)
#
# pour les points
  gg2 <- gg +
    geom_sf(data = disques.sf, colour = "blue", fill = NA)

# la distance au point
  df2 <- points.df %>%
    mutate(d = sqrt(x^2 + y^2)) %>%
    mutate(cp = exp(-d^2 / (2 * sigma^2))) %>%
# les oiseaux détectés
    mutate(couleur = ifelse(cp >= alea, "red", "black")) %>%
    glimpse()
  nc2 <- st_as_sf(df2, coords = c("x", "y"), remove = FALSE)
  glimpse(df2)
  gg2 <- gg2 +
    geom_sf(data = nc2, col = nc2$couleur, fill = NA)
  plot(gg2)
  return()
  nc1 <- rbind(disques.sf, bandes.sf)
  nc1$nb_points <- lengths(st_intersects(nc1, points.sf))
  df1 <- nc1 %>%
    mutate(surface = as.numeric(round(st_area(.), 0))) %>%
    st_drop_geometry() %>%
    mutate(densite = round((nb_points * 10000) / surface, digit = 2)) %>%
    mutate(surface = round(surface / 10000, digit = 2)) %>%
    glimpse()
  misc_print(df1)
}

# <!-- coding: utf-8 -->
#
# quelques fonctions pour extraire de https://pecbms.info/trends-and-indicators/species-trends/
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#

# source("geo/scripts/odf.R");pecbms_lire()
pecbms_lire <- function(force = FALSE) {
  library(tidyverse)
  library(janitor)
  dsn <- sprintf("%s/pecbms/europe-indicesandtrends-till2019.xlsx", webDir)
  df <- rio::import(dsn) %>%
    clean_names() %>%
    glimpse()
  carp("dsn: %s", dsn)
  return(invisible(df))
}
# source("geo/scripts/odf.R");pecbms_oiseaux()
pecbms_oiseaux <- function() {
  df1 <- pecbms_lire() %>%
    group_by(latin = pecbms_species_name) %>%
    summarize(nb = n())
  df2 <- oiseaux_lire() %>%
    glimpse()
  df3 <- df1 %>%
    filter(latin %notin% df2$Pecbms) %>%
    glimpse()
  misc_print(df3)
}
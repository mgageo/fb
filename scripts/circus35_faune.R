# <!-- coding: utf-8 -->
#
# quelques fonctions pour les données faune
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
fauneDir <- sprintf("%s/faune", varDir)
dir.create(fauneDir, showWarnings = FALSE, recursive = TRUE)
#
## les traitements journaliers
#
# les traitements suite à une actualisation de faune-bretagne
# source("geo/scripts/circus35.R");faune_jour(force = TRUE)
faune_jour <- function(force = FALSE) {
  carp("début")
  biolo_fb(force = TRUE)
  faune_groupes_export(force = force)
  faune_exports_lire(force = force)
  faune_exports_sf(force = force)
  faune_geojson(force = force)
}
#
# que le busard cendré
# source("geo/scripts/circus35.R");faune_espece_donnees_get();
# BSM = 163
# BC = 165
faune_espece_jour <- function(force = TRUE) {
  carp()
  library(tidyverse)
  library(janitor)
  library(rio)
  for (espece in c(163, 165)) {
    faune_espece_donnees_get(espece = espece, force = force)
    faune_espece_pdf(espece = espece, force = force)
  }
  return(invisible(df))
}
faune_espece_donnees_get <- function(espece = 163, force = TRUE) {
  carp()
  library(tidyverse)
  library(janitor)
  library(rio)
  dsn <- sprintf("%s/faune_espece_donnees_%s.xlsx", varDir, espece)
  df <- biolo_export_espece(espece = espece , dsn = dsn, force = force)
  return(invisible(df))
}
# source("geo/scripts/circus35.R");faune_espece_pdf();
faune_espece_pdf <- function(espece = 165, force = TRUE) {
  carp()
  dsn <- sprintf("%s/faune_espece_donnees_%s.xlsx", varDir, espece)
  pdf <- pdf_faune_nicheurs(dsn)
  suffixe <- sprintf("_%s.pdf", espece)
  dsn <- gsub(".pdf", suffixe, pdf)
  carp("dsn:%s", dsn)
  file.copy(pdf, dsn, overwrite = TRUE )
  return(invisible())
}
# source("geo/scripts/circus35.R");faune_groupes_export()
faune_groupes_export <- function(force = TRUE) {
  carp()
  library(tidyverse)
#  glimpse(WGS84_ullr); exit;
  carp("début")
  txt <- format(Sys.time(), format = "%A %d %B %Y %H:%M")
  dsn <- sprintf("%s/faune_groupes_export.txt", varDir)
  writeLines(txt, dsn)
  df <- read.table(text="code,libelle
13,accipiters
15,faucons
", header=TRUE, sep=",", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="")
  for(i in 1:nrow(df) ) {
    code <- df$code[i]
    if ( grepl('^#', code) ) {
      next
    }
    libelle <- df$libelle[i]
    dsn <- sprintf("%s/faune_export_%s.xlsx", fauneDir, libelle)
#    biolo_export_groupe(groupe = code, dsn = dsn, depuis = "01.01.2024", force = TRUE)
    biolo_export_tpl(tpl = "groupe", groupe = code, dsn = dsn, debut = "01.01.2024", force = TRUE)
    carp('dsn: %s', dsn)
#    dsn <- sprintf("%s/faune_export_%s.json", fauneDir, libelle)
#    biolo_export_groupe(groupe = code, dsn = dsn, depuis = "01.01.2024", force = TRUE, Format = "JSON")
#    carp('dsn: %s', dsn)
  }
}
#
# lecture des exports faune
#
# source("geo/scripts/circus35.R"); df <- faune_exports_lire(force = TRUE)
faune_exports_lire <- function(force = FALSE, libelle = "rapaces") {
  carp("début")
  library(janitor)
  library(rio)
  library(tidyverse)
  if(exists("faune_rapaces.df") & force == FALSE) {
    return(invisible(faune_rapaces.df))
  }
  files <- list.files(fauneDir, pattern = "\\.xlsx$", full.names = TRUE, ignore.case = TRUE)
  df1 <- data.frame()
  for (i in 1:length(files) ) {
    file <- files[i]
    df <- import(file)
    df <- df[-1,]
    df1 <- rbind(df1, df)
  }
  dsn <- sprintf("%s/faune/faune_export_%s.xlsx", varDir, libelle)
  df1 <- df1 %>%
    clean_names() %>%
    remove_empty(c("rows")) %>%
#    filter(precision %in% c("Point", "Localisation précise")) %>%
    mutate(d = excel_numeric_to_date(as.numeric(date))) %>%
    mutate(nom = sprintf('%s %s', surname, name)) %>%
    mutate(annee = date_year) %>%
    mutate(aaaammjj = strftime(d, format="%Y-%m-%d")) %>%
    mutate(observateur = sprintf("%s %s", surname, name)) %>%
    mutate(commune = sprintf("%s %s", insee, municipality)) %>%
    mutate(nom_francais = name_species) %>%
    rename(longitude_wgs84 = coord_lon) %>%
    rename(latitude_wgs84 = coord_lat)
  carp("fin nrow: %s", nrow(df1))
# la normalisation du nom d'espèce
  df1 <- df1 %>%
# pour les noms latins entre parenthèses
    mutate(nom_francais = gsub("\\s*\\(.*$", "", nom_francais))
  faune_rapaces.df <<- df1
  return(invisible(df1))
}
#
# source("geo/scripts/circus35.R"); nc1 <- faune_exports_sf(force = TRUE)
faune_exports_sf <- function(force = FALSE, libelle = "rapaces") {
  library(tidyverse)
  library(sf)
  carp("début")
  if(exists("faune_rapaces.sf") & force == FALSE) {
    return(invisible(faune_rapaces.sf))
  }
  df1 <- faune_exports_lire(force) %>%
    filter(annee == "2024") %>%
#    filter(grepl("^Busard", name_species)) %>%
    filter(precision %in% c("Point", "Localisation précise"))
  names.df <- tribble(
~name_species,~espece,
"Busard Saint-Martin","BSM",
"Busard cendré","BC",
"Élanion blanc","EB")
  df <- df1 %>%
    left_join(names.df, by = c("name_species")) %>%
    filter(! is.na(espece)) %>%
    glimpse()
  for (a in c("longitude_wgs84", "latitude_wgs84") ) {
    df[, a] <- sapply(df[, a], as.numeric)
  }
  for ( a in c("atlas_code", "date_year") ) {
    df[, a] <- sapply(df[, a], as.integer)
  }
  nc1 <- st_as_sf(df, coords = c("longitude_wgs84", "latitude_wgs84"), crs = 4326, remove=FALSE) %>%
    st_transform(2154)
  emprise.sf <- grille_emprise_lire() %>%
    st_transform(2154)
  nc2 <- nc1 %>%
    st_filter(emprise.sf)
  faune_rapaces.sf <<- nc2
  return(invisible(nc2))
}
#
# source("geo/scripts/circus35.R"); nc1 <- faune_exports_trace_sf(force = TRUE)
faune_exports_trace_sf <- function(force = FALSE, libelle = "rapaces") {
  library(tidyverse)
  library(sf)
  carp("début")
  df1 <- faune_exports_lire(force) %>%
    filter(annee == "2024") %>%
    filter(grepl("^Busard", name_species)) %>%
    filter(trace != "") %>%
    glimpse()
  nc1 <- st_as_sf(df1, geometry = st_as_sfc(df1$trace, crs = st_crs(4326))) %>%
    st_transform(2154) %>%
    glimpse()
  nc3 <- nc1 %>%
    mutate(trigramme = sprintf("%s%s", substr(surname, 1, 1), substr(name, 1, 2))) %>%
    mutate(espece = dplyr::recode(name_species,
      "Busard Saint-Martin" = "BSM",
      "Busard cendré" = "BC",
      )) %>%
    filter(espece == "BC") %>%
    filter(trigramme == "ABe") %>%
    dplyr::select(name = espece, id_sighting, trigramme, aaaammjj) %>%
    st_transform(4326)
  dsn <- sprintf("%s/circus35fb_trace.geojson", webDir)
  st_write(nc3, dsn, delete_dsn = TRUE)
  carp("dsn: %s", dsn)
  leaflet_polygons(nc3)
#  leaflet_point()
  return()
  emprise.sf <- grille_emprise_lire() %>%
    st_transform(2154)
# https://stackoverflow.com/questions/69224441/find-intersection-between-multilinestring-and-polygons-sf-r
  nc2 <- nc1 %>%
    st_intersection(emprise.sf) %>%
    glimpse()
}
# en format geojson
# source("geo/scripts/circus35.R");faune_geojson(force = TRUE)
faune_geojson <- function(force = TRUE) {
  library(tidyverse)
  library(sf)
  carp()
  nc1 <- faune_rapaces.sf
  nc3 <- nc1 %>%
    mutate(trigramme = sprintf("%s%s", substr(surname, 1, 1), substr(name, 1, 2))) %>%
    dplyr::select(name = espece, id_sighting, trigramme, aaaammjj, atlas = atlas_code, detail) %>%
    st_transform(4326)
  dsn <- sprintf("%s/circus35fb.geojson", webDir)
  st_write(nc3, dsn, delete_dsn = TRUE)
  carp("dsn: %s", dsn)
  dest <- sprintf("circus35/circus35fb.geojson")
  ftp_upload(site = "atlasnw", local = dsn, dest = dest)
  dest <- sprintf("www/%s", dest)
  ftp_upload(site = "always_mga", local = dsn, dest = dest)
  names.df <- nc3 %>%
    st_drop_geometry() %>%
    group_by(name) %>%
    summarize(nb = n())
  for (i in 1:nrow(names.df)) {
    name <- names.df[[i, "name"]]
    nc4 <- nc3 %>%
      filter(name == !!name)
    dsn <- sprintf("%s/%s.geojson", webDir, name)
    st_write(nc4, dsn, delete_dsn = TRUE)
    carp("dsn: %s", dsn)
    dest <- sprintf("circus35/%s.geojson", name)
    ftp_upload(site = "atlasnw", local = dsn, dest = dest)
    dest <- sprintf("www/circus35/%s.geojson", name)
    ftp_upload(site = "always_mga", local = dsn, dest = dest)
  }

  dsn <- sprintf("%s/faune_groupes_export.txt", varDir)
  dest <- sprintf("%s/faune_groupes_export.txt", webDir)
  file.copy(dsn, dest)
  dest <- sprintf("circus35/faune_groupes_export.txt")
  ftp_upload(site = "atlasnw", local = dsn, dest = dest)
  dest <- sprintf("www/circus35/faune_groupes_export.txt")
  ftp_upload(site = "always_mga", local = dsn, dest = dest)
  return(invisible())
}
# <!-- coding: utf-8 -->
#
# quelques fonctions pour produire une grille
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
############################################################################################
#
#
# https://github.com/Groupe-ElementR/cartography/blob/master/R/getGridLayer.R
# http://glaikit.org/tag/spatialite/
#
# require(devtools)
# devtools::install_github("Groupe-ElementR/cartography")

grille_emprise <- function() {
  library(sp)
  library(rgdal)
  library(rgeos)
  library(cartography)
  print(sprintf("grille_emprise"))
  dsn <- sprintf('%s/OSM_communes.shp', dl_dir)
  layer <- ogrListLayers(dsn)
  spdf <- readOGR(dsn, layer=layer, stringsAsFactors=FALSE)
  grille <- getGridLayer0(spdf=spdf,cellsize = 1000)
  grille_spdf <- grille$spdf
  grille_spdf@data$AREA <- as.integer(rgeos::gArea(grille_spdf, byid=TRUE))
  grille_spdf <- grille_spdf[grille_spdf@data$AREA >= 1000*1000*.6, ]
  grille_spdf@data$NUMERO <- seq.int(nrow(grille_spdf@data))
  grille_spdf@data <- grille_spdf@data[c("AREA", "NUMERO")]
  print(summary(grille_spdf))
  print(str(grille_spdf[1:2,]))
  dsn <-  sprintf('%s/grille_emprise.kml', dl_dir)
  print(sprintf("grille_emprise() dsn:%s", dsn))
  kml_spdf <- spTransform(grille_spdf, CRS("+proj=longlat +datum=WGS84"))
  writeOGR(kml_spdf, dsn, layer="grille_emprise", driver="KML", overwrite_layer=TRUE)
  if ( interactive() ) {
    plot(spdf, border= "black", add=FALSE, lwd=10, lty = "dotted" )
    plot(grille_spdf, border="black", add=TRUE, lwd=3)
    text(coordinates(grille_spdf), labels=grille_spdf@data$NUMERO, cex=3, col="black")
  }
}
#
# alignement sur un multiple des coordonnées Lambert
grille_lambert <- function(spdf, pas = 1000) {
  library(sp)
  library(rgdal)
  library(cartography)
  print(sprintf("grille_lambert() pas:%s", pas))
  print(sprintf("grille_lambert() version cartography :%s", packageVersion("cartography")))
  grille <- getGridLayer2(spdf, pas)
  grille.spdf <- grille$spdf
  grille.spdf@data$AREA <- as.integer(rgeos::gArea(grille.spdf, byid=TRUE))
#  centres <- rgeos::gCentroid(grille.spdf, byid=TRUE)
  grille.spdf <- grille.spdf[grille.spdf@data$AREA > pas*pas*.6, ]
  grille.spdf@data$NUMERO <- seq.int(nrow(grille.spdf@data))
# que les colonnes utiles
  grille.spdf@data <- grille.spdf@data[c("AREA", "NUMERO")]
#  print(summary(grille.spdf))
#  print(str(grille.spdf[1:2,]))
  dsn <-  sprintf('%s/grille_lambert_%s', varDir, pas)
  grille_sauve(grille.spdf, dsn)

  if ( ! interactive() ) {
    pdf("grille.pdf")
  }
  plot(spdf, border= "black", add=FALSE, lwd=10 )
  plot(grille.spdf, border="black", add=TRUE, lwd=3)
  text(coordinates(grille.spdf), labels=grille.spdf@data$NUMERO, cex=0.3, col="black")
  if ( ! interactive() ) {
    dev.off()
  }
  return(grille.spdf)
}
#
# alignement sur un multiple des coordonnées Lambert
grille_lambert2 <- function(spdf, cellsize = 1000, align = TRUE, offset=FALSE) {
  library(sp)
  library(rgdal)
  library(rgeos)
  carp(" cellsize:%s", cellsize)
  boundingBox <- bbox(spdf)
  carp(" extent %s",  proj4string(spdf))
  print(extent(spdf))
  if (align == TRUE) {
    rounder <- boundingBox %% cellsize
    boundingBox[,1] <- boundingBox[,1] - rounder[,1]
    boundingBox[,2] <- boundingBox[,2] - rounder[,2] + cellsize
  }
  if (offset == TRUE) {
    boundingBox[,1] <- boundingBox[,1] - cellsize/2
    boundingBox[,2] <- boundingBox[,2] + cellsize/2
  }
  print(sprintf("grille_lambert() X: %s => %s", boundingBox[1,1], boundingBox[1,2]))
  print(sprintf("grille_lambert() Y: %s => %s", boundingBox[2,1], boundingBox[2,2]))
  ENs <- list()
  IDs <- list()
  POLYs <- list()
# http://www.nickeubank.com/wp-content/uploads/2015/10/RGIS1_SpatialDataTypes_part1_vectorData.html
  i <- 0
  for ( x in seq(boundingBox[1,1], boundingBox[1,2], by=cellsize) ) {
    for ( y in seq(boundingBox[2,1], boundingBox[2,2], by=cellsize) ) {
      i <- i + 1
      ID <- sprintf("E%sN%s", as.integer(x/cellsize), as.integer(y/cellsize))
      ENs[[i]] = ID
      IDs <- cbind(IDs, ID)
      poly <- Polygon(rbind(c(x, y), c(x+cellsize, y), c(x+cellsize, y+cellsize), c(x, y+cellsize)))
      polys <- Polygons(list(poly), ID)
      POLYs[[i]] <- polys
    }
  }
  sp <- SpatialPolygons(POLYs)
  attr <- data.frame(NUMERO=1:i, row.names=IDs)
  grille.spdf <- SpatialPolygonsDataFrame(sp, attr)
  proj4string(grille.spdf) <- proj4string(spdf)
  grille.spdf@data$AREA <- as.integer(rgeos::gArea(grille.spdf, byid=TRUE))
#  print(str(IDs))
  grille.spdf@data$EN <- as.character(ENs)
#  plot(grille.spdf)
#  print(head(grille.spdf@data))
  dsn <- sprintf('%s/grille_lambert_%s', varDir, cellsize)
  grille_sauve(grille.spdf, dsn);
  return(grille.spdf)
# http://www.maths.lancs.ac.uk/~rowlings/Teaching/UseR2012/cheatsheet.html
}
#
# alignement sur un multiple des coordonnées UTM
grille_epsg <- function(spdf, cellsize = 1000, epsg="utm30n", crs="+init=epsg:32630", align = TRUE, offset=FALSE) {
  library(sp)
  library(rgdal)
  library(rgeos)
  carp(" cellsize:%s", cellsize)
  epsg.spdf <- spTransform(spdf, CRS(crs))
  boundingBox <- bbox(epsg.spdf)
  carp(" extent %s",  proj4string(spdf))
  print(extent(spdf))
  if (align == TRUE) {
    rounder <- boundingBox %% cellsize
    boundingBox[,1] <- boundingBox[,1] - rounder[,1]
    boundingBox[,2] <- boundingBox[,2] - rounder[,2] + cellsize
  }
  if (offset == TRUE) {
    boundingBox[,1] <- boundingBox[,1] - cellsize/2
    boundingBox[,2] <- boundingBox[,2] + cellsize/2
  }
  carp(" X: %s => %s", boundingBox[1,1], boundingBox[1,2])
  carp(" Y: %s => %s", boundingBox[2,1], boundingBox[2,2])
  ENs <- list()
  IDs <- list()
  POLYs <- list()
# http://www.nickeubank.com/wp-content/uploads/2015/10/RGIS1_SpatialDataTypes_part1_vectorData.html
  i <- 0
  for ( x in seq(boundingBox[1,1], boundingBox[1,2], by=cellsize) ) {
    for ( y in seq(boundingBox[2,1], boundingBox[2,2], by=cellsize) ) {
      i <- i + 1
      ID <- sprintf("E%sN%s", as.integer(x/cellsize), as.integer(y/cellsize))
      ENs[[i]] = ID
      IDs <- cbind(IDs, ID)
      poly <- Polygon(rbind(c(x, y), c(x+cellsize, y), c(x+cellsize, y+cellsize), c(x, y+cellsize)))
      polys <- Polygons(list(poly), ID)
      POLYs[[i]] <- polys
    }
  }
  sp <- SpatialPolygons(POLYs)
  attr <- data.frame(NUMERO=1:i, row.names=IDs)
  grille.spdf <- SpatialPolygonsDataFrame(sp, attr)
  proj4string(grille.spdf) <- proj4string(epsg.spdf)
  grille.spdf@data$AREA <- as.integer(rgeos::gArea(grille.spdf, byid=TRUE))
#  print(str(IDs))
  grille.spdf@data$EN <- as.character(ENs)
  grille.spdf <- spTransform(grille.spdf, proj4string(spdf))
#  plot(grille.spdf)
#  print(head(grille.spdf@data))
  dsn <- sprintf('%s/grille_%s_%s', varDir, epsg, cellsize)
  grille_sauve(grille.spdf, dsn);
  return(grille.spdf)
# http://www.maths.lancs.ac.uk/~rowlings/Teaching/UseR2012/cheatsheet.html
}
#
# sauvegarde d'une grille dans différents formats
grille_sauve <- function(grille.spdf, fic) {
  dsn <-  sprintf('%s.shp', fic)
  writeOGR(grille.spdf, dsn, "grille_lambert", driver="ESRI Shapefile", overwrite_layer=TRUE)
  dsn <-  sprintf('%s.geojson', fic)
  carp(" dsn:%s", dsn)
  writeOGR(grille.spdf, dsn, layer="grille_lambert", driver="GeoJSON", overwrite_layer=TRUE)
  dsn <-  sprintf('%s.kml', fic)
  carp(" dsn:%s", dsn)
  kml.spdf <- spTransform(grille.spdf, CRS("+proj=longlat +datum=WGS84"))
  writeOGR(kml.spdf, dsn, layer="grille_lambert", driver="KML", overwrite_layer=TRUE)
  dsn <-  sprintf('%s.json', fic)
  carp(" dsn:%s", dsn)
  writeOGR(kml.spdf, dsn, layer="grille_lambert", driver="GeoJSON", overwrite_layer=TRUE)
}
#
# ma version
getGridLayer2 <- function(spdf, cellsize, spdfid = NULL) {
  print(sprintf("getGridLayer2() début"))
  if (!requireNamespace("rgeos", quietly = TRUE)) {
    stop("'rgeos' package needed for this function to work. Please install it.",
    call. = FALSE)
  }
  print(str(spdf@data))
  if (is.null(spdfid)){
    spdfid <- base::names(spdf@data)[1]
  }
  spdf@data <- spdf@data[spdfid]
  row.names(spdf@data)<-spdf@data[,spdfid]
#  spdf <- spChFIDs(spdf, spdf@data[,spdfid])
  spdf@data$area <- rgeos::gArea(spdf, byid=TRUE)

  boundingBox <- bbox(spdf)
  rounder <- boundingBox %% cellsize
  boundingBox[,1] <- boundingBox[,1] - rounder[,1]
  roundermax <- cellsize - rounder[,2]
  boundingBox[,2] <- boundingBox[,2] + cellsize - rounder[,2]
  print(str(boundingBox))
  boxCoordX <- seq(from = boundingBox[1,1]-cellsize/2, to = boundingBox[1,2]+cellsize/2, by = cellsize)
  boxCoordY <- seq(from = boundingBox[2,1]-cellsize/2, to = boundingBox[2,2]+cellsize/2, by = cellsize)
  spatGrid <- expand.grid(boxCoordX, boxCoordY)
  spatGrid$id <- seq(1, nrow(spatGrid), 1)
  coordinates(spatGrid) <- 1:2 # promote to SpatialPointsDataFrame
  gridded(spatGrid) <- TRUE # promote to SpatialPixelsDataFrame
  spgrid <- methods::as(spatGrid, "SpatialPolygonsDataFrame") # promote to SpatialPolygonDataFrame

  proj4string(spgrid) <-proj4string(spdf)
  row.names(spgrid) <- as.character(spgrid$id)

  # On ne garde que ce qui touche le fond de carte initial
  over <- rgeos::gIntersects(spgrid, spdf, byid = TRUE)
  x <- colSums(over)
  spgrid <- spgrid[spgrid$id %in% base::names(x[x>0]),]

  mask <- rgeos::gBuffer(spdf, byid=FALSE, id=NULL, width=1.0, quadsegs=5, capStyle="ROUND",joinStyle="ROUND", mitreLimit=1.0)
#  spgrid <- rgeos::gIntersection(spgrid, mask, byid=TRUE, id=as.character(spgrid@data$id), drop_lower_td=FALSE)
  data <- data.frame(id=sapply(methods::slot(spgrid, "polygons"), methods::slot, "ID"))
  row.names(data) <- data$id
  spgrid<-SpatialPolygonsDataFrame(spgrid, data)
  spgrid@data$cell_area <- rgeos::gArea(spgrid, byid=TRUE)
  proj4string(spgrid) <-proj4string(spdf)

  # On calcule la table de passage
  # intersection
  parts <- rgeos::gIntersection(spgrid, spdf, byid=TRUE,drop_lower_td=TRUE)
  data <- data.frame(id=sapply(methods::slot(parts, "polygons"), methods::slot, "ID"))
  tmp <- data.frame(do.call('rbind', (strsplit(as.character(data$id)," "))))
  data$id1 <- as.vector(tmp$X1)
  data$id2 <- as.vector(tmp$X2)
  row.names(data)<-data$id
  parts<-SpatialPolygonsDataFrame(parts, data)
  proj4string(parts) <-proj4string(spdf)

  # Part de surface intersectée
  parts@data$area_part <- rgeos::gArea(parts, byid=TRUE)
  parts@data <- data.frame(parts@data,  area_full=spdf@data[match(  parts@data$id2, spdf@data[,spdfid]),"area"])
  parts@data$area_pct <- (parts@data$area_part/parts@data$area_full)*100
  areas <- parts@data[,c("id1","id2","area_pct")]
  colnames(areas) <- c("id_cell","id_geo","area_pct")
  return(list(spdf = spgrid, df = areas))
}
#' @title Build a Regular Grid Layer
#' @name getGridLayer
#' @description Build a regular grid based on a SpatialPolygonsDataFrame.
#' Provide also a table of surface intersections.
#' @param spdf a SpatialPolygonsDataFrame.
#' @param cellsize output cell size, in map units.
#' @param spdfid identifier field in spdf, default to the first column
#' of the spdf data frame. (optional)
#' @param type shape of the cell, "regular" for squares, "hexagonal" for hexagons.
#' @return A list is returned. The list contains "spdf": a SpatialPolygonsDataFrame of
#' a regular grid and "df":  a data frame of surface intersection. df fields are id_cell: ids of the grid;
#' id_geo: ids of the spdf and area_pct: share of the area of the polygon in the cell
#' (a value of 55 means that 55\% of the spdf unit is within the cell).
#' @import sp
#' @seealso \link{getGridData}
#' @examples
#' \dontrun{
#' data(nuts2006)
#' # Get a grid layer
#' mygrid <- getGridLayer(spdf = nuts2.spdf, cellsize = 200000)
#' # Plot the grid
#' plot(mygrid$spdf)
#' head(mygrid$df)
#'        }
#' @export
getGridLayer0 <- function(spdf, cellsize, type = "regular", spdfid = NULL){
  # id check
  if (is.null(spdfid)){spdfid <- base::names(spdf@data)[1]}

  # spdf management/area
  spdf@data <- spdf@data[spdfid]
  row.names(spdf) <- as.character(spdf@data[,spdfid])

  spdf$area <- rgeos::gArea(spdf, byid=TRUE)

  # get a grid
  if(type %in% c("regular", "hexagonal")){
    spgrid <- switch(type,
                     regular = getGridSquare(spdf, cellsize),
                     hexagonal = getGridHexa(spdf, cellsize))
  }else{
    stop("type should be either 'regular' or 'hexagonal'", call. = F)
  }

  # keep only intersecting cells
  over <- rgeos::gIntersects(spgrid, spdf, byid = TRUE)
  x <- colSums(over)
  spgrid <- spgrid[spgrid$id %in% base::names(x[x>0]),]

  # intersection
  parts <- rgeos::gIntersection(spgrid, spdf, byid = TRUE)
  data <- data.frame(id=sapply(methods::slot(parts, "polygons"),
                               methods::slot, "ID"))
  tmp <- data.frame(do.call('rbind', (strsplit(as.character(data$id)," "))))

  data$id1 <- as.vector(tmp$X1)
  data$id2 <- as.vector(tmp$X2)
  row.names(data) <- data$id
  parts <- SpatialPolygonsDataFrame(parts, data)
  proj4string(parts) <- proj4string(spdf)

  #
  x <- rgeos::gBuffer(rgeos::gUnaryUnion(parts, id = parts$id1), byid=T)
  data <- data.frame(id = sapply(methods::slot(x, "polygons"),
                                 methods::slot, "ID"))
  row.names(data) <- data$id
  spgrid <- SpatialPolygonsDataFrame(x, data)
  spgrid@data$cell_area <- rgeos::gArea(spgrid, byid = TRUE)
  proj4string(spgrid) <-proj4string(spdf)

  #
  areas <- data.frame(parts@data, area_part = rgeos::gArea(parts, byid=TRUE),
                      area_full = spdf@data[match(parts@data$id2,
                                                  spdf@data[,spdfid]),"area"])
  areas$area_pct <- (areas$area_part/areas$area_full) * 100
  areas <- areas[,c("id1","id2","area_pct")]
  colnames(areas) <- c("id_cell","id_geo","area_pct")

  return(list(spdf = spgrid, df = areas))
}
getGridSquare <- function(spdf, cellsize){
  print(sprintf("getGridSquare() cellsize:%s", cellsize))
  boundingBox <- bbox(spdf)
  rounder <- boundingBox %% cellsize
  boundingBox[,1] <- boundingBox[,1] - rounder[,1] - cellsize/2
  roundermax <- cellsize - rounder[,2]
  boundingBox[,2] <- boundingBox[,2] - rounder[,2] + cellsize + cellsize/2
  print(sprintf("getGridSquare() X: %s => %s", boundingBox[1,1], boundingBox[1,2]))
  print(sprintf("getGridSquare() Y: %s => %s", boundingBox[2,1], boundingBox[2,2]))
  boxCoordX <- seq(from = boundingBox[1,1],
                   to = boundingBox[1,2],
                   by = cellsize)
  boxCoordY <- seq(from = boundingBox[2,1],
                   to = boundingBox[2,2],
                   by = cellsize)
#  boxCoordX <- seq(from = boundingBox[1,1]-cellsize/2, to = boundingBox[1,2]+cellsize/2, by = cellsize)
#  boxCoordY <- seq(from = boundingBox[2,1]-cellsize/2, to = boundingBox[2,2]+cellsize/2, by = cellsize)

  spatGrid <- expand.grid(boxCoordX, boxCoordY)
  spatGrid$id <- seq(1, nrow(spatGrid), 1)

  coordinates(spatGrid) <- 1:2
  gridded(spatGrid) <- TRUE
  spgrid <-  methods::as(spatGrid, "SpatialPolygonsDataFrame")
  proj4string(spgrid) <-proj4string(spdf)
  row.names(spgrid) <- as.character(spgrid$id)
  return(spgrid)
}


getGridHexa <- function(spdf, cellsize){
  bbox <- bbox(spdf)
  bbox[, 1] <- bbox[, 1] - cellsize
  bbox[, 2] <- bbox[, 2] + cellsize
  bboxMat <- rbind( c(bbox[1,'min'] , bbox[2,'min']),
                    c(bbox[1,'min'],bbox[2,'max']),
                    c(bbox[1,'max'],bbox[2,'max']),
                    c(bbox[1,'max'],bbox[2,'min']),
                    c(bbox[1,'min'],bbox[2,'min']) )
  bboxSP <- sp::SpatialPolygons(Srl = list(sp::Polygons(list(sp::Polygon(bboxMat)),"bbox")),
                                proj4string=sp::CRS(sp::proj4string(spdf)))
  x <- sp::spsample(x = bboxSP, type = "hexagonal",
                cellsize = cellsize, bb = bbox(spdf))
  grid <- sp::HexPoints2SpatialPolygons(x)
  grid <- sp::SpatialPolygonsDataFrame(Sr = grid,
                                   data = data.frame(id = 1: length(grid)),
                                   match.ID = FALSE)
  row.names(grid) <- as.character(grid$id)
  return(grid)
}
#
# grille pour la Bretagne en Lambert 93
grille_bzh_l93_xkm <- function(pas=10000, diviseur=1000, offset=0) {
  library(sf)
  library(tidyverse)
  les_departements <- c("22","29","35","56")
# c'est parti
  crs <- 2154
  nc <- ign_departement_lire_sf(les_departements) %>%
    st_transform(crs) %>%
    glimpse()
  dsn <- sprintf("%s/grille_bzh_l93.json", varDir)
  st_write(nc, dsn, delete_dsn=TRUE, driver='GeoJSON')
  e <- fonds_extent(nc, marge=pas, aligne=1, offsetX=offset, offsetY=0) %>%
    glimpse()
#  stop('****')
  grille.sfc <- st_make_grid(e, cellsize=pas) %>%
    st_set_crs(crs) %>%
    glimpse()
  grille.sf <- st_as_sf(data.frame(no=c(1:length(grille.sfc)), geometry=grille.sfc)) %>%
    mutate(X=map_dbl(geometry, ~st_centroid(.x)[[1]]), Y=map_dbl(geometry, ~st_centroid(.x)[[2]])) %>%
    mutate(X=round((X-pas/2)/diviseur, 0), Y=round((Y-pas/2)/diviseur, 0)) %>%
    mutate(Name=sprintf("E%sN%s", X, Y)) %>%
    mutate(ncarre=sprintf("E%03dN%s", X, Y)) %>%
    st_set_crs(crs) %>%
    glimpse()
  carp('sf1 apres intersection')
  sf1 <- st_intersection(grille.sf, nc) %>%
    dplyr::select(no) %>%
    glimpse()
  carp('les mailles entières')
  sf2 <- grille.sf[grille.sf$no %in% sf1$no, ] %>%
    dplyr::select(-no, -X, -Y) %>%
    st_cast("POLYGON") %>%
    glimpse()
  dsn <- sprintf("%s/grille_l93_%s_%s.json", varDir, pas, offset)
  st_write(sf2, dsn, delete_dsn=TRUE, driver='GeoJSON')
  sf2 <- st_transform(sf2, 4326)
  dsn <- sprintf("%s/grille_l93_%s_%s.geojson", varDir, pas, offset)
  st_write(sf2, dsn, delete_dsn=TRUE)
  carp('dsn: %s', dsn)
}


# <!-- coding: utf-8 -->
#
# atlas
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
# traitements pour avoir les contours
# pour les simplifier
# https://mapshaper.org/
#
# setwd("d:/web"); source("geo/scripts/atlas.R");couches_jour(force = TRUE)
couches_jour <- function(force = TRUE) {
  carp()
  couches_bzh_lire(force = force)
  couches_d35(force = force)
}
#
# source("geo/scripts/atlas.R");couches_bzh(force = TRUE)
couches_bzh <- function(force = FALSE) {
  library(sf)
  library(tidyverse)
  carp()
  les_departements <- c("22", "29", "35", "44", "56")
  carp("les départements bretons")
  nc <- ign_adminexpress_lire_sf("DEPARTEMENT") %>%
    filter(INSEE_DEP %in% les_departements) %>%
    st_transform(4326) %>%
    glimpse()
  dsn <- sprintf("%s/couches_bzh.geojson", varDir)
  st_write(nc, dsn, append = FALSE, delete_dsn = TRUE)
  carp("dsn: %s", dsn)
}
#
# source("geo/scripts/atlas.R");couches_d35(force = TRUE)
couches_d35 <- function(force = FALSE) {
  library(sf)
  library(tidyverse)
  carp()
  les_departements <- c("35")
  carp("les départements bretons")
  nc <- ign_adminexpress_lire_sf("DEPARTEMENT") %>%
    filter(INSEE_DEP %in% les_departements) %>%
    st_transform(4326) %>%
    glimpse()
  dsn <- sprintf("%s/couches_d35.geojson", varDir)
  st_write(nc, dsn, append = FALSE, delete_dsn = TRUE)
  carp("dsn: %s", dsn)
}
#
# source("geo/scripts/atlas.R");couches_d35_villes(force = TRUE)
couches_d35_villes <- function(force = FALSE) {
  library(sf)
  library(tidyverse)
  carp()
  les_communes <- c("Saint-Malo", "Vitré", "Rennes", "Fougères")
  nc <- ign_adminexpress_lire_sf("COMMUNE") %>%
    filter(grepl("^35", INSEE_COM)) %>%
    filter(NOM %in% les_communes) %>%
    st_transform(4326) %>%
    glimpse()
  dsn <- sprintf("%s/couches_d35_villes.geojson", varDir)
  st_write(nc, dsn, append = FALSE, delete_dsn = TRUE)
  carp("dsn: %s", dsn)
}
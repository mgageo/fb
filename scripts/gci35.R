# <!-- coding: utf-8 -->
#
# quelques fonctions pour le suivi gravelot à collier interrompu
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
mga  <- function() {
  source("geo/scripts/gci35.R");
}
source("geo/scripts/misc.R")
#
# les variables globales
cfgDir <- sprintf("%s/web/geo/%s", Drive, "GCI35")
texDir <- sprintf("%s/web/geo/%s", Drive, "GCI35")
varDir <- sprintf("%s/bvi35/CouchesGci35", Drive);
dir.create(varDir, showWarnings = FALSE, recursive = TRUE)
dir.create(texDir, showWarnings = FALSE, recursive = TRUE)
ignDir <- sprintf("%s/bvi35/CouchesIGN", Drive)
couchesDir <- sprintf("%s/bvi35/CouchesIGN", Drive)
webDir <- sprintf("%s/web.heb/bv/gci35", Drive);
tplDir <- sprintf("%s/web/geo/scripts", Drive);
user1 <- "apivn45"
gci35_annee <- "2024"; # l'année en cours
donneesFic <- sprintf("gci35_%s.xlsx", gci35_annee)
donneesDsn <-  sprintf("%s/%s" , varDir, donneesFic)
#
# les bibliothèques
setwd(baseDir)
source("geo/scripts/mga.R")
source("geo/scripts/misc_apivn.R")
source("geo/scripts/misc_biolo.R")
source("geo/scripts/misc_biolo54.R")
source("geo/scripts/misc_couches.R")
source("geo/scripts/misc_db.R")
source("geo/scripts/misc_drive.R")
source("geo/scripts/misc_faune.R")
source("geo/scripts/misc_fonds.R")
source("geo/scripts/misc_ftp.R")
#source("geo/scripts/misc_gdal.R")
source("geo/scripts/misc_geo.R")
source("geo/scripts/misc_geocode.R")
source("geo/scripts/misc_ggplot.R")
source("geo/scripts/misc_happign.R")
source("geo/scripts/misc_psql.R")
source("geo/scripts/misc_tex.R")
source("geo/scripts/gci35_apivn.R")
source("geo/scripts/gci35_bagues.R")
source("geo/scripts/gci35_bdmsm.R")
source("geo/scripts/gci35_cartes.R")
source("geo/scripts/gci35_db.R")
source("geo/scripts/gci35_donnees.R")
source("geo/scripts/gci35_drive.R")
source("geo/scripts/gci35_echasse.R")
source("geo/scripts/gci35_faune.R")
source("geo/scripts/gci35_hermelles.R")
source("geo/scripts/gci35_mymaps.R")
source("geo/scripts/gci35_rassemblement.R")
source("geo/scripts/gci35_serena.R")
source("geo/scripts/gci35_secteurs.R")
source("geo/scripts/maree_shom.R")
#
# les commandes permettant le lancement
setwd(texDir)
DEBUG <- FALSE
if ( interactive() ) {
  carp("interactive")
# un peu de nettoyage
  graphics.off()
  par(mar = c(0,0,0,0), oma = c(0,0,0,0))
} else {
  carp("console")
}
# les traitements à enchainer suite à une mise à jour
# source("geo/scripts/gci35.R");jour()
jour <- function(force = TRUE) {
  carp()
  library(tidyverse)
#  secteurs_bagues_lire()
#  mymaps_jour(force = force)
#  gci35_faune_jour(force = force)
#  cartes_jour(force = FALSE, test = 2)
  apivn_jour()
#  bagues_jour()
  url <- sprintf("https://mga.alwaysdata.net/gci35/gci35fb.html")
  browseURL(
    url,
    browser = "C:/Program Files/Mozilla Firefox/firefox.exe"
  )
}

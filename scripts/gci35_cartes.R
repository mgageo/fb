# <!-- coding: utf-8 -->
#
# quelques fonctions pour le suivi gravelot à collier interrompu
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
# les données gci35 sur BMSM
#
# source("geo/scripts/gci35.R");cartes_jour(test = 2, force = TRUE)
cartes_jour <- function(test = 2, force = FALSE) {
  carp()
  gci35_faune_jour(force = force)
  cartes_secteurs(test = test)
#  cartes_secteur(secteur = "secteur2")
}
#
# production des cartes par secteur
# source("geo/scripts/gci35.R");cartes_secteurs(test = 2)
cartes_secteurs <- function(test = 1) {
  library(tidyverse)
  library(sf)
  carp()
  ogcDir <<- sprintf("%s/ogc", varDir)
  dir.create(ogcDir, showWarnings = FALSE)
  fonds.df <- cartes_ogc_fonds_secteurs()
  nc <- secteurs_lire() %>%
    mutate(Name = secteur) %>%
    mutate(name = secteur) %>%
    arrange(Name) %>%
    st_transform(2154)
  cartes_ogc(nc, fonds.df, type = "secteurs", test = test); stop("****")
  bagues.sf <<- secteurs_bagues_lire() %>%
    mutate(no = 1:n()) %>%
    mutate(no = sprintf("B%s", no)) %>%
    st_join(nc)
  df1 <- bagues.sf %>%
    st_drop_geometry() %>%
    filter(is.na(secteur))
  if (nrow(df1) > 0) {
    glimpse(df1)
    stop("****")
  }
  tex_dftpl2fic(bagues.sf, "bagues")
  faunes.sf <<- gci35_faune_export_lire() %>%
    st_transform(2154) %>%
    filter(grepl("2022-06-08", aaaammjj)) %>%
    arrange(aaaammjj) %>%
    mutate(no = 1:n()) %>%
    mutate(no = sprintf("FB%s", no)) %>%
    st_join(nc) %>%
    glimpse()
  df2 <- faunes.sf %>%
    st_drop_geometry() %>%
    filter(is.na(secteur))
  if (nrow(df2) > 0) {
    carp("observations hors secteurs")
    glimpse(df2)
    stop("****")
  }
  tex_dftpl2fic(faunes.sf, "faunes")
#  return()
  cartes_tex(nc, fonds.df, type = "secteurs", test = 1)
  tex_pdflatex("cartes_secteurs.tex")
}
#
# lecture des contours des secteurs
# le contour englobe les lieudits faune-bretagne hors dpm de rattachement
cartes_secteurs_lire <- function() {
  library(tidyverse)
  library(sf)
  carp()
  dsn <- sprintf("%s/secteurs.geojson", varDir)
  nc <- st_read(dsn) %>%
    glimpse()
  carp("dsn: %s", dsn)
  return(invisible(nc))
}
cartes_tex <- function(nc, fonds.df, type = "secteurs", test = 2) {
  carp()
  library(tidyverse)
  cartesDossier <<- sprintf("cartes_%s", type)
  cartesDir <- sprintf("%s/%s", texDir, cartesDossier)
  dir.create(cartesDir, showWarnings = FALSE)
  texFic <- sprintf("%s/cartes_tex_%s.tex", texDir, type)
  TEX <- file(texFic)
  tex <- "% <!-- coding: utf-8 -->"
  tex_page <- ""
#
# le template tex
  dsn <- sprintf("%s/cartes_%s_tpl.tex", texDir, type)
  template <- readLines(dsn)
  for (i in 1:nrow(nc)) {
    site <- nc[[i, "Name"]]
    if (! grepl("^\\w+", site)) {
      glimpse(nc)
      stop("**** site")
    }
    carp("i: %s site: %s", i,  site)
    fct <- sprintf("_%s", type)
    nc1 <- nc[i, ]
    nc1 <- cartes_fonds(fonds.df, nc1, site, cartesDir, test = test, type = type)
    fonction <- sprintf("cartes_tex_%s", type)
    if (exists(fonction, mode = "function")) {
      nc1 <- do.call(fonction, list(nc1, site = site))
    }
    tpl <- template
    tpl <- tex_df2tpl(nc1, 1, tpl)
    tex <- append(tex, tpl)
#    break
  }
  write(tex, file = TEX, append = FALSE)
  close(TEX)
  carp("texFic: %s", texFic)
}
# génération des différents pdf
cartes_fonds <- function(df, nc, site, pdfDir, test, type) {
  Carp("site: %s", site)
  for (i in 1:nrow(df)) {
    e <- df[i, "pdf"]
    fond <- df[i, "fond"]
#    test <- df[i, "test"]
    param <- df[i, "param"]
    marge <- df[i, "marge"]
    carp("fond: %s", fond)
    dsn <- sprintf("%s/%s_%s.pdf", pdfDir, site, fond)

    if (test != 1 & file.exists(dsn)) {
      next
    }
#    carp("test: %s dsn: %s", test, dsn)
    fonction <- sprintf("cartes_fonds_%s_%s", e, type)
    if (! exists(fonction, mode = "function")) {
      carp("fonction inconnue: %s", fonction)
      stop("***")
    }
    do.call(fonction, list(nc = nc, site = site, df = df[i, ]))
    dev.copy(pdf, dsn, width = par("din")[1], height = par("din")[2])
    carp("dsn: %s %sx%s", dsn, par("din")[1], height = par("din")[2])
#    stop("***")
    graphics.off()
  }
#  stop("***")
  return(invisible(nc))
}
#
# le cas d'une image de type brick
cartes_fonds_img_secteurs <- function(nc, site, df, affiche = TRUE) {
  library(raster)
  Carp("site: %s", site)
  dsn <- sprintf(df[1, "param"], varDir, site, df[1, "fond"])
  Carp("dsn: %s", dsn)
  img <- brick(dsn)
  plotImg(img, maxpixels = 500000)
#  plot(st_geometry(bagues.sf), pch = 19, lwd = 3, col = "red", add = TRUE)
#  text(st_coordinates(st_centroid(bagues.sf)), labels = bagues.sf$no, cex = 1.2, col = "red")
  text(st_coordinates(st_centroid(faunes.sf)), labels = faunes.sf$no, cex = 1.2, col = "orange")
  legend("topright", legend = site, cex = 2, text.col = "darkblue", box.lwd = 0, box.col = "transparent", bg = "transparent")
  nc1 <- bagues.sf %>%
    filter(secteur == site)
  tex_dftpl2fic(nc1, "bagues", suffixe = site, dossier = cartesDossier)
  nc2 <- faunes.sf %>%
    filter(secteur == site)
  tex_dftpl2fic(nc2, "faunes", suffixe = site, dossier = cartesDossier)
}
#
# les couches ogc : récupération sur internet
cartes_ogc <- function(nc, fonds.df, type = "secteurs", test = 2) {
  carp()
  library(tidyverse)
  library(sf)
#  stop("***')
  ogcDir <<- sprintf("%s/ogc", varDir)
  dir.create(ogcDir, showWarnings = FALSE)
  for (i in 1:nrow(nc)) {
    site <- nc[[i, "Name"]]
    carp("i: %s site: %s", i,  site)
    fonds_ogc_fonds(fonds.df, nc[i,], site, test)
#    break
  }
}
#
# la configuration des cartes
cartes_ogc_fonds_secteurs <- function(test=2) {
  df <- read.table(text="fonction|pdf|fond|param|marge|size_max
read|img|geo_photos|%s/ogc/%s_%s.tif|250|4096
read|img|geo_plan|%s/ogc/%s_%s.tif|250|2048
read|img|geo_spot2013|%s/ogc/%s_%s.tif|250|2048
read|img|geo_spot2014|%s/ogc/%s_%s.tif|250|2048
read|img|geo_spot2015|%s/ogc/%s_%s.tif|250|2048
read|img|geo_spot2016|%s/ogc/%s_%s.tif|250|2048
read|img|geo_spot2017|%s/ogc/%s_%s.tif|250|2048
read|img|geo_spot2018|%s/ogc/%s_%s.tif|250|2048
read|img|geo_spot2019|%s/ogc/%s_%s.tif|250|2048
read|img|geo_spot2020|%s/ogc/%s_%s.tif|250|2048
read|img|geo_spot2021|%s/ogc/%s_%s.tif|250|2048
", header = TRUE, sep = "|", blank.lines.skip = TRUE, stringsAsFactors = FALSE, quote = "") %>%
    filter(!grepl("^#", fonction))
  return(invisible(df))
}
#
#
# source("geo/scripts/gci35.R");cartes_arzon(test = 2, force = TRUE)
cartes_arzon <- function(test = 1, force = TRUE) {
  library(tidyverse)
  library(sf)
  carp()
  ogcDir <<- sprintf("%s/ogc", varDir)
  dir.create(ogcDir, showWarnings = FALSE)
  site <- "arzon"
  nc <- cartes_arzon_lire()
  fonds.df <- cartes_ogc_fonds_arzon()
  cartes_ogc_arzon(nc, fonds.df, test = test)
  dsn <- sprintf(fonds.df[1, "param"], varDir, site, fonds.df[1, "fond"])
  Carp("dsn: %s", dsn)
  img <- brick(dsn)
  plotImg(img, maxpixels = 500000)
  nc <- faune_export_lire_sf(donneesDsn, force = FALSE) %>%
    filter(grepl("2021", date_year)) %>%
    st_transform(2154) %>%
    glimpse()
  plot(st_geometry(nc), pch = 19, cex = 2, col = "orange", add=TRUE)
}
#
cartes_arzon_lire <- function() {
  library(tidyverse)
  library(sf)
  carp()
  dsn <- sprintf("%s/Arzon.kml", varDir)
  nc <- st_read(dsn) %>%
    st_transform(2154) %>%
    mutate(Name = "arzon")
  return(invisible(nc))
}
#
# les couches ogc : récupération sur internet
cartes_ogc_arzon <- function(nc, fonds.df, type = "secteurs", test = 2) {
  carp()
  library(tidyverse)
  library(sf)
#  stop("***')
  ogcDir <<- sprintf("%s/ogc", varDir)
  dir.create(ogcDir, showWarnings = FALSE)
  for (i in 1:nrow(nc)) {
    site <- nc[[i, "Name"]]
    carp("i: %s site: %s", i,  site)
    fonds_ogc_fonds(fonds.df, nc[i,], site, test)
#    break
  }
}
# la configuration des cartes pour la Grande Bosse
cartes_ogc_fonds_arzon <- function(test=2) {
  df <- read.table(text="fonction|pdf|fond|param|marge|size_max
read|img|ign_photo_hr|%s/ogc/%s_%s.tif|0|2048
", header = TRUE, sep = "|", blank.lines.skip = TRUE, stringsAsFactors = FALSE, quote = "") %>%
    filter(!grepl("^#", fonction))
  return(invisible(df))
}
#
#
# source("geo/scripts/gci35.R");cartes_grandebosse(test = 2, force = TRUE)
cartes_grandebosse <- function(test = 1, force = TRUE) {
  library(tidyverse)
  library(sf)
  carp()
  ogcDir <<- sprintf("%s/ogc", varDir)
  dir.create(ogcDir, showWarnings = FALSE)
  nc <- cartes_grandebosse_lire()
  fonds.df <- cartes_ogc_fonds_grandebosse()
  cartes_ogc_grandebosse(nc, fonds.df, test = test)
}
#
cartes_grandebosse_lire <- function() {
  library(tidyverse)
  library(sf)
  carp()
  dsn <- sprintf("%s/GrandeBosse.kml", varDir)
  nc <- st_read(dsn) %>%
    st_transform(2154) %>%
    mutate(Name = "GrandeBosse")
  return(invisible(nc))
}
#
# les couches ogc : récupération sur internet
cartes_ogc_grandebosse <- function(nc, fonds.df, type = "secteurs", test = 2) {
  carp()
  library(tidyverse)
  library(sf)
#  stop("***')
  ogcDir <<- sprintf("%s/ogc", varDir)
  dir.create(ogcDir, showWarnings = FALSE)
  for (i in 1:nrow(nc)) {
    site <- nc[[i, "Name"]]
    carp("i: %s site: %s", i,  site)
    fonds_ogc_fonds(fonds.df, nc[i,], site, test)
#    break
  }
}
# la configuration des cartes pour la Grande Bosse
cartes_ogc_fonds_grandebosse <- function(test=2) {
  df <- read.table(text="fonction|pdf|fond|param|marge|size_max
read|img|ign_express2017|%s/ogc/%s_%s.tif|0|2048
#read|img|ign_pac2020|%s/ogc/%s_%s.tif|0|2048
#read|img|ign_pac2021|%s/ogc/%s_%s.tif|0|2048
#read|img|ign_photo1950|%s/ogc/%s_%s.tif|0|2048
read|img|ign_photo2000_2005|%s/ogc/%s_%s.tif|0|2048
read|img|ign_photo2006_2010|%s/ogc/%s_%s.tif|0|2048
read|img|ign_photo2012|%s/ogc/%s_%s.tif|0|2048
read|img|ign_photo2018|%s/ogc/%s_%s.tif|0|2048
read|img|ign_photo|%s/ogc/%s_%s.tif|0|2048
read|img|ign_photo_hr|%s/ogc/%s_%s.tif|0|2048
read|img|geo_photos|%s/ogc/%s_%s.tif|0|2048
read|img|gl_littoral_v1|%s/ogc/%s_%s.tif|0|2048
read|img|gl_littoral_v2|%s/ogc/%s_%s.tif|0|2048
read|img|geo_photos1950|%s/ogc/%s_%s.tif|0|2048
", header = TRUE, sep = "|", blank.lines.skip = TRUE, stringsAsFactors = FALSE, quote = "") %>%
    filter(!grepl("^#", fonction))
  return(invisible(df))
}
#
# production des cartes pour un secteur
# source("geo/scripts/gci35.R");cartes_secteur()
cartes_secteur <- function(Secteur = "Secteur 2", force = FALSE, test = 1) {
  library(tidyverse)
  library(sf)
  carp()
  observateurs <- c("Marc Gauthier", "Pierre Letort")
  ogcDir <<- sprintf("%s/ogc", varDir)
  dir.create(ogcDir, showWarnings = FALSE)
  fonds.df <- cartes_ogc_fonds_secteur()
  nc <- secteurs_lire() %>%
    st_transform(2154)
  faunes.sf <- gci35_faune_export_lire(force = force) %>%
    st_transform(2154) %>%
    filter(grepl("2021-0", aaaammjj)) %>%
    arrange(aaaammjj) %>%
    st_join(nc) %>%
    filter(secteur == get("Secteur")) %>%
    filter(observateur %in% observateurs) %>%
    arrange(lat, lon) %>%
    dplyr::select(id_sighting, Name, lat, lon, secteur, place, detail, comment, atlas_code, observateur, mmjj) %>%
    mutate(Name = sprintf("%s;%s", Name, atlas_code)) %>%
    glimpse()
  df1 <- faunes.sf %>%
    st_drop_geometry()
  df2 <- df1 %>%
    filter(is.na(secteur))
  if (nrow(df2) > 0) {
    glimpse(df2)
    stop("****")
  }
  carp("production des cartes nrow: %s", nrow(faunes.sf))
  for (i in 1:nrow(faunes.sf)) {
    fonds_ogc_fonds(fonds.df, faunes.sf[i,], faunes.sf[[i, "id_sighting"]], test = 2)
    nc1 <- faunes.sf[i, ] %>%
      st_buffer(250) %>%
      glimpse()
    nc2 <- st_join(faunes.sf, nc1, join = st_within, left = FALSE) %>%
      filter(id_sighting.x != id_sighting.y)
    if (nrow(nc2) == 0) {
      next;
    }
    nc3 <- nc2 %>%
      mutate(point.y = sprintf("POINT(%f %f)", lon.y, lat.y)) %>%
      mutate(geom.y = st_transform(st_as_sfc(point.y, crs = 4326), 2154)) %>%
      mutate(d = st_distance(geometry, geom.y, x1, by_element = TRUE)) %>%
      glimpse()
    dsn <- sprintf(fonds.df[1, "param"], varDir, faunes.sf[[i, "id_sighting"]], fonds.df[1, "fond"])
    Carp("dsn: %s", dsn)
    img <- brick(dsn)
    plotImg(img, maxpixels = 500000)
    plot(st_geometry(nc1), add = TRUE)
#    plot(st_geometry(nc3), add = TRUE)
    text(st_coordinates(st_centroid(nc3)), labels = nc3$Name.x, cex = 0.8, font = 2, col = "black")
    text(st_coordinates(st_centroid(faunes.sf[i,])), labels = faunes.sf[[i, "Name"]], cex = 0.8, font = 2, col = "red")
#    stop("*****")
  }
}
#
# production des cartes pour un secteur
# source("geo/scripts/gci35.R");cartes_secteur_apivn()
cartes_secteur_apivn <- function(Secteur = "Secteur 5", force = FALSE, test = 1) {
  library(tidyverse)
  library(sf)
  library(raster)
  library(janitor)
  carp()
# l'export de la trace fait sur faune-france
  dsn <- sprintf("%s/export_%s.kml", webDir, Secteur)
  carp("dsn: %s", dsn)
  nc0 <- st_read(dsn) %>%
    st_transform(2154)
# l'image de fonds
  fonds.df <- cartes_ogc_fonds_secteurs()
  dsn <- sprintf(fonds.df[1, "param"], varDir, Secteur, fonds.df[1, "fond"])
  Carp("dsn: %s", dsn)
  img <- brick(dsn)
  plotImg(img, maxpixels = 1000000)
  observateurs <- c("Marc Gauthier", "Pierre Letort")
  nc <- secteurs_lire() %>%
    st_transform(2154)
  plot(st_geometry(nc), add = TRUE, border = "black", lwd = 2)

  plot(st_geometry(nc0), add = TRUE, col = "#AA336A", lwd = 2)
  df2 <- apivn_donnees_get(force = force) %>%
    filter(grepl("^35",insee)) %>%
    filter(grepl("2023-06", aaaammjj)) %>%
    arrange(timing) %>%
    glimpse()
  nc2 <- st_as_sf(df2, coords = c("coord_lon", "coord_lat"), crs = 4326, remove = FALSE) %>%
    st_transform(2154)
  nc3 <- nc2 %>%
    st_join(nc) %>%
    filter(secteur == get("Secteur")) %>%
    rowid_to_column("no") %>%
    glimpse()
  text(st_coordinates(st_centroid(nc3)), labels = nc3$no, cex = 1.2, font = 2, col = "blue")
  df3 <- nc3 %>%
    st_drop_geometry()
  df4 <- apivn_recode_details(df3, mode = "toto") %>%
    dplyr::select(no, id_sighting,  hhmm, comment, count, atlas_code, Details) %>%
    adorn_totals()
  misc_print(df4)
}

# la configuration des cartes pour un contact
cartes_ogc_fonds_secteur <- function(test=2) {
  df <- read.table(text="fonction|pdf|fond|param|marge|size_max
read|img|ign_photo_hr|%s/ogc/%s_%s.tif|300|2048
", header = TRUE, sep = "|", blank.lines.skip = TRUE, stringsAsFactors = FALSE, quote = "") %>%
    filter(!grepl("^#", fonction))
  return(invisible(df))
}
#
# production des cartes pour un secteur
# source("geo/scripts/gci35.R");cartes_secteur_apivn()
cartes_secteur_apivn <- function(Secteur = "Secteur 4", force = FALSE, res = 1) {
  library(tidyverse)
  library(sf)
  library(terra)
  library(mapsf)
  library(janitor)
  carp()
# gci35 secteur 4 le 14/06/2024
  id_form <- 3232443
  webDir <- sprintf("%s/cartes", webDir)
#
# les données de faune-france en export xlsx
  dsn <- sprintf("%s/export_%s.xlsx", webDir, id_form)
  ff.sf <- faune_export_lire_sf(dsn) %>%
    mutate(Detail = sprintf("%s(%s)%s", total_count, detail, atlas_code)) %>%
    st_transform(2154)
# l'export de la trace fait sur faune-france
  dsn <- sprintf("%s/export_%s.json", webDir, id_form)
  if (! file.exists(dsn) || force == TRUE) {
    biolo_ff();biolo_export_trace(id_form = id_form, dsn = dsn)
  }
  trace.sf <- st_read(dsn) %>%
    st_transform(2154)
  borders.sf <- trace.sf %>%
    st_buffer(200)
  dsn <- sprintf("%s/ortho_%s.tif", webDir, id_form)
  if (! file.exists(dsn) || force == TRUE) {
    wms_layers <- get_layers_metadata("wms-r")$Name
    wms_layer <- grep("ORTHO-EXPRESS.2023", wms_layers, ignore.case = T, value = T)
    get_wms_raster(
        borders.sf,
        layer = wms_layer,
        filename = dsn,
        res = res,
        crs = 2154,
        rgb = TRUE,
        overwrite = force
      )
  }
  img <- rast(dsn)
  dsn <- sprintf("%s/export_%s.png", webDir, id_form)
  mf_theme(mar = c(0, 0, 0, 0))
  mf_export(
    x = img,
    filename = dsn,
    width = 2048,
    expandBB = c(0, 0, 0, 0),
  )
  mf_raster(img, add = TRUE)
#  mf_scale(1, pos = "bottomleft")
  mf_map(x = trace.sf, col = "blue", lwd = 3, pch = 19, add = TRUE)
  mf_label(x = ff.sf,
    var = "Detail",
    cex = 1,
    halo = TRUE,
    r = .5,
    overlap = FALSE,
    q = 2,
    lines = TRUE
  )
  mf_title("secteur 4 14/06/2024", inner = TRUE)
  dev.off()
  carp("dsn: %s", dsn)
}
#
# lecture d'un export en format json
# source("geo/scripts/gci35.R");res <- biolo_ff_json()
biolo_ff_json <- function() {
  library(tidyverse)
  library(RcppSimdJson)
  carp()
  webDir <- sprintf("%s/cartes", webDir)
#
# les données de faune-france en export json
  dsn <- sprintf("%s/export_donnees.json", webDir)
  res <- faune_export_lire_json(dsn)
  return(invisible(res))
}
# <!-- coding: utf-8 -->
#
# quelques fonctions pour clochouette35
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# la partie Google Drive
# ===============================================================
# https://googledrive.tidyverse.org/
# source("geo/scripts/clochouette35.R"); drive_jour()
drive_jour <- function(test=1) {
  carp()
  drive_actions_get()
  drive_geocode()
}
#
# une fenêtre firefox doit s"ouvrir !
# source("geo/scripts/clochouette35.R"); drive_jour_get()
drive_jour_get <- function(test=1) {
  carp()
  library(googledrive)
#  drive_deauth()
#  options(httr_oob_default = TRUE)
  drive_auth("univasso35@gmail.com")
  drive_tex_get(path = "~/clochouette35/GoogleDrive", recursif = FALSE)
}
#
# source("geo/scripts/clochouette35.R"); drive_actions_get()
drive_actions_get <- function() {
  carp()
  library(tidyverse)
  library(googlesheets4)
  library(writexl)
  gs4_deauth()
  url <- "https://docs.google.com/spreadsheets/d/1P1yA7nN0UHtImAHcdjJmnu0VxHMVUZ3FxNo-_RHveUQ/edit?#gid=0"
  df <- googlesheets4::read_sheet(url, sheet = 1) %>%
    glimpse()
  dsn <- sprintf("%s/actions_avec_poses.xlsx", varDir)
  writexl::write_xlsx(df, dsn)
  url <- "https://docs.google.com/spreadsheets/d/1P1yA7nN0UHtImAHcdjJmnu0VxHMVUZ3FxNo-_RHveUQ/edit#gid=1856659282"
  df <- googlesheets4::read_sheet(url, sheet = 2) %>%
    glimpse()
  dsn <- sprintf("%s/actions_en_cours.xlsx", varDir)
  writexl::write_xlsx(df, dsn)
  return(invisible(df))
}
#
# source("geo/scripts/clochouette35.R"); df <- drive_geocode() %>% misc_print()
drive_geocode <- function() {
  carp()
  library(tidyverse)
  library(readxl)
  library(janitor)
  dsn <- sprintf("%s/actions_avec_poses.xlsx", varDir)
  df1 <- read_excel(dsn) %>%
    rename_with(.cols = 1, ~"m")
  dsn <- sprintf("%s/actions_en_cours.xlsx", varDir)
  df2 <- read_excel(dsn) %>%
    rename_with(.cols = 1, ~"m")
  df <- bind_rows(df1, df2) %>%
    select(1:3) %>%
    rowid_to_column("index") %>%
    rename(annee = m) %>%
    clean_names() %>%
    filter(grepl(".{3,}_.{3,}", nom_du_nichoir)) %>%
    separate_wider_delim(nom_du_nichoir, "_", names = c("commune", "lieudit"), cols_remove = FALSE)
#  misc_print(df)
  df1 <- geocode_direct_geopf(df) %>%
    dplyr::select(index, fulltext, lon = x, lat = y)
#  misc_print(df1); stop("ùùùùù")
  df2 <- df %>%
    left_join(df1, by = c("index")) %>%
    filter(!is.na(lon)) %>%
    dplyr::select(nom_du_nichoir, lon, lat)
  dsn <- sprintf("%s/drive_geocode.csv", varDir)
  write_csv(df2, dsn)
  dest <- sprintf("/clochouette35/drive_geocode.csv")
  ftp_upload(site = "atlasnw", local = dsn, dest = dest)
  return(invisible(df2))
}
# <!-- coding: utf-8 -->
#
# traitements pour les effraies de clochouette35
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
#
# https://drive.google.com/drive/u/1/folders/1J8IRi4vVWTVuf9i4pe7TsZxmOAba6JVm
#
mga  <- function() {
  source("geo/scripts/clochouette35.R");
}
#
# le programme principal
# ======================
Drive <- substr( getwd(),1,2)
Drive <- "D:"
print(sprintf("Drive:%s", Drive))
baseDir <- sprintf("%s/web", Drive)
cfgDir <- sprintf("%s/web/geo/CLOCHOUETTE35", Drive)
varDir <- sprintf("%s/web.var/geo/clochouette35", Drive)
bviDir <- sprintf("%s/bvi35/CouchesClochouette35", Drive)
webDir <- sprintf("%s/web.heb/clochouette35", Drive)
scan25Dir <- sprintf("%s/bvi35/CouchesIGN/SCAN25", Drive)
texDir <- cfgDir
dir.create(cfgDir, showWarnings = FALSE, recursive = TRUE)
dir.create(varDir, showWarnings = FALSE, recursive = TRUE)
dir.create(webDir, showWarnings = FALSE, recursive = TRUE)
donneesFic <- "clochouette35.xlsx"
donneesDsn <-  sprintf("%s/%s" , varDir, donneesFic)
setwd(baseDir)
DEBUG <- FALSE
Tex <- TRUE
HtmlBrowse <- FALSE
source("geo/scripts/mga.R")
source("geo/scripts/misc.R")
source("geo/scripts/misc_apivn.R")
source("geo/scripts/misc_biolo.R")
source("geo/scripts/misc_couches.R")
source("geo/scripts/misc_db.R")
source("geo/scripts/misc_faune.R")
source("geo/scripts/misc_fonds.R")
source("geo/scripts/misc_ftp.R")
source("geo/scripts/misc_geo.R")
source("geo/scripts/misc_geocode.R")
source("geo/scripts/misc_psql.R")
source("geo/scripts/misc_st.R")
source("geo/scripts/misc_tex.R")
source("geo/scripts/clochouette35_drive.R")
source("geo/scripts/clochouette35_faune.R")
source("geo/scripts/clochouette35_pdf.R")
source("geo/scripts/clochouette35_umap.R")
if ( interactive() ) {
  DEBUG <- TRUE
} else {
  args = commandArgs(trailingOnly=TRUE)
  if (length(args)==0) {
    stop("At least one argument must be supplied (input file).n", call.=FALSE)
  } else if (length(args)==1) {
    print(sprintf("arg:%s", args[1]))
  }
  jour(force = TRUE)
}
#
# setwd("d:/web"); source("geo/scripts/clochouette35.R");jour(force = TRUE)
jour <- function(force = TRUE) {
  carp()
  drive_jour()
  faune_jour(force = force)
  pdf_jour()
  umap_jour()
}
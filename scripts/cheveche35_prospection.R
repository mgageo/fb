# <!-- coding: utf-8 -->
#
# traitements pour les chevêches du 35
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
#
#
## traitements des sorties
#
# un répertoire par année
# un ou deux fichiers par sortie
# marc-aammjj marcaammjj
#  invalidation d'un fichier Marc...
# source("geo/scripts/cheveche35.R"); prospection_jour()
prospection_jour <- function(force = FALSE) {
  carp()
  prospection_annees_lire()
  prospection_annees_valid()
}
# source("geo/scripts/cheveche35.R"); prospection_tex()
prospection_tex <- function(annee = "2024", force = FALSE) {
  carp()
  tex_tpl_annee(annee = "2024")
#  drive_jour()
  grappes_geocode(annee = annee)
  carto_prospections_terra(annee = annee, force = TRUE)
  prospection_annee_stat(annee = annee)
  dsn <- tex_pdflatex("prospection.tex", dossier = annee)
  dest <- sprintf("cheveche/cheveche%s/prospection.pdf", annee)
  ftp_upload(site = "atlasnw", local = dsn, dest = dest)
}
# source("geo/scripts/cheveche35.R"); nc <- prospection_annees_lire()
prospection_annees_lire <- function(force = FALSE) {
  library(tidyverse)
  library(sf)
  carp()
  nc2 <- tibble()
  for (annee in Annees) {
    if (annee != "2013") {
#      next
    }
    pattern <- sprintf("cheveche%s$", annee)
    files <- list.files(gmlDir, pattern = pattern, full.names = TRUE, ignore.case = FALSE, recursive = FALSE) %>%
      glimpse()
    nc1 <- data.frame()
    for (file in files) {
      nc <- gpx_dir_valid(dir = file)
      nc1 <- rbind(nc1, nc)
    }
    nc1$annee <- annee
    misc_ecrire(nc1, sprintf("prospection_annees_%s", annee))
    nc2 <- rbind(nc2, nc1)
  }
  misc_ecrire(nc2, "prospection_annees")
  return(invisible(nc2))
}
# source("geo/scripts/cheveche35.R"); nc <- prospection_annee_lire(annee = "2024")
prospection_annee_lire <- function(annee = "2024", force = FALSE) {
  library(tidyverse)
  library(sf)
  carp()
  pattern <- sprintf("cheveche%s$", annee)
  files <- list.files(gmlDir, pattern = pattern, full.names = TRUE, ignore.case = FALSE, recursive = FALSE) %>%
    glimpse()
  nc1 <- data.frame()
  for (file in files) {
    nc <- gpx_dir_valid(dir = file)
    nc1 <- rbind(nc1, nc)
   }
  misc_ecrire(nc1, sprintf("prospection_annee_%s", annee))
  return(invisible(nc1))
}
#
# https://stackoverflow.com/questions/77179007/group-spatial-points-by-distance-in-r-how-to-group-cluster-spatial-points-so-gr
# source("geo/scripts/cheveche35.R"); stat <- prospection_annee_stat(annee = "2024")
prospection_annee_stat <- function(annee = "2024", force = FALSE) {
  library(tidyverse)
  library(sf)
  library(stringi)
  library(igraph)
  library(stars)
  carp()
  stat <- list(
    "annee"= as.integer(annee)
  )
  nc1 <- misc_lire(sprintf("prospection_annee_%s", annee)) %>%
    st_transform(2154) %>%
    glimpse()
  repasses.sf <- nc1 %>%
    filter(grepl("Blue|Green", sym))
  reponses.sf <- nc1 %>%
    filter(grepl("Orange", sym))
  surface <- prospection_annee_tache(repasses.sf) %>%
    st_area() %>%
    glimpse()
  stat$nb_sorties <- nrow(repasses.sf %>% distinct(dsn))
  stat$nb_repasses <- nrow(repasses.sf)
  stat$surface_ha <- as.integer(surface / 10000)
  stat$surface_km2 <- as.integer(surface / 1000000)
#  stat$repasses_km2 <- round(stat$nb_repasses / stat$surface_km2, 1)
  stat$nb_reponses <- nrow(reponses.sf)
  grappes.sf <- prospection_annee_grappes(reponses.sf)
  stat$nb_grappes <- nrow(grappes.sf)
  glimpse(stat)
  df1 <- tibble::enframe(stat) %>%
    tidyr::unnest(value)
  tex_df2kable(df1, dossier = annee)
  return(invisible(df1))
}
#
#
# source("geo/scripts/cheveche35.R"); prospection_annee_tache(annee = "2024")
prospection_annee_tache <- function(nc1) {
  library(tidyverse)
  library(sf)
  carp()
#
# la tache "prospectée"
# on dilate
  sfc1 <- st_buffer(nc1, 500)
# on fusionne
  sfc2 <- st_union(sfc1)
  return(invisible(sfc2))
}
#
# source("geo/scripts/cheveche35.R");grappes_get(force = TRUE) %>% glimpse()
prospection_annee_grappes <- function(reponses.sf) {
  library(tidyverse)
  library(sf)
  library(igraph)
#
# les territoires potentiels
# regroupement des réponses
  adj <- st_distance(reponses.sf)
  adj <- matrix(as.numeric(as.numeric(adj)) < 300, nrow = nrow(adj))
  g <- graph_from_adjacency_matrix(adj)
  carp("possible: %s", length(unique(components(g)$membership)))
  reponses.sf$grappe <- factor(components(g)$membership)
  nc2 <- reponses.sf %>%
    group_by(grappe) %>%
    summarize(nb = n()) %>%
    st_centroid()
  return(invisible(nc2))
}
# source("geo/scripts/cheveche35.R"); prospection_annees_stat()
prospection_annees_stat <- function(force = FALSE) {
  library(tidyverse)
  library(sf)
  library(stringi)
  carp()
#  prospection_annees_lire()
  df1 <- misc_lire("prospection_annees") %>%
    st_drop_geometry() %>%
    tidyr::extract(dsn, c("aammjj"), "(\\d{6})\\.gpx$", remove = FALSE) %>%
    filter(!grepl("(Grey)", sym)) %>%
    glimpse()
  df0 <- df1 %>%
    filter(!grepl("(Blue|Green|Orange)", sym)) %>%
    glimpse()
  if (nrow(df0) > 0) {
    stop("****")
  }
  df2 <- df1 %>%
    group_by(annee) %>%
    summarize(nb = n()) %>%
    glimpse()
  df3 <- df1 %>%
    filter(grepl("(Blue|Green)", sym)) %>%
    group_by(annee) %>%
    summarize(repasse = n()) %>%
    glimpse()
  df4 <- df1 %>%
    filter(grepl("(Orange)", sym)) %>%
    group_by(annee) %>%
    summarize(reponse = n()) %>%
    glimpse()
  df5 <- df1 %>%
    group_by(annee, aammjj) %>%
    summarize(nb = n()) %>%
    group_by(annee) %>%
    summarize(sortie = n()) %>%
    glimpse()
  df2 <- df2 %>%
    left_join(df5, by = c("annee")) %>%
    left_join(df3, by = c("annee")) %>%
    left_join(df4, by = c("annee"))
  misc_print(df2)
}
# source("geo/scripts/cheveche35.R"); nc <- prospection_annees_valid()
prospection_annees_valid <- function(force = FALSE) {
  library(tidyverse)
  library(sf)
  library(stringi)
  carp()
#  prospection_annees_lire()
  df1 <- misc_lire("prospection_annees") %>%
    st_drop_geometry() %>%
#    mutate(sym = gsub("Flag, Red", "Flag, Orange", sym)) %>%
    filter(! grepl("^Flag, (Grey)", sym)) %>%
    glimpse()
  df2 <- df1 %>%
    filter(! grepl("^[A-Za-h]", name))
  if (nrow(df2) > 0) {
    misc_print(df2)
    stop("nom du point")
  }
# les flags possibles
  df3 <- df1 %>%
    filter(! grepl("^Flag, (Blue|Orange|Green)", sym))
  if (nrow(df3) > 0) {
    misc_print(df3)
    stop("flag")
  }
# les flags possibles pour le point de repasse
  df4 <- df1 %>%
    filter(! grepl("^Flag, (Blue|Green)$", sym)) %>%
    filter(grepl("^[A-Za-h]\\d\\d$", name))
  if (nrow(df4) > 0) {
    misc_print(df4)
    stop("flag point de repasse")
  }
# les flags possibles pour les contacts
  df5 <- df1 %>%
    filter(! grepl("^Flag, Orange$", sym)) %>%
    filter(grepl("^[A-Za-h]\\d\\d[a-z]$", name))
  if (nrow(df5) > 0) {
    misc_print(df5)
    stop("flag contact")
  }
# le format du name
  df6 <- df1 %>%
    filter(! grepl("^[A-Za-h]\\d\\d[a-z]?$", name))
  if (nrow(df6) > 0) {
    misc_print(df6)
    stop("format name")
  }
  df1 <- df1 %>%
    tidyr::extract(dsn, c("aammjj"), "(\\d{6})\\.gpx$", remove = FALSE) %>%
    mutate(point = sprintf("%s_%s", aammjj, name)) %>%
    mutate(repasse = sprintf("%s_%s", aammjj, stri_sub(name, 1, 3)))
  repasses.df <- df1 %>%
    filter(grepl("^[A-Za-h]\\d\\d$", name)) %>%
    glimpse()
  contacts.df <- df1 %>%
    filter(! grepl("^[A-Za-h]\\d\\d$", name)) %>%
    glimpse()
# les contacts sans repasse
  df11 <- contacts.df %>%
    filter(repasse %notin% repasses.df$repasse)
  if (nrow(df11) > 0) {
    misc_print(df11)
    stop("les contacts sans repasse")
  }
#
# les points en double
  df12 <- df1 %>%
    filter(duplicated(cbind(point)))
  df13 <- df1 %>%
    filter(point %in% df12$point) %>%
    arrange(point)
# données identiques ?
  df14 <- df13 %>%
    filter(duplicated(cbind(point, name, sym))) %>%
    arrange(point)
  df15 <- df13 %>%
    filter(point %notin% df14$point) %>%
    arrange(point)
  if (nrow(df15) > 0) {
    misc_print(df15)
    stop("points en double")
  }
# le format de fichier
  df16 <- df1 %>%
    filter(is.na(aammjj))
  if (nrow(df16) > 0) {
    misc_print(df16)
    stop("format nom fichier")
  }
#
# sans les doubles
  df1 <- df1 %>%
    filter(! duplicated(cbind(point, name, sym))) %>%
    arrange(point) %>%
    glimpse()
#
# vérification de la couleur du point de repasse
  df21 <- df1 %>%
    group_by(repasse) %>%
    summarize(nb = n())
  df22 <- df1 %>%
    filter(repasse == point) %>%
    left_join(df21, by = c("repasse"))
  df23 <- df22 %>%
    filter(sym == "Flag, Blue") %>%
    filter(nb != 1)
  if (nrow(df23) > 0) {
    misc_print(df23)
    stop("repasse avec contact")
  }
  df24 <- df22 %>%
    filter(sym != "Flag, Blue") %>%
    filter(nb == 1)
  if (nrow(df24) > 0) {
    misc_print(df24)
    stop("repasse avec contact")
  }
  df31 <- df1 %>%
    distinct(aammjj) %>%
    glimpse()
  misc_ecrire(df31, "prospection_aammjj")
  return(invisible(df31))
}
# <!-- coding: utf-8 -->
#
# les traitements sur les données en base postgresql
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
## comparaison avec les exports xlsx
#
# source("geo/scripts/apivn.R"); gci_jour()
gci_jour <- function() {
  library(dbplyr)
  library(tidyverse)
  library(lubridate)
  carp()
  user1 <- "apivn35"
#  gci_apivn_extract(user1)
  gci_compare()
}
#
# source("geo/scripts/apivn.R"); gci_apivn_extract()
gci_apivn_extract <- function(user = "apivn32") {
  carp()
  psql_connect(user = user)
  psql <- extract_psql2()
  psql <- sprintf(psql, "src_vn.observations.id_species = 219")
  res <- db_SendQuery(db_con, psql) %>%
    glimpse()
  misc_ecrire(res, "gci_apivn_extract")
}
#
# source("geo/scripts/apivn.R"); gci_apivn_extract_lire()
gci_apivn_extract_lire <- function(user = "apivn32") {
  carp()
  df <- misc_lire("gci_apivn_extract") %>%
    arrange(id_sighting) %>%
    glimpse()
  return(invisible(df))
}
# lecture de l'export de faune-bretagne
gci_faune_export_lire <- function(force = TRUE) {
  library(tidyverse)
  carp()
  varDir <- sprintf("%s/bvi35/CouchesGci35", Drive);
  donneesFic <- "gci35.xlsx"
  donneesDsn <-  sprintf("%s/%s" , varDir, donneesFic)
  df <- faune_export_lire(donneesDsn, force = force) %>%
    dplyr::select(id_sighting, id_species, name_species, date, insert_date, update_date) %>%
    mutate(id_sighting = as.numeric(id_sighting)) %>%
    arrange(id_sighting) %>%
    glimpse()
  return(invisible(df))
}
gci_compare <- function() {
  carp()
  df1 <- gci_apivn_extract_lire() %>%
    dplyr::select(id_sighting) %>%
    mutate(source = "apivn")
  df2 <- gci_faune_export_lire()%>%
    dplyr::select(id_sighting) %>%
    mutate(source = "export")
  df3 <- df1 %>%
    full_join(df2, by = c("id_sighting")) %>%
    filter(is.na(source.x) | is.na(source.y)) %>%
    glimpse()
  misc_print(df3)
}
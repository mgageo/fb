# <!-- coding: utf-8 -->
#
# quelques fonctions pour Bretagne Vivante Ornithologie
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#####################################################################################################################
#
# lecture des fichiers de données
#
# données du 29
#####################################################################################################################
#
# http://www.milanor.net/blog/?p=779
bvo_donnees_29 <- function() {
  f_csv <- sprintf('%s\\web.heb\\ao35\\AH29\\resultats\\%s', Drive, 'donnees2013.csv')
  df <- read.csv(f_csv, header = TRUE,sep=";", na.strings="")
  colnames(df) <- c("espece", "date", "lieudit", "commune", "maille", "effectif", "observation", "observateur")
  if ( DEBUG ) {
    print(summary(df))
    print(sprintf("lit_donnees_29() head"))
    print(head(df), 20)
  }
  df$espece <- iconv(df$espece, "latin1", "UTF-8")
  print(sprintf("bvo_donnees_29() nb:%d", length(df$espece)))
  return(df)
}
#
# détermination du qualitatif maille espèce
bvo_maille_espece <- function() {
  source("geo/scripts/misc_bvo.R")
  library(sqldf)
  print(sprintf("atlas() debut"))
#
# lecture des mailles
  dept <- '29'
  lit_carte()
# lecture des données
  donneesDF <- bvo_donnees_29()
  donneesDF$espece_ascii <- toASCII(donneesDF$espece)
  synonymesDF <- lit_especes()
#  print(sprintf("atlas() synonymes"))
#  print(head(synonymesDF,30))

# ajout du nom atlas
  donnees_atlasDF <- sqldf("select donneesDF.*, synonymesDF.espece as espece_atlas from donneesDF left join synonymesDF on donneesDF.espece_ascii = synonymesDF.synonyme;")
#  print(sprintf("atlas() donnees_atlas"))
#  print(head(donnees_atlasDF,30))
#
# suppression des données hors zone atlas
  maillesSP <- lit_mailles(dept)
  mailles <- as.vector(maillesSP@data$CODE10KM)
  donnees_atlasDF <- donnees_atlasDF[donnees_atlasDF$maille %in% mailles , ]
#
# calcul du qualitatif
  maille_especeDF <- sqldf("select maille, espece_atlas as espece, count(*) as nb from donnees_atlasDF group by maille, espece_atlas;")
  maille_especeDF$espece_ascii <- toASCII(maille_especeDF$espece)
  f_csv <- sprintf('geo/AOH%s/bvo_maille_espece.csv', dept)
  write.csv(maille_especeDF, f_csv, row.names=FALSE, quote=FALSE)
  print(sprintf("bvo_maille_espece() nb:%s f_csv:%s", length(maille_especeDF$espece), f_csv))
  return(maille_especeDF)
}
#
## liste rouge
# source("geo/scripts/odf.R");bvo_lire() %>% glimpse()
bvoDir <- sprintf("%s/bvi35/CouchesODF", Drive)
bvo_nicheurs_lire <- function(force = FALSE) {
  library(tidyverse)
  dsn <- sprintf("%s/bvo_nicheurs_mga2.xlsx", bvoDir)
  carp("dsn: %s", dsn)
  df <- rio::import(dsn)
  carp("dsn: %s", dsn)
  return(invisible(df))
}
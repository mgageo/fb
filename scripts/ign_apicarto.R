# <!-- coding: utf-8 -->
#
# les données de l'ign via l'api carto
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
#
# https://paul-carteron.github.io/happign/articles/web_only/api_carto.html
#
# récupération des données pour une commune
#
# source("geo/scripts/ign.R"); df <- apicarto_cadastre() %>% glimpse()
apicarto_cadastre <- function() {
  library(happign)
  library(sf)
  library(tmap); tmap_mode("view") # Set map to interactive
  library(dplyr)
  borders <- get_apicarto_cadastre("35051", type = "commune")
# result
  tm <- tm_shape(borders)+
     tm_borders(col = "black")
  print(tm)
}
#
# source("geo/scripts/ign.R"); df <- apicarto_rpg() %>% glimpse()
apicarto_rpg <- function() {
  library(happign)
  library(sf)
  library(tmap); tmap_mode("view") # Set map to interactive
  library(dplyr)
  borders <- get_apicarto_cadastre("35051", type = "commune")
  rpg <- get_apicarto_rpg(borders, annee = 2013:2022) %>%
    glimpse()
# result
  tm <- tm_shape(borders)+
     tm_borders(col = "black")
  print(tm)
}
#
# source("geo/scripts/ign.R"); df <- apicarto_gpu() %>% glimpse()
apicarto_gpu <- function() {
  library(happign)
  library(sf)
  library(tmap); tmap_mode("view") # Set map to interactive
  library(dplyr)
  borders <- get_apicarto_cadastre("35051", type = "commune")
  doc <- get_apicarto_gpu(borders, "document",  dTolerance = 10) %>%
    glimpse()
  partition <- doc |>
   filter(grid_title == "PLUI RENNES METROPOLE") |>
   pull(partition)
  zone_urba <- get_apicarto_gpu(partition, ressource = "zone-urba")

# click on polygon for legend
  tm <- tm_shape(zone_urba)+
    tm_polygons("libelong", legend.show = FALSE)
  print(tm)
  return()
}
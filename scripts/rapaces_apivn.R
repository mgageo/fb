# <!-- coding: utf-8 -->
#
# la partie apivn
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# la totale
# source("geo/scripts/rapaces.R");apivn_jour();
apivn_jour <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  df <- apivn_donnees_get()
  apivn_donnees_fb(df)
}
#
# les données
# source("geo/scripts/rapaces.R");apivn_donnees_get() %>% glimpse()
apivn_donnees_get <- function(force = TRUE) {
  carp()
  library(tidyverse)
  library(janitor)
  library(rio)
  df <- apivn_extract_ObsRap_test(force = force) %>%
    filter(grepl("(Accipitridae|Falconidae)", famille))
  df1 <- df %>%
    group_by(famille, espece) %>%
    summarize(nb = n())
  misc_print(df1)
  return(invisible(df))
}
# source("geo/scripts/rapaces.R");apivn_carto()
apivn_carto <- function(force = TRUE) {
  carp()
  library(tidyverse)
  library(janitor)
  library(sf)
  df1 <- apivn_donnees_get(force = FALSE) %>%
    glimpse()
  df2 <- df1 %>%
    distinct(espece)
  misc_print(df2)
  nc1 <- st_as_sf(df1, coords = c("coord_lon", "coord_lat"), crs = 4326)
  nc2 <- nc1 %>%
    dplyr::select(name = espece) %>%
    glimpse()
  map <- leaflet_points(nc2)
  print(map)

  return(invisible(nc1))
}

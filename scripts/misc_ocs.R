# <!-- coding: utf-8 -->
#
# la partie occupation du sol
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# http://journals.openedition.org/cybergeo/27067?lang=en
# https://github.com/mapbox/robosat reconnaissance automatique sur des images, apprentissage OpenStreetMap
#
# couches bocage de l'onf ignf
# https://inventaire-forestier.ign.fr/IMG/pdf/dsb_4pages.pdf
#
# ==================================================================================================
# Conservatoire National Botanique de Brest
#
# ocs de l'I&V publié en septembre 2019
# fichiers sur demande mail
#
# le shapefile est gros : 1,6 Go
# il correspond au Physionomie_vegetation
#
# source("geo/scripts/onc35.R");ocs_cbnbrest_jour()
ocs_cbnbrest_jour <- function() {
  carp()
  library(sf)
  library(tidyverse)
#  nc <- ocs_cbnbrest_shp_lire()
#  ocs_cbnbrest_ecrire(nc)
  ocs_cbnbrest_lire()
}
ocs_cbnbrest_shp_lire <- function() {
  carp()
  library(sf)
  library(tidyverse)
  dsn <- sprintf('%s/CGTV_35.shp', cbnbrestDir)
  carp('dsn: %s', dsn)
  nc  <- st_read(dsn) %>%
    glimpse()
  return(invisible(nc))
}
ocs_cbnbrest_ecrire <- function(nc) {
  carp()
  library(sf)
  library(tidyverse)
  dsn <- sprintf("%s/ocs_cbnbrest.Rds", cbnbrestDir)
  saveRDS(nc, dsn)
  carp("dsn: %s", dsn)
}
ocs_cbnbrest_lire <- function(...) {
  carp()
  library(sf)
  library(tidyverse)
  if(exists('ocs_cbnbrest.sf')) {
    return(invisible(ocs_cbnbrest.sf))
  }
  dsn <- sprintf("%s/ocs_cbnbrest.Rds", cbnbrestDir)
  nc <- readRDS(dsn)
  carp("dsn: %s", dsn)
  ocs_cbnbrest.sf <<- nc
  return(invisible(nc))
}
# lecture de la nomenclature
ocs_cbnbrest_nomenclature_lire <- function() {
  library(rio)
  dsn <- sprintf('%s/%s',cbnbrestDir, 'gml_styles.xlsx')
  dsn <- sprintf('%s/%s',cbnbrestDir, 'nomenclature_mga.csv')
  df <- import(dsn)
  carp('dsn: %s nrow: %s', dsn, nrow(df))
  return(invisible(df))
}
#
# ==================================================================================================
# Cartographie des ensembles de paysages en Bretagne réalisée par L. Le Du-Blayo (Université Rennes 2, UMR CNRS 6590 ESO « Espaces et Sociétés »)
# https://geobretagne.fr/geoserver/rennes2/wfs?service=wfs&version=2.0.0&request=getfeature&typename=ensembles_familles_paysages&outputformat=shape-zip
#
# gdalwarp -s_srs EPSG:27572 -t_srs EPSG:2154 classifpaysmodis2.tif classifpays.tif
# gdalinfo -noct classifpays.tif
# source("geo/scripts/onc35.R");ocs_paysages_raster_lire()
ocs_paysages_raster_lire <- function() {
  library(raster)
  library(tidyverse)
  dsn <- sprintf('%s/bvi35/CouchesRennes2/classifpays.tif', Drive)
  carp('dsn: %s', dsn)
  r  <- raster(dsn) %>%
    glimpse()
  carp('crs: %s', crs(r))
  return(invisible(r))
}
#
# source("geo/scripts/onc35.R");ocs_paysages_raster_stat()
ocs_paysages_raster_stat <- function() {
  library(raster)
  library(tidyverse)
  carp()
  dsn <- sprintf('%s/bvi35/CouchesRennes2/classifpaysmodis2.tif', Drive)
  r  <- ocs_paysages_raster_lire()
  plot(r, axes=FALSE)
  colortable(r)
  freq(r)
  ocs_raster_vals(r)
  plot.new()
  couleurs <- colortable(r)[1:19] %>%
    glimpse()
  codes <- seq(1:19)
  df <- data.frame(code=codes, couleur=couleurs) %>%
    print(n=19, na.print='')
  xref <- 0
  yref <- 0
  width <- 20
  height <- 10
  delta <- 2
  plot(c(0, 200), c(0, 250), type = "n", xlab = "", ylab = "")
  for (i in 1:(length(couleurs))){
    rect(xref, yref, xref + width, yref + height, col=couleurs[i])
    text(xref + width + delta , yref + height / 2, labels=couleurs[i], adj = c(0,0.5))
    yref <- yref + height + delta
  }
}
#
# source("geo/scripts/onc35.R");ocs_familles_paysages_lire()
ocs_familles_paysages_lire <- function() {
  library(sf)
  library(tidyverse)
  carp()
  dsn <- sprintf('%s/bvi35/CouchesRennes2/ensembles_familles_paysages.geojson', Drive)
  nc  <- st_read(dsn) %>%
    st_transform(2154) %>%
    glimpse()
  return(invisible(nc))
}
# déterminations des codes présents
# source("geo/scripts/onc35.R");ocs_paysages_raster_code()
ocs_paysages_raster_code <- function() {
  library(rio)
  library(tidyverse)
  carp()
  dsn <- sprintf('%s/classifpaysmodis2.tif', rennes2Dir)
  img <- raster(dsn)
#
# la nomenclature
  n.df <- ocs_paysages_raster_nomenclature_lire()
  v <- getValues(img) %>%
    glimpse()
  v <- as_tibble(v)
  carp('nrow: %s', nrow(v))
  if( nrow(v) < 1 ) {
    stop('***')
  }
  colnames(v) <- c('code')
  df <- v %>%
    group_by(code) %>%
    summarize(nb=n()) %>%
    arrange(nb) %>%
#    na.omit() %>%
    left_join(n.df)
    df %>%
    bind_rows(summarise(., nb = sum(nb), libelle='total')) %>%
    glimpse() %>%
    print(n=30)
  df2 <- df %>%
    group_by(libelle1) %>%
    summarize(nb=sum(nb)) %>%
    mutate(id='bbox') %>%
    spread(libelle1, nb, fill=0) %>%
    adorn_totals(c("row")) %>%
    adorn_percentages("row") %>%
    adorn_pct_formatting(rounding = "half up", digits = 0) %>%
    print(n=20)

}
# lecture de la nomenclature
ocs_paysages_raster_nomenclature_lire <- function(annee='2018') {
  carp()
  dsn <- sprintf("%s/nomenclature_mga.txt", rennes2Dir, annee);
  df <- read.table(dsn,header=TRUE, sep=";", blank.lines.skip = TRUE, comment.char= "", stringsAsFactors=FALSE, quote="", encoding="UTF-8")
  return(invisible(df))
}
# source("geo/scripts/onc35.R");ocs_paysages_legende()
ocs_paysages_legende <- function() {
  library(rio)
  library(tidyverse)
  carp()
  dsn <- sprintf('%s/bvi35/CouchesRennes2/classifpaysmodis2.tif', Drive)
  r  <- raster(dsn)
  dsn <- sprintf('%s/bvi35/CouchesRennes2/legende.txt', Drive)
  df <- import(dsn, encoding = "UTF-8") %>%
    glimpse()
  plot.new()
  couleurs <- colortable(r)[1:19] %>%
    glimpse()
  xref <- 0
  yref <- 0
  width <- 20
  height <- 10
  delta <- 2
  plot(c(0, 200), c(0, 250), type = "n", xlab = "", ylab = "")
  for (i in 1:(length(couleurs))){
    rect(xref, yref, xref + width, yref + height, col=couleurs[i])
    text(xref + width + delta , yref + height / 2, labels=couleurs[i], adj = c(0,0.5))
    df1 <- df %>%
      filter(code==couleurs[i])
    if(nrow(df1) > 0 ) {
      text(xref + width + delta +40 , yref + height / 2, labels=df1[1, 'libelle'], adj = c(0,0.5))
    }
    yref <- yref + height + delta
  }
}
# http://zevross.com/blog/2015/03/30/map-and-analyze-raster-data-in-r/
ocs_raster_vals <- function(raster) {
  require(raster)
  vals <- unique(values(raster)) %>%
    glimpse()
}
#
# ==================================================================================================
#
# carte des sols
# http://geowww.agrocampus-ouest.fr/web/?page_id=1725
# http://geowww.agrocampus-ouest.fr/geonetwork/apps/georchestra/?uuid=505375ac-6315-4484-b196-36db3ab459fa#
# http://geowww.agrocampus-ouest.fr/geoserver/wfs?typeName=sdb%3Aucs_pp_bzh&outputFormat=json&request=GetFeature&service=WFS&version=2.0.0
# http://geowww.agrocampus-ouest.fr/geoserver/wfs?typeName=sdb%3Aucs_pp_bzh&outputFormat=SHAPE-ZIP&request=GetFeature&service=WFS&version=2.0.0
#
# source("geo/scripts/onc35.R");ocs_sols_lire()
ocs_sols_lire <- function() {
  library(sf)
  library(tidyverse)
  carp()
  dsn <- sprintf('%s/bvi35/CouchesAgroCampus/ucs_pp_bzh.shp', Drive)
  nc  <- st_read(dsn) %>%
    glimpse()
}
#
# ==================================================================================================
# cesbio
# vecteur et raster
#
# http://osr-cesbio.ups-tlse.fr/~oso/
# http://osr-cesbio.ups-tlse.fr/echangeswww/TheiaOSO/vecteurs_2017/departement_35.zip
# pour 2019
# https://theia.cnes.fr/atdistrib/rocket/#/search?location=FRANCE&page=1&collection=OSO&startDate=2019-01-01&completionDate=2019-12-31&typeOSO=RASTER&locationRASTER=FRANCE&geometry=POLYGON((-596683.1689384759%206247187.581899974,-386328.46709767083%206272870.423403793,-174750.77280430298%206234957.657374346,-101371.2256505338%206201936.8611551495,-94033.27093515685%206088198.563066808,-149067.93130048376%206028271.932891229,-273813.1614618914%205968345.302715651,-383882.4821925452%205968345.302715651,-502512.75009113876%206027048.940438666,-596683.1689384759%206112658.412118064,-596683.1689384759%206247187.581899974))
# pour 2020
# https://www.theia-land.fr/product/carte-doccupation-des-sols-de-la-france-metropolitaine/
# https://theia.cnes.fr/atdistrib/rocket/#/search?collection=OSO
ocs_cesbio_lire_sf <- function() {
  carp()
  if ( exists('cesbio.sf') ) {
    return(cesbio.sf)
  }
  cesbioDir <- sprintf("%s/cesbio", varDir)
  dsn <- sprintf('%s/%s.shp', cesbioDir, 'departement_35')
  carp("dsn: %s", dsn)
  nc <- st_read(dsn, stringsAsFactors=FALSE)
  nc <- st_transform(nc, 2154)
  glimpse(nc)
  cesbio.sf <<- nc
  return(invisible(nc))
}
# lecture de la nomenclature
ocs_cesbio_nomenclature_lire <- function(annee='2018') {
  carp()
  library(rio)
  dsn <- sprintf("%s/nomenclature_%s_mga.txt", osoDir, annee);
#  df <- read.table(dsn,header=TRUE, sep=";", blank.lines.skip = TRUE, comment.char= "", stringsAsFactors=FALSE, quote="", encoding="UTF-8")
  df <- import(dsn)
  return(invisible(df))
}
#
# le fichier CESBIO n'a qu'un layer
# https://books.google.fr/books?id=zbRkDwAAQBAJ&pg=PT264&lpg=PT264&dq=R+getValues+Raster&source=bl&ots=Czd3WSYVdx&sig=ACfU3U0PTBo3sEaCs2eHnb3AeHCkL0lrCw&hl=fr&sa=X&ved=2ahUKEwjLlYrOjfzhAhWQ3eAKHYysDGkQ6AEwCXoECAkQAQ#v=onepage&q=R%20getValues%20Raster&f=false
#
# changement de nomenclature en 2018
# source("geo/scripts/onc35.R");ocs_cesbio_code()
ocs_cesbio_code <- function(annee='2017') {
  library(sf)
  library(tidyverse)
  library(raster)
  carp()
  graphics.off()
  carp("l'image")
  rasterFic <-  sprintf("%s/OCS_%s_CESBIO.tif", osoDir, annee);
  carp("rasterFic:%s", rasterFic)
  imgCESBIO <- raster(rasterFic)
#
# la nomenclature
  n.df <- ocs_cesbio_nomenclature_lire(annee)
  xmin <- 350000
  xmax <- xmin + 10000
  ymin <- 6810000
  ymax <- ymin + 10000
  bbox.sfc <<- st_sfc(st_polygon(list(rbind(c(xmin, ymin), c(xmax, ymin), c(xmax, ymax), c(xmin, ymax), c(xmin, ymin)))), crs=st_crs(2154))
  bbox.sf <<- st_sf(id = 1:1, geometry = bbox.sfc)
  img1 <- crop(imgCESBIO, bbox.sf)
  v <- getValues(img1) %>%
    glimpse()
  v <- as_tibble(v)
  carp('nrow: %s', nrow(v))
  if( nrow(v) < 1 ) {
    stop('***')
  }
  colnames(v) <- c('code')
  df <- v %>%
    group_by(code) %>%
    summarize(nb=n()) %>%
    arrange(nb) %>%
#    na.omit() %>%
    left_join(n.df)
    df %>%
    bind_rows(summarise(., nb = sum(nb), libelle='total')) %>%
    glimpse() %>%
    print(n=30)
  df2 <- df %>%
    group_by(libelle1) %>%
    summarize(nb=sum(nb)) %>%
    mutate(id='bbox') %>%
    spread(libelle1, nb, fill=0) %>%
    adorn_totals(c("row")) %>%
    adorn_percentages("row") %>%
    adorn_pct_formatting(rounding = "half up", digits = 0) %>%
    print(n=20)
}
#
# découpe du fichier métropole pour la Bretagne
# buffer de 5 km autour
# compression LZW
# setwd("d:/web");source("geo/scripts/epoc.R"); ocs_cesbio_bzh_annees(test = 1)
ocs_cesbio_bzh_annees <- function(test = 2) {
  carp()
  annees <- c("2018", "2019", "2020")
  df1 <- data.frame()
  for ( annee in annees) {
    ocs_cesbio_bzh(annee = annee, test = test)
    df <- ocs_cesbio_bzh_stat(annee = annee, test = test) %>%
      mutate(annee = annee)
    df1 <- rbind(df1, df)
  }
  df2 <- df1 %>%
    pivot_wider(names_from = annee, values_from = c("nb"), values_fill = list(nb = 0)) %>%
    arrange(code) %>%
    glimpse()
  df3 <- fonds_oso_nomenclature_lire('2019') %>%
    glimpse() %>%
    dplyr::select(code, libelle, libelle1)
  df4 <- df2 %>%
    left_join(df3, by = c("code"))
  misc_print(df4)
}
# setwd("d:/web");source("geo/scripts/epoc.R"); ocs_cesbio_mtd(annee = 2019)
ocs_cesbio_mtd <- function(annee = "2019", test = 2) {
  carp()
  library(tidyverse)
  library(xml2)
  dsn <-  sprintf("%s/PUBLIC_METADATA/OSO_%s0101_RASTER_V1-0_MTD_ALL.xml", osoDir, annee);
  carp("dsn: %s", dsn)
  doc <- xml2::read_xml(dsn)
#       <Classes_Statistic_List classe="11">
  classes <- xml2::xml_find_all(doc, "//Classes_Statistic_List")

  carp("classes nb: %s", length(classes))
  for (classe in classes) {
    code <- xml_attr(classe, "classe")[[1]]
#         <Name>riz</Name>
    name <- xml_find_first(classe, ".//Name") %>%
      xml_text()
    carp("code: %s %s", code, name)
  }
}
ocs_cesbio_bzh <- function(annee = "2019", test = 2) {
  carp()
  library(tidyverse)
  library(raster)
  srcFic <-  sprintf("%s/OCS_%s.tif", osoDir, annee);
  carp("srcFic:%s", srcFic)
  dstFic <-  sprintf("%s/OCS_%s_bzh.tif", osoDir, annee);
  carp("dstFic:%s", dstFic)
  if (file.exists(dstFic) & test == 2) {
    return()
  }
  les_departements <- c("22","29","35","56")
  departements.sf <- ign_departement_lire_sf(les_departements) %>%
    st_transform(2154) %>%
    st_buffer(5000)
  bbox.sfc <- st_as_sfc(st_bbox(departements.sf)) %>%
    glimpse()
  bbox.sf <<- st_sf(id = 1:1, geometry = bbox.sfc)
  img <- raster(srcFic) %>%
    glimpse()
  img1 <- crop(img, bbox.sf)
  writeRaster(img1, dstFic, format = "GTiff", overwrite = TRUE, options = "COMPRESS=LZW")
}
# setwd("d:/web");source("geo/scripts/epoc.R"); ocs_cesbio_bzh_stat(annee = "2019", test = 2)
ocs_cesbio_bzh_stat <- function(annee = "2019", test = 2) {
  carp()
  library(tidyverse)
  library(raster)
  dsn <-  sprintf("%s/OCS_%s_bzh.tif", osoDir, annee);
  carp("dsn:%s", dsn)
  img <- raster(dsn) %>%
    glimpse()
  if ( test == 1) {
    plotImg(img)
  }
  v <- getValues(img)
  glimpse(v)
  v <- as_tibble(v)
  colnames(v) <- c('code')
  colnames(v) <- c('code')
  df <- v %>%
    group_by(code) %>%
    summarize(nb=n()) %>%
    arrange(-nb) %>%
    na.omit() %>%
    glimpse()
}
ocs_cesbio_crop <- function(nc, marge=300, affiche=FALSE) {
  library(sf)
  library(tidyverse)
  library(raster)
  carp()
  graphics.off()
  carp("le buffer")
  b150.sf <<- st_buffer(nc, marge) %>%
    glimpse()
  carp("détermination de l'emprise")
  bbox <- st_bbox(nc)
  xmin <- bbox['xmin']-marge
  xmax <- bbox['xmax']+marge
  ymin <- bbox['ymin']-marge
  ymax <- bbox['ymax']+marge
  bbox.sfc <<- st_sfc(st_polygon(list(rbind(c(xmin, ymin), c(xmax, ymin), c(xmax, ymax), c(xmin, ymax), c(xmin, ymin)))), crs=st_crs(2154))
  bbox.sf <<- st_sf(id = 1:1, geometry = bbox.sfc)
  n.df <- ocs_cesbio_nomenclature_lire()
# lecture en multi-layers (brick)
  if ( ! exists("imgCESBIO") ) {
    rasterFic <-  sprintf("%s/OCS_2017_CESBIO.tif", osoDir);
    carp("rasterFic:%s", rasterFic)
    imgCESBIO <<- raster(rasterFic)
  }
# en deux étapes pour les performances
  img1 <- crop(imgCESBIO, bbox.sf)
  img1 <- mask(img1, nc)
#  img1 <- trim(img1, values = NA)
  if ( affiche ) {
    plotImg(img1)
    plot(st_geometry(nc), lwd = 3, col="transparent", add=TRUE)
    plot(st_geometry(bbox.sf), lwd = 3, col="transparent", add=TRUE, density=8)
  }
  v <- getValues(img1) %>%
    glimpse()
  v <- as_tibble(v)
  carp('nrow: %s', nrow(v))
  if( nrow(v) < 1 ) {
    carp('*** intersection vide')
    stop('***')
  }
  colnames(v) <- c('code')
  id <- as.character(nc$id[1])
  df <- v %>%
    group_by(code) %>%
    summarize(nb=n()) %>%
    arrange(nb) %>%
    na.omit() %>%
    left_join(n.df) %>%
    bind_rows(summarise(., nb = sum(nb), libelle='total')) %>%
    mutate(id=id) %>%
    glimpse() %>%
    print(n=20)
  return(df)
}
#
# découpe du raster par un polygone
ocs_cesbio_raster <- function(nc, annee='2018',  affiche=FALSE) {
  library(sf)
  library(tidyverse)
  library(raster)
  p <- nc$id[1]
  carp('p: %s', p)
  glimpse(nc)
  graphics.off()
  n.df <- ocs_cesbio_nomenclature_lire(annee)
  glimpse(n.df)
# lecture en multi-layers (brick)
  if ( ! exists("imgCESBIO") ) {
    rasterFic <-  sprintf("%s/OCS_%s_CESBIO.tif", osoDir, annee);
    carp("rasterFic:%s", rasterFic)
    imgCESBIO <<- raster(rasterFic)
  }
  img1 <- crop(imgCESBIO, nc)
  img1 <- mask(img1, nc)
#  img1 <- trim(img1, values = NA)
  if ( affiche ) {
    plotImg(img1)
  }
  v <- getValues(img1)
  glimpse(v)
  v <- as_tibble(v)
  colnames(v) <- c('code')
  df <- v %>%
    group_by(code) %>%
    summarize(nb=n()) %>%
    arrange(nb) %>%
    na.omit() %>%
    left_join(n.df) %>%
    bind_rows(summarise(., nb = sum(nb), libelle='total')) %>%
    mutate(id=p) %>%
    glimpse() %>%
    print(n=20)
  return(df)
}
# source("geo/scripts/ika.R");ocs_cesbio_raster_parcours_lire()
# https://github.com/sfirke/janitor
# https://cran.r-project.org/web/packages/janitor/vignettes/tabyls.html
ocs_cesbio_stat <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  dsn <- sprintf("%s/ocs_cesbio.Rds", varDir)
  carp("dsn: %s", dsn)
  df <- readRDS(dsn) %>%
    glimpse()
  carp("verification des libelles")
  df1 <- df %>%
    na.omit() %>%
#    dplyr::select(parcours, code, libelle, nb) %>%
    group_by(code, libelle, libelle1) %>%
    summarize(nb=sum(nb)) %>%
    print(n=20)
  carp("les ratios")
  df1 <- df %>%
    na.omit() %>%
#    dplyr::select(parcours, code, libelle, nb) %>%
    group_by(id, libelle) %>%
    summarize(nb=sum(nb)) %>%
    spread(libelle, nb, fill=0) %>%
    arrange(id) %>%
    adorn_totals(c("row")) %>%
    adorn_percentages("row") %>%
    adorn_pct_formatting(rounding = "half up", digits = 0) %>%
    print(n=20)
  tex_df2table(df1, num=TRUE)
}
#
# ==================================================================================================
#
# Zone Armorique
#
# source("geo/scripts/onc35.R");ocs_zaar_jour()
ocs_zaar_jour <- function() {
  carp()
  library(sf)
  library(tidyverse)
  nc <- ocs_zaar_shp_lire()
}
ocs_zaar_contour <- function() {
  carp()
  library(sf)
  library(tidyverse)
  nc <- ocs_zaar_shp_lire()
  nc <- st_buffer(nc, 10) %>%
    st_union() %>%
    glimpse()
  plot(st_geometry(nc))
  dsn <- sprintf('%s/%s.geojson', zaarDir, 'contour')
  st_write(st_transform(nc, 4326), dsn, delete_dsn=TRUE)
}
ocs_zaar_shp_lire <- function() {
  carp()
  library(sf)
  library(tidyverse)
  dsn <- sprintf('%s/ZAAR_ARMORIQUE_SUD.shp', zaarDir)
  carp('dsn: %s', dsn)
  nc  <- st_read(dsn) %>%
    glimpse()
  return(invisible(nc))
}
#
## les actions génériques
# =============================================================================
#
#
ocs_actions <- function(df, nc, parcours, pdfDir, test, affiche) {
  for (i in 1:nrow(df)) {
    e <- df[i, "fonction"]
    fond <- df[i, "fond"]
#    test <- df[i, 'test']
    param <- df[i, "param"]
    marge <- df[i, "marge"]
    buffer <- df[i, "buffer"]
    carp("parcours: %s fond: %s", parcours, fond)
    dsn <- sprintf("%s/%s_%s.pdf", pdfDir, parcours, fond)
    if ( test != 1 && file.exists(dsn)) {
      next
    }
    buffer.sf <<- st_buffer(nc, buffer)
#    plot(st_geometry(buffer.sf), lwd = 3, col="transparent"); stop('***')
#    carp("test: %s dsn: %s", test, dsn)
    fonction <- sprintf("ocs_action_%s", e)
    if( ! exists(fonction, mode = "function") ) {
      carp("fonction inconnue: %s", fonction)
      stop("***")
    }
    df1 <- do.call(fonction, list(nc = nc, parcours = parcours, affiche = affiche, df = df[i, ]))
    if(affiche == TRUE)  {
      plot(st_geometry(buffer.sf), lwd = 3, col = "transparent", add = TRUE)
      dsn <- sprintf("%s/%s_%s_buffer.pdf", pdfDir, parcours, fond)
      dev.copy(pdf, dsn, width = par("din")[1], height = par("din")[2])
      graphics.off()
      carp("dsn: %s", dsn)
    }
#    stop('******')
    surface <- as.numeric(st_area(buffer.sf))
    df1$Parcours <- parcours
    df1$Fond <- fond
    df1$Surface <- surface
    stats.list[[fond]] <<- rbind(stats.list[[fond]], df1)
  }
#  stop('***')
}
#
# stat sur un shapefile pour le ruban
ocs_action_stat <- function(nc, parcours, df, nc1, affiche = TRUE) {
  nc2 <- st_intersection(st_make_valid(nc1), buffer.sf)
  if(affiche == TRUE) {
    plot(st_geometry(nc2), lwd = 3, col = nc2$rgb, border = NA)
  }
  longueur <- st_length(nc)
  surface <- st_area(buffer.sf)
  df <- nc2 %>%
    mutate(surface=st_area(.)) %>%
    st_drop_geometry() %>%
    group_by(ocs) %>%
    summarize(surface = as.numeric(sum(surface)))
  return(invisible(df))
}
#
# le cas du fichier json bocage_haie
# https://geo.data.gouv.fr/fr/datasets/2d99521c51a95715d2fc459bdbb0966e49d8f772
ocs_action_bocage_haie <- function(nc, parcours, df, affiche = affiche) {
  dsn <- sprintf(df[1, "param"], varDir, parcours)
  Carp("parcours: %s dsn: %s", parcours, dsn)
  nc1 <- st_read(dsn, quiet = TRUE) %>%
    glimpse()
#    rename(ocs=libelle1)
  plot(st_geometry(nc1), lwd = 3, border = NA)
  stop("***")
  return(invisible())
}
#
# le cas du fichier raster du cbnb
# pb d'effet de bord
ocs_action_cbnb <- function(nc, parcours, df, affiche=affiche) {
  img <- ocs_fonds_raster(nc, parcours, df, affiche)
#  stop('***')
# en deux étapes pour les performances
#  img2 <- crop(imgCESBIO, bbox.sf)
  img1 <- mask(img, buffer.sf)
#  img1 <- trim(img1, values = NA)
  if (affiche) {
    plotImg(img1)
    plot(st_geometry(nc), lwd = 3, col = "darkblue", add = TRUE)
#    stop('***')
  }
  df <- ocs_raster2values(img1)
  glimpse(df); stop("****")
  return(invisible(df))
}
#
# le cas du fichier shapefile du cbnbrest
ocs_action_cbnbrest <- function(nc, parcours, df, affiche = affiche) {
  dsn <- sprintf(df[1, "param"], varDir, parcours)
  Carp("parcours: %s dsn: %s", parcours, dsn)
  nc1 <- st_read(dsn, quiet = TRUE) %>%
    rename(ocs = CLASS_NAME)
#    rename(ocs=libelle1)
  df1 <- ocs_action_stat(nc, parcours, df, nc1, affiche = affiche)
  return(invisible(df1))
}
#
# le cas du fichier raster du cesbio
ocs_action_cesbio <- function(nc, parcours, df, affiche=affiche) {
  Carp("ocs_action_cesbio parcours: %s", parcours)
  img <- ocs_fonds_raster(nc, parcours, df, affiche)

  img1 <- mask(img, buffer.sf)
#  img1 <- trim(img1, values = NA)
  if (affiche) {
    plotImg(img1)
    plot(st_geometry(nc), lwd = 3, col = "darkblue", add = TRUE)
#    stop('***')
  }
  df <- ocs_raster2values(img1)
  return(invisible(df))
}
#
# le cas d'un fichier raster
# type corine land cover qui n'est pas en projection Lambert 93 !!!
ocs_action_img <- function(nc, parcours, df, affiche=affiche) {
  fond <- df[1, 'fond']
  marge <- df[1, 'marge']
  buffer <- df[1, 'buffer']
  Carp("ocs_action_img parcours: %s fond: %s", parcours, fond)
  img <- fonds_cache.list[[fond]]
  crs_img <- crs(img)
  crs_nc <- st_crs(nc)
  nc1 <- nc %>%
    st_transform(crs_img)
  buffer.sf <- st_buffer(nc1, buffer)
  Carp("découpe du raster avec une marge")
  img1 <- crop(img, st_buffer(nc1, marge))
  img1 <- mask(img1, buffer.sf)
  if (affiche) {
    plotImg(img1)
    plot(st_geometry(nc1), lwd = 3, col = "darkblue", add = TRUE)
    plot(st_geometry(buffer.sf), lwd = 3, border = "darkblue", add = TRUE)
  }
  img <<- img1
  df <- geo_raster2values(img1)
#  glimpse(df); stop("****")
  return(invisible(df))
}
#
# le cas du fichier shapefile du rpg_2017
ocs_action_rpg <- function(nc, parcours, df, affiche = affiche) {
  nc1 <- cartes_fonds_rpg(nc, parcours, df, affiche = affiche) %>%
    rename(ocs = code_group)
  df1 <- ocs_action_stat(nc, parcours, df, nc1, affiche = affiche)
  return(invisible(df1))
}
#
# le cas d"une image de type brick
ocs_fonds_img <- function(nc, parcours, df, affiche = TRUE) {
  library(raster)
  dsn <- sprintf(df[1, "param"], varDir, parcours)
  Carp("ocs_fonds_img: %s dsn: %s", parcours, dsn)
  img <- brick(dsn)
  plotImg(img)
  plot(st_geometry(nc), lwd = 8, col = "darkblue", add = TRUE)
  geo_echelle()
  legend("topright", legend = parcours, cex = 3, text.col = "darkblue", box.lwd = 0, box.col = "transparent", bg = "transparent")
  cartes_fonds_troncons(parcours, nc)
#  stop("***")
}
#
# le cas d'une image de type raster
ocs_fonds_raster <- function(nc, parcours, df, affiche = TRUE) {
  library(raster)
  dsn <- sprintf(df[1, "param"], varDir, parcours)
  Carp("ocs_fonds_raster parcours: %s dsn: %s", parcours, dsn)
  img <- raster(dsn)
  if (affiche == TRUE) {
    par(mar = c(0, 0, 0, 0), oma = c(0, 0, 0, 0))
    plotImg(img)
    plot(st_geometry(nc), lwd = 8, col = "darkblue", add = TRUE)
    geo_echelle()
    legend("topright", legend = parcours, cex = 3, text.col = "darkblue", box.lwd = 0, box.col = "transparent", bg = "transparent")
#    stop("***")
  }
  return(invisible(img))
#  stop("***")
}
#
# le cas d'une image de type raster clc
ocs_fonds_raster_clc_______ <- function(nc, parcours, df, affiche = TRUE) {
  library(raster)
  img <- fonds_cache.list[[ogc_layer]]
  return(invisible(img))
#  stop("***")
}


#
## les statistiques
#
# source("geo/scripts/stoc.R"); ocs_stats_type_stat(type='points', stat='cesbio')
# source("geo/scripts/stoc.R"); ocs_stats_type_stat(type='dpts', stat='cesbio')
ocs_stats_type_stat <- function(type = "parcours", stat = "cesbio") {
  carp()
  library(tidyverse)
  library(janitor)
  dsn <- sprintf("%s/ocs_%s_stats.Rds", varDir, type)
  carp("dsn: %s", dsn)
  stats.list <- readRDS(dsn) %>%
    glimpse()
  df <- stats.list[[stat]] %>%
    glimpse()
  if(grepl("cesbio2018", stat)) {
    df1 <- fonds_oso_nomenclature_lire('2018') %>%
      glimpse() %>%
      dplyr::select(code, libelle, libelle1)
  }
  if(grepl("cesbio2019", stat)) {
    df1 <- fonds_oso_nomenclature_lire('2019') %>%
      glimpse() %>%
      dplyr::select(code, libelle, libelle1)
  }
  if(grepl("clc2018", stat)) {
    df1 <- clc_gpkg_codes_lire() %>%
      dplyr::select(code = GRID_CODE, libelle = LABEL3, libelle1 = LABEL1) %>%
      glimpse()
  }
  tex_df2kable(df1, num = TRUE, suffixe = sprintf("%s", stat))
  carp("codes sans libellé")
  df3 <- df %>%
    na.omit() %>%
    left_join(df1, by=c("code" = "code")) %>%
    filter(is.na(libelle)) %>%
    glimpse()
  carp("répartition par libellé")
  df2 <- df %>%
    na.omit() %>%
    left_join(df1, by=c("code" = "code")) %>%
    mutate(surface = nb) %>%
    group_by(libelle1, libelle) %>%
    summarize(surface = sum(surface)) %>%
#      spread(ocs, surface, fill = 0) %>%
    adorn_totals(c("row")) %>%
#      adorn_percentages("row") %>%
#      adorn_pct_formatting(rounding = "half up", digits = 0) %>%
    print(n=20)
  tex_df2kable(df2, num = TRUE, suffixe = sprintf("%s_%s_detail", type, stat))
  carp("répartition par regroupement de libellé")
  df2 <- df %>%
    na.omit() %>%
    left_join(df1, by=c("code" = "code")) %>%
    mutate(ocs = libelle1, surface = nb) %>%
    group_by(Parcours, ocs) %>%
    summarize(surface = sum(surface)) %>%
    spread(ocs, surface, fill = 0) %>%
    adorn_totals(c("row")) %>%
    adorn_percentages("row") %>%
    adorn_pct_formatting(rounding = "half up", digits = 0) %>%
    print(n=20)
  tex_df2kable(df2, num = TRUE, suffixe = sprintf("%s_%s", type, stat))
}
#
# les ocs par département
# les ocs pour les départements
# setwd("d:/web");source("geo/scripts/epoc.R"); ocs_jour_dpts(test = 1)
ocs_jour_dpts <- function(test=2) {
  carp()
  ocs_dpts(type = "dpts", test = test)
  ocs_stats_type_stat(type='dpts', stat='cesbio2019')
}
# le cbn brest n'est disponible que sur le 35
ocs_dpts <- function(type = "dpts", test = 2) {
  carp()
  library(tidyverse)
  library(utf8)
  library(janitor)
  dsn <- sprintf("%s/ocs_%s_stats.Rds", varDir, type)
  carp("dsn: %s", dsn)
  if (file.exists(dsn) && test == 2) {
    return()
  }
  stats.list <<- list()
  les_departements <- c('22', '29', '35', '56')
#  les_departements <- c('35')
  nc <- ign_ade_lire_sf('DEPARTEMENT')
  dpts.sf <- filter(nc, INSEE_DEP %in% les_departements) %>%
    st_transform(2154) %>%
    rename(dpt = INSEE_DEP) %>%
    glimpse()
  dpts.df <- st_set_geometry(dpts.sf, NULL)
  actions.df <- read.table(text="fonction|fond|param|marge|buffer
img|cesbio2019|%s/ogc/%s_cesbio2019.tif|300|150
", header = TRUE, sep = "|", blank.lines.skip = TRUE, stringsAsFactors = FALSE, quote = "") %>%
    filter(!grepl("^#", fonction)) %>%
    glimpse()
  if(nrow(dpts.sf) == 0) {
    stop("***")
  }
  cartesDir <- sprintf("%s/ocs_dpts", texDir)
  dir.create(cartesDir, showWarnings = FALSE)
  nb_dpts <- nrow(dpts.sf)
  for (i in 1:nrow(dpts.sf)) {
    carp("dpt: %s %s/%s", dpts.sf[[i, "dpt"]], i, nb_dpts)
    dpt <- dpts.sf[[i, "dpt"]]
    ocs_actions(actions.df, dpts.sf[i, ], dpt, cartesDir, test = test, affiche = FALSE)
#    break
  }
  carp("les stats")
  glimpse(stats.list)
  saveRDS(stats.list, dsn)
  carp("dsn: %s", dsn)
}
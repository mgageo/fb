# <!-- coding: utf-8 -->
#
# quelques fonctions pour les wetlands - version 1 en sp
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
#
# fonctions packages
# ==================
#
install <-function() {
  require(devtools)
  install.packages("rgrass7", repos="http://R-Forge.R-project.org")
}
# limite des départements
ign_departement_lire <- function(les_departements) {
  require(sp)
  require(rgdal)
  dsn <-  sprintf('%s/web.var/geo/GEOFLA/DEPARTEMENT.SHP', Drive)
  layer <- ogrListLayers(dsn)
  spdf <- readOGR(dsn, layer=layer, stringsAsFactors=FALSE)
  spdf <- spdf[spdf@data$CODE_DEPT %in% les_departements, ]
  spdf <- spTransform(spdf, CRS("+init=epsg:2154"))
#  plot(spdf)
  return(spdf)
}
# bd Carthage PlanEau
# http://www.sandre.eaufrance.fr/atlas/srv/fre/catalog.search#/metadata/f12458f9-2757-42be-b468-eedb1eff1253
planeau_lire <- function() {
  require(sp)
  require(rgdal)
  dsn <-  sprintf('%s/bvi35/CouchesSandre/PlanEau.shp', Drive)
  layer <- ogrListLayers(dsn)
  spdf <- readOGR(dsn, layer=layer, stringsAsFactors=FALSE)
  spdf <- spTransform(spdf, CRS("+init=epsg:2154"))
#  plot(spdf)
  return(spdf)
}
#
# que les plans d'eau du 35
planeau35_ecrire <- function() {
  require(raster)
  dpt.spdf <- ign_departement_lire(c("35"))
  plot(dpt.spdf)
  planeau.spdf <- planeau_lire()
  spdf <- raster::intersect(planeau.spdf, dpt.spdf)
  spdf@data <- spdf@data[, colnames(planeau.spdf@data)]
  print(summary(spdf))
  plot(spdf, add=TRUE)
  spdf <- spTransform(spdf, CRS("+init=epsg:4326"))
  dsn <- sprintf("%s/PlanEau35.shp", varDir)
  writeOGR(spdf, dsn, layer = 'donnees', driver="ESRI Shapefile", overwrite_layer=TRUE)
}

#
# affectation de sites élémentaires aux sites fonctionnels
# rm(list=ls()); source("geo/scripts/wetlands.R");test()
test <- function() {
  require(raster)
  dsn <- sprintf("%s/export.geojson", varDir)
  etangs.spdf <- ogr_lire(dsn, "wkbPolygon")
  etangs.spdf <- etangs.spdf[etangs.spdf$surface > 50000, ]
  dsn <- sprintf("%s/sites35.geojson", varDir)
  sites.spdf <- ogr_lire(dsn, "wkbPolygon")
#  print(head(sites.spdf))
  etangs.spdf$id <- 1:nrow(etangs.spdf)
#
# les étangs avec un point commun avec un site fonctionnel
  inter.spdf <- raster::intersect(etangs.spdf, sites.spdf)
  print(sprintf(" inter.spdf nrow : %s", nrow(inter.spdf@data)))
  inter.spdf$NAT_ID <- sprintf("%s/%s", inter.spdf$NOM, inter.spdf$SITE)
  df <- inter.spdf@data[, c("id", "NAT_ID")]
  print(sprintf(" inter.spdf df nrow : %s", nrow(df)))
  avec.spdf <- etangs.spdf[inter.spdf$id, ]
  print(sprintf(" avec.spdf nrow : %s", nrow(avec.spdf@data)))
  avec.spdf@data$NAT_ID <- df$NAT_ID
#
# les étangs sans point commun avec un site fonctionnel
  sans.spdf <- etangs.spdf[! etangs.spdf$id %in% inter.spdf$id, ]
  sans.spdf$NAT_ID <- sprintf("%s/%s", "35", "3500")
  print(sprintf(" sans.spdf nrow : %s", nrow(sans.spdf@data)))
  spdf <- rbind(avec.spdf[, c("name", "NAT_ID")], sans.spdf[, c("name", "NAT_ID")])
  spdf$LOCAL_ID <- iconv(spdf$name, to="ASCII//TRANSLIT//IGNORE")
  spdf$REFERENT <- 'jeanluc.chateigner@free.fr'
  spdf$ID <- 1:nrow(spdf)
  spdf <- spTransform(spdf, CRS("+init=epsg:4326"))
  c <- coordinates(spdf)
  colnames(c) <- c("LON", "LAT")
  spdf <- cbind(spdf, c)
  spdf <- spdf[, c("ID", "NAT_ID", "LOCAL_ID", "LAT", "LON", "REFERENT")]
  dsn <- sprintf("%s/sitesosm.geojson", varDir)
  ogr_ecrire(spdf, dsn)
  dsn <- sprintf("%s/sitesosm.geojson", leafletDir)
  ogr_ecrire(spdf, dsn)
  print(head(spdf@data, 120))
}
#
# la base des étangs de Jean-Luc
etangs <- function() {
  require(sp)
  require(rgdal)
  require(readxl)
  require(raster)
  dsn <- sprintf("%s/Etangs35.xlsx", varDir)
  df <- read_excel(dsn, sheet = "etangs")
  colnames(df) <- c("zone", "site", "sous_site", "site_actuel", "carré", "lon", "lat", "znieff", "interet")
  df <- df[grepl('^\\d+$', df$zone), ]
  df$lon <- as.numeric(df$lon)
  df$lat <- as.numeric(df$lat)
  inconnu.df <- subset(df, is.na(df$lon))
  if ( nrow(inconnu.df) > 0 ) {
    print(sprintf("etangsl() lon nb: %d", nrow(inconnu.df)))
    print(head(inconnu.df))
  }
  df <- subset(df, ! is.na(df$lon))
  coordinates(df) = ~ lon + lat
  spdf <- df
  proj4string(spdf) <- CRS("+init=epsg:4326")
  pe.spdf <- spTransform(spdf, CRS("+init=epsg:2154"))
  dsn <- sprintf("%s/polygones_fonctionnels.shp", varDir)
  wi.spdf <- ogr_lire(dsn, "wkbPolygon")
  spdf <- raster::intersect(pe.spdf, wi.spdf)
  df <- spdf@data[, c("sous_site", "zone", "SITE")]
  dpt.spdf <- ign_departement_lire(c("35"))
  plot(dpt.spdf)
  plot(wi.spdf, add=TRUE)
  text(coordinates(pe.spdf), labels=pe.spdf@data$site_actuel, cex=1, font=2)
}

#
# lecture d'un fichier ogr
ogr_lire <- function(dsn, geomType="wkbPolygon") {
  require(rgdal)
  require(rgeos)
  print(sprintf("ogr_lire() dsn:%s", dsn))
  layer <- ogrListLayers(dsn)
#  Log(sprintf("relation_lire() %s %s", layer, dsn))
#  spdf <- readOGR(dsn, layer=layer, stringsAsFactors=FALSE, use_iconv=TRUE, encoding="UTF-8", require_geomType=geomType)
  spdf <- readOGR(dsn, layer=layer, stringsAsFactors=FALSE, require_geomType=geomType)
  spdf <- spTransform(spdf, CRS("+init=epsg:2154"))
  return(invisible(spdf))
}
#
# écriture d'un fichier ogr
ogr_ecrire <- function(spdf, dsn) {
  require(rgdal)
  require(rgeos)
  print(sprintf("ogr_ecrire() dsn:%s", dsn))
  spdf <- spTransform(spdf, CRS("+init=epsg:4326"))
  writeOGR(spdf, dsn, layer = 'donnees', driver="GeoJSON", overwrite_layer=TRUE, layer_options= c(encoding= "UTF-8"))
  return(invisible(spdf))
}
#
# les données en provenance d'un export overpass d'openstreetmap
export <- function() {
  dsn <- sprintf("%s/way.geojson", varDir)
  way.spdf <- ogr_lire(dsn, "wkbPolygon")
  dsn <- sprintf("%s/relation.geojson", varDir)
  spdf <- ogr_lire(dsn, "wkbPolygon")
  relation.spdf <- tour(spdf)
  plot(spdf[2,], add=FALSE)
  relation.spdf@data <- relation.spdf@data[, c("id", "name")]
  way.spdf@data <- way.spdf@data[, c("id", "name")]
  spdf <- rbind(relation.spdf, way.spdf)
  spdf@data$surface <- as.integer(rgeos::gArea(spdf, byid=TRUE))
  plot(spdf[2,], add=TRUE, lwd=2)
#  spdf@data$name <- iconv(spdf@data$name, "UTF-8")
  Encoding(spdf@data$name) <- "UTF-8"
  dsn <- sprintf("%s/export.geojson", varDir)
  ogr_ecrire(spdf, dsn)
}
#
# récupération du tour dans le cas des multi-polygones
# https://stackoverflow.com/questions/12663263/dissolve-holes-in-polygon-in-r
# plot(SpatialPolygons(list(Polygons(list(buf@polygons[[1]]@Polygons[[1]]),ID=1))),lwd=2)
# https://gis.stackexchange.com/questions/194848/creating-outside-only-buffer-around-polygon-using-r
# https://gis.stackexchange.com/questions/224048/deleting-inner-holes-rings-borders-of-a-spatial-polygon-in-r
tour <- function(spdf) {
  nb <- length(spdf@polygons )
  tour <- list()
  for (i in 1:nb) {
#    print(sprintf("i %d", i))
    tour[[i]] <- Polygons(list(spdf@polygons[[i]]@Polygons[[1]]),ID=i)
  }
  tour.sp <- SpatialPolygons(tour)
  tour.spdf <- SpatialPolygonsDataFrame(tour.sp, data=spdf@data,match.ID=FALSE)
  proj4string(tour.spdf) <- CRS("+init=epsg:2154")
  return(invisible(tour.spdf))
}
grass <- function() {
  require(rgdal)
  require(rgeos)
  require(rgrass7)
  loc <-initGRASS("d:/grass7", home=getwd(), override=TRUE)
  dsn <- sprintf("%s/export.geojson", varDir)
  spdf <- ogr_lire(dsn, "wkbPolygon")
#  spdf <- spdf[spdf$name == "La Cantache", ]
  writeVECT(SDF = spdf, vname = "spdf", v.in.ogr_flags = c("o", "overwrite"))
  execGRASS(cmd = "v.generalize", flags = c("overwrite"),
    input = "spdf", output = "simple", threshold = 50, method = "reduction")
  simple.spdf <- readVECT(vname = "simple")
  proj4string(simple.spdf) <- CRS("+init=epsg:2154")
  plot(spdf)
  plot(simple.spdf, add=TRUE, border="red")
  simple.spdf <- spTransform(simple.spdf, CRS("+init=epsg:4326"))
#  Encoding(simple.spdf@data$name) <- "UTF-8"
  dsn <- sprintf("%s/simple.geojson", varDir)
  writeOGR(simple.spdf, dsn, layer = 'donnees', driver="GeoJSON", overwrite_layer=TRUE, layer_options= c(encoding= "UTF-8"))
}
sites35 <- function() {
  dsn <- sprintf("%s/polygone sites WI.shp", varDir)
  spdf <- ogr_lire(dsn, "wkbPolygon")
  spdf <- spdf[spdf$DEPT == "35", ]
  dsn <- sprintf("%s/sites35.geojson", varDir)
  spdf <- ogr_ecrire(spdf, dsn)

}
leaflet <- function() {
  dsn <- sprintf("%s/export.geojson", varDir)
  spdf <- ogr_lire(dsn, "wkbPolygon")
  spdf$name <- iconv(spdf$name, to="ASCII//TRANSLIT//IGNORE")
  dsn <- sprintf("%s/export.geojson", leafletDir)
  spdf <- spTransform(spdf, CRS("+init=epsg:4326"))
  writeOGR(spdf, dsn, layer = 'donnees', driver="GeoJSON", overwrite_layer=TRUE, layer_options= c(encoding= "UTF-8"))
  dsn <- sprintf("%s/polygones_fonctionnels.shp", varDir)
  wi.spdf <- ogr_lire(dsn, "wkbPolygon")
  wi.spdf <- spTransform(wi.spdf, CRS("+init=epsg:4326"))
  dsn <- sprintf("%s/sitesfonct.geojson", leafletDir)
  writeOGR(wi.spdf, dsn, layer = 'donnees', driver="GeoJSON", overwrite_layer=TRUE, layer_options= c(encoding= "UTF-8"))
}

#
mga  <- function() {
  source("geo/scripts/wetlands.R");
}
#
# fonctions packages
# ==================
#
install <-function() {
  require(devtools)
  install.packages("rgrass7", repos="http://R-Forge.R-project.org")
}
# limite des départements
ign_departement_lire <- function(les_departements) {
  require(sp)
  require(rgdal)
  dsn <-  sprintf('%s/web.var/geo/GEOFLA/DEPARTEMENT.SHP', Drive)
  layer <- ogrListLayers(dsn)
  spdf <- readOGR(dsn, layer=layer, stringsAsFactors=FALSE)
  spdf <- spdf[spdf@data$CODE_DEPT %in% les_departements, ]
  spdf <- spTransform(spdf, CRS("+init=epsg:2154"))
#  plot(spdf)
  return(spdf)
}
# bd Carthage PlanEau
# http://www.sandre.eaufrance.fr/atlas/srv/fre/catalog.search#/metadata/f12458f9-2757-42be-b468-eedb1eff1253
planeau_lire <- function() {
  require(sp)
  require(rgdal)
  dsn <-  sprintf('%s/bvi35/CouchesSandre/PlanEau.shp', Drive)
  layer <- ogrListLayers(dsn)
  spdf <- readOGR(dsn, layer=layer, stringsAsFactors=FALSE)
  spdf <- spTransform(spdf, CRS("+init=epsg:2154"))
#  plot(spdf)
  return(spdf)
}
#
# que les plans d'eau du 35
planeau35_ecrire <- function() {
  require(raster)
  dpt.spdf <- ign_departement_lire(c("35"))
  plot(dpt.spdf)
  planeau.spdf <- planeau_lire()
  spdf <- raster::intersect(planeau.spdf, dpt.spdf)
  spdf@data <- spdf@data[, colnames(planeau.spdf@data)]
  print(summary(spdf))
  plot(spdf, add=TRUE)
  spdf <- spTransform(spdf, CRS("+init=epsg:4326"))
  dsn <- sprintf("%s/PlanEau35.shp", varDir)
  writeOGR(spdf, dsn, layer = 'donnees', driver="ESRI Shapefile", overwrite_layer=TRUE)
}

#
# affectation de sites élémentaires aux sites fonctionnels
# rm(list=ls()); source("geo/scripts/wetlands.R");test()
test <- function() {
  require(raster)
  dsn <- sprintf("%s/export.geojson", varDir)
  etangs.spdf <- ogr_lire(dsn, "wkbPolygon")
  etangs.spdf <- etangs.spdf[etangs.spdf$surface > 50000, ]
  dsn <- sprintf("%s/sites35.geojson", varDir)
  sites.spdf <- ogr_lire(dsn, "wkbPolygon")
#  print(head(sites.spdf))
  etangs.spdf$id <- 1:nrow(etangs.spdf)
#
# les étangs avec un point commun avec un site fonctionnel
  inter.spdf <- raster::intersect(etangs.spdf, sites.spdf)
  print(sprintf(" inter.spdf nrow : %s", nrow(inter.spdf@data)))
  inter.spdf$NAT_ID <- sprintf("%s/%s", inter.spdf$NOM, inter.spdf$SITE)
  df <- inter.spdf@data[, c("id", "NAT_ID")]
  print(sprintf(" inter.spdf df nrow : %s", nrow(df)))
  avec.spdf <- etangs.spdf[inter.spdf$id, ]
  print(sprintf(" avec.spdf nrow : %s", nrow(avec.spdf@data)))
  avec.spdf@data$NAT_ID <- df$NAT_ID
#
# les étangs sans point commun avec un site fonctionnel
  sans.spdf <- etangs.spdf[! etangs.spdf$id %in% inter.spdf$id, ]
  sans.spdf$NAT_ID <- sprintf("%s/%s", "35", "3500")
  print(sprintf(" sans.spdf nrow : %s", nrow(sans.spdf@data)))
  spdf <- rbind(avec.spdf[, c("name", "NAT_ID")], sans.spdf[, c("name", "NAT_ID")])
  spdf$LOCAL_ID <- iconv(spdf$name, to="ASCII//TRANSLIT//IGNORE")
  spdf$REFERENT <- 'jeanluc.chateigner@free.fr'
  spdf$ID <- 1:nrow(spdf)
  spdf <- spTransform(spdf, CRS("+init=epsg:4326"))
  c <- coordinates(spdf)
  colnames(c) <- c("LON", "LAT")
  spdf <- cbind(spdf, c)
  spdf <- spdf[, c("ID", "NAT_ID", "LOCAL_ID", "LAT", "LON", "REFERENT")]
  dsn <- sprintf("%s/sitesosm.geojson", varDir)
  ogr_ecrire(spdf, dsn)
  dsn <- sprintf("%s/sitesosm.geojson", leafletDir)
  ogr_ecrire(spdf, dsn)
  print(head(spdf@data, 120))
}
#
# la base des étangs de Jean-Luc
etangs <- function() {
  require(sp)
  require(rgdal)
  require(readxl)
  require(raster)
  dsn <- sprintf("%s/Etangs35.xlsx", varDir)
  df <- read_excel(dsn, sheet = "etangs")
  colnames(df) <- c("zone", "site", "sous_site", "site_actuel", "carré", "lon", "lat", "znieff", "interet")
  df <- df[grepl('^\\d+$', df$zone), ]
  df$lon <- as.numeric(df$lon)
  df$lat <- as.numeric(df$lat)
  inconnu.df <- subset(df, is.na(df$lon))
  if ( nrow(inconnu.df) > 0 ) {
    print(sprintf("etangsl() lon nb: %d", nrow(inconnu.df)))
    print(head(inconnu.df))
  }
  df <- subset(df, ! is.na(df$lon))
  coordinates(df) = ~ lon + lat
  spdf <- df
  proj4string(spdf) <- CRS("+init=epsg:4326")
  pe.spdf <- spTransform(spdf, CRS("+init=epsg:2154"))
  dsn <- sprintf("%s/polygones_fonctionnels.shp", varDir)
  wi.spdf <- ogr_lire(dsn, "wkbPolygon")
  spdf <- raster::intersect(pe.spdf, wi.spdf)
  df <- spdf@data[, c("sous_site", "zone", "SITE")]
  dpt.spdf <- ign_departement_lire(c("35"))
  plot(dpt.spdf)
  plot(wi.spdf, add=TRUE)
  text(coordinates(pe.spdf), labels=pe.spdf@data$site_actuel, cex=1, font=2)
}

#
# lecture d'un fichier ogr
ogr_lire <- function(dsn, geomType="wkbPolygon") {
  require(rgdal)
  require(rgeos)
  print(sprintf("ogr_lire() dsn:%s", dsn))
  layer <- ogrListLayers(dsn)
#  Log(sprintf("relation_lire() %s %s", layer, dsn))
#  spdf <- readOGR(dsn, layer=layer, stringsAsFactors=FALSE, use_iconv=TRUE, encoding="UTF-8", require_geomType=geomType)
  spdf <- readOGR(dsn, layer=layer, stringsAsFactors=FALSE, require_geomType=geomType)
  spdf <- spTransform(spdf, CRS("+init=epsg:2154"))
  return(invisible(spdf))
}
#
# écriture d'un fichier ogr
ogr_ecrire <- function(spdf, dsn) {
  require(rgdal)
  require(rgeos)
  print(sprintf("ogr_ecrire() dsn:%s", dsn))
  spdf <- spTransform(spdf, CRS("+init=epsg:4326"))
  writeOGR(spdf, dsn, layer = 'donnees', driver="GeoJSON", overwrite_layer=TRUE, layer_options= c(encoding= "UTF-8"))
  return(invisible(spdf))
}
#
# les données en provenance d'un export overpass d'openstreetmap
export <- function() {
  dsn <- sprintf("%s/way.geojson", varDir)
  way.spdf <- ogr_lire(dsn, "wkbPolygon")
  dsn <- sprintf("%s/relation.geojson", varDir)
  spdf <- ogr_lire(dsn, "wkbPolygon")
  relation.spdf <- tour(spdf)
  plot(spdf[2,], add=FALSE)
  relation.spdf@data <- relation.spdf@data[, c("id", "name")]
  way.spdf@data <- way.spdf@data[, c("id", "name")]
  spdf <- rbind(relation.spdf, way.spdf)
  spdf@data$surface <- as.integer(rgeos::gArea(spdf, byid=TRUE))
  plot(spdf[2,], add=TRUE, lwd=2)
#  spdf@data$name <- iconv(spdf@data$name, "UTF-8")
  Encoding(spdf@data$name) <- "UTF-8"
  dsn <- sprintf("%s/export.geojson", varDir)
  ogr_ecrire(spdf, dsn)
}
#
# récupération du tour dans le cas des multi-polygones
# https://stackoverflow.com/questions/12663263/dissolve-holes-in-polygon-in-r
# plot(SpatialPolygons(list(Polygons(list(buf@polygons[[1]]@Polygons[[1]]),ID=1))),lwd=2)
# https://gis.stackexchange.com/questions/194848/creating-outside-only-buffer-around-polygon-using-r
# https://gis.stackexchange.com/questions/224048/deleting-inner-holes-rings-borders-of-a-spatial-polygon-in-r
tour <- function(spdf) {
  nb <- length(spdf@polygons )
  tour <- list()
  for (i in 1:nb) {
#    print(sprintf("i %d", i))
    tour[[i]] <- Polygons(list(spdf@polygons[[i]]@Polygons[[1]]),ID=i)
  }
  tour.sp <- SpatialPolygons(tour)
  tour.spdf <- SpatialPolygonsDataFrame(tour.sp, data=spdf@data,match.ID=FALSE)
  proj4string(tour.spdf) <- CRS("+init=epsg:2154")
  return(invisible(tour.spdf))
}
grass <- function() {
  require(rgdal)
  require(rgeos)
  require(rgrass7)
  loc <-initGRASS("d:/grass7", home=getwd(), override=TRUE)
  dsn <- sprintf("%s/export.geojson", varDir)
  spdf <- ogr_lire(dsn, "wkbPolygon")
#  spdf <- spdf[spdf$name == "La Cantache", ]
  writeVECT(SDF = spdf, vname = "spdf", v.in.ogr_flags = c("o", "overwrite"))
  execGRASS(cmd = "v.generalize", flags = c("overwrite"),
    input = "spdf", output = "simple", threshold = 50, method = "reduction")
  simple.spdf <- readVECT(vname = "simple")
  proj4string(simple.spdf) <- CRS("+init=epsg:2154")
  plot(spdf)
  plot(simple.spdf, add=TRUE, border="red")
  simple.spdf <- spTransform(simple.spdf, CRS("+init=epsg:4326"))
#  Encoding(simple.spdf@data$name) <- "UTF-8"
  dsn <- sprintf("%s/simple.geojson", varDir)
  writeOGR(simple.spdf, dsn, layer = 'donnees', driver="GeoJSON", overwrite_layer=TRUE, layer_options= c(encoding= "UTF-8"))
}
sites35 <- function() {
  dsn <- sprintf("%s/polygone sites WI.shp", varDir)
  spdf <- ogr_lire(dsn, "wkbPolygon")
  spdf <- spdf[spdf$DEPT == "35", ]
  dsn <- sprintf("%s/sites35.geojson", varDir)
  spdf <- ogr_ecrire(spdf, dsn)

}
leaflet <- function() {
  dsn <- sprintf("%s/export.geojson", varDir)
  spdf <- ogr_lire(dsn, "wkbPolygon")
  spdf$name <- iconv(spdf$name, to="ASCII//TRANSLIT//IGNORE")
  dsn <- sprintf("%s/export.geojson", leafletDir)
  spdf <- spTransform(spdf, CRS("+init=epsg:4326"))
  writeOGR(spdf, dsn, layer = 'donnees', driver="GeoJSON", overwrite_layer=TRUE, layer_options= c(encoding= "UTF-8"))
  dsn <- sprintf("%s/polygones_fonctionnels.shp", varDir)
  wi.spdf <- ogr_lire(dsn, "wkbPolygon")
  wi.spdf <- spTransform(wi.spdf, CRS("+init=epsg:4326"))
  dsn <- sprintf("%s/sitesfonct.geojson", leafletDir)
  writeOGR(wi.spdf, dsn, layer = 'donnees', driver="GeoJSON", overwrite_layer=TRUE, layer_options= c(encoding= "UTF-8"))
}

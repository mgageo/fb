# <!-- coding: utf-8 -->
#
# quelques fonctions d'interrogation de faune-bretagne
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
## la liste des départements du site
#
# source("geo/scripts/visionature.R");biolo6_lieux_jour()
biolo6_lieux_jour <- function() {
  carp()
  biolo6_lieux_get()
  biolo6_lieux_parse()
}
# source("geo/scripts/visionature.R");biolo6_lieux_get()
biolo6_lieux_get <- function(force = TRUE) {
  carp()
  biolo_handle_ff(force = TRUE)
  url <- sprintf("%s/index.php?m_id=6", biolo_url)
  dsn <- sprintf("%s/lieux_lire.html", biolo6Dir);
  if( ! file.exists(dsn) || force == TRUE) {
    biolo_get(url, dsn)
  }
}
# source("geo/scripts/visionature.R");biolo6_lieux_parse()
biolo6_lieux_parse <- function(force = TRUE) {
  library(rvest)
  dsn <- sprintf("%s/lieux_lire.html", biolo6Dir);
  carp("dsn: %s", dsn)
  pg <- read_html(dsn)
  main_table <- html_node(pg, "#td-main-table")
  div_rows <- html_nodes(main_table, ".row")
  if ( length(div_rows) < 3 ) {
    return(FALSE);
  }
  df <- data.frame()
}

# <!-- coding: utf-8 -->
#
# quelques fonctions pour les exports
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
##
export_session <- FALSE
#
## les traitements journaliers
#
# les données saisies sur faune-bretagne
# source("geo/scripts/fbzh.R");export_jour(annee = "2023", force = FALSE)
export_jour <- function(annee = odf_annee, force = TRUE) {
  library(tidyverse)
  carp("début")
}
#
# source("geo/scripts/fbzh.R");export_test()
export_test <- function(force = TRUE) {
  carp()
  library(tidyverse)
  carp("début")
  annee <- "2023"
  depuis <- "01.01.2023"
  fin <- "02.01.2023"
  dpt <- "35"
  dsn <- sprintf("%s/export_test.xlsx", bioloDir)
  carp("dsn: %s", dsn)
  url <- export_url_periode_fbzh(depuis = "01.01.2023", fin = "02.01.2023")
#  export_post_curl(url = url, dest = dsn, force = force)
  export_fbzh_get(dest = dsn, force = TRUE)
}
#
# source("geo/scripts/fbzh.R"); url <- export_url_periode_fbzh() %>% glimpse()
export_url_periode_fbzh <- function(dpt = "35", depuis = "01.01.2023", fin = "02.01.2023", tg = 1, DCa = 1, Format = "XLSX") {
  tpl <- sprintf("%s/biolo_periode_fbzh_post_tpl.txt", tplDir)
  if (! file.exists(tpl)) {
    confess("tpl: %s", tpl)
  }
  parametres <- readLines(tpl)
  tpl <- sprintf("%s/biolo_dpt.csv", tplDir)
  if (! file.exists(tpl)) {
    confess("tpl: %s", tpl)
  }
  dpt.df <- rio::import(tpl) %>%
    filter(dpt == !!dpt) %>%
    glimpse()
  if (nrow(dpt.df) != 1) {
    confess("tpl: %s", tpl)
  }
  DateSynth <- strftime(Sys.Date(), "%d.%m.%Y")
  variables <- list(
    tg = tg,
    DCa = DCa,
    Format = Format,
    DateSynth = DateSynth,
    DFrom = depuis,
    DTo = fin,
    cC = dpt.df[1, "cC"]
  )
  parametres <- misc_list2tpl(variables, parametres)
#  parametres <- stringr::str_glue(parametres)
  writeLines(parametres)
  url <- paste(parametres, collapse = "&")
#  browseURL(
#    u,
#    browser = "C:/Program Files/Mozilla Firefox/firefox.exe"
#  )
  return(url)
}
#
# le post de demande
# source("geo/scripts/fbzh.R");export_post_curl()
export_post_curl <- function(url, dest, force = TRUE) {
  library(curl)
  h <- export_session_fbzh_curl(force = force)
# le referer
  url_referer <- sprintf("https://bzh.biolovision.net/index.php?m_id=94&%s", url)
  h <- h %>%
    curl::handle_setopt(customrequest = "GET")
  curl_download(url_referer, destfile = "d:/export_post_curl.html", handle = h)
# en POST pour avoir le fichier
  url_post <- sprintf("https://bzh.biolovision.net/index.php?m_id=97&%s", url)
  h <- h %>%
    curl::handle_setopt(customrequest = "POST")
  h <- handle_setheaders(h,
    "referer" = url_referer
  )
  dsn <- sub("\\.[^.]+$", ".jsonb", dest)
  curl_download(url_post, destfile =dsn, handle = h)
  carp("dsn: %s", dsn)
}
# source("geo/scripts/fbzh.R");export_session_fbzh_curl()
export_session_fbzh_curl <- function(force = TRUE) {
  library(curl)
  library(rvest)
  url <- sprintf("%s/index.php", biolo_url)
  h <- curl::new_handle(verbose = FALSE)
  h <- handle_setheaders(h,
    "user-agent" = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/115.0",
    "Accept" = "application/json, text/javascript, */*; q=0.01",
    "Accept-Language" = "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
    "Accept-Encoding" = "gzip, deflate, br"
  )
  curl::handle_setform(h,
    "login" = "1",
    "USERNAME" = USERNAME,
    "PASSWORD" = PASSWORD,
    "login_button" = "Me connecter"
  )
  dsn <- "d:/export_session_fbzh_curl.html"
  req <- curl_download(url, destfile = dsn, handle = h)
  page <- read_html(dsn)
  statut <- page %>%
    html_node(".profile-card-container") %>%
    html_text()
  carp("statut: %s", statut)
  return(invisible(h))
}
#
# récupération d'un export
export_fbzh_get <- function(dest = FALSE, force = FALSE) {
  require(tidyverse)
  library(jsonlite)
  Carp("dest: %s", dest)
  if ( file.exists(dest) & force == FALSE ) {
    carp("*** fichier present: %s", dest)
    return()
  }
  if ( file.exists(dest)) {
    file.remove(dest)
  }
  dsn <- sub("\\.[^.]+$", ".jsonb", dest)
  if ( ! file.exists(dsn) ) {
    carp("*** fichier absent: %s", dsn)
    return()
  }
  df <- read_json(dsn)
  glimpse(df)
  file_name <- df["file_name"]
  carp("file_name: %s", file_name)
  h <- export_session_fbzh_curl ()
  url <- sprintf("%s/index.php?m_id=1472&content=export&id_file=%s", biolo_url, df["id"])
  carp("url: %s", url)
  for (i in seq(1, 1000)) {
    req <- curl_fetch_disk(url, dest, handle = h)
    carp("status: %s", req$status_code)
    if ( req$status_code == 200) {
      break
    }
    if ( file.exists(dest) & file.info(dest)$size > 0) {
      carp("*** fichier present: %s", dest)
      break
    }
    carp("attente: %d id_file: %s", i, df["id"])
    Sys.sleep(10)
  }
#  stop('***')
}
#
##
# tg = 1 : les oiseaux
# sp_ExportModel=30 OLD_BASIC+
# source("geo/scripts/fbzh.R");fbzh_export_donnees()
fbzh_export_donnees <- function(tg = 1, DCa = 0, force = TRUE) {
  carp("début")
  for (dpt in c("22", "29", "35", "56")) {
    fbzh_export_donnees_mois(tg = tg, DCa = DCa, dpt = dpt)
  }
}
#
# export version mensuelle
# source("geo/scripts/fbzh.R");fbzh_export_donnees_mois(dpt = "22")
fbzh_export_donnees_mois <- function(tg = 1, dpt = "35", DCa = 1, Format = "XLSX", force = FALSE) {
  carp("début")
  biolo_fbzh()
  exportDir <<- sprintf("%s/mois_tg%s_DCa%s", bioloDir, tg, DCa);
  dir.create(exportDir, showWarnings = FALSE, recursive = TRUE)
  format <- "%d.%m.%Y"
  date_jour <- as.Date("21.07.2017", format = format)
  date_jour <- Sys.Date()
  if (DCa == 0) {
    mois <- seq(as.Date("01.01.1900", format = format), date_jour, by = "1 month")
    mois <- seq(as.Date("01.01.2013", format = format), date_jour, by = "1 month")
  } else {
    mois <- seq(as.Date("01.05.2013", format = format), date_jour, by = "1 month")
    mois <- seq(as.Date("01.01.2023", format = format), date_jour, by = "1 month")
  }
# pour le mois en cours
  mois[length(mois) + 1] <- date_jour
  for (i in 1:(length(mois) - 1)) {
    from <- mois[i]
    to <- mois[i + 1] - 1
# le premier du mois
    if (from > to) {
      next
    }
    from <- format(from, format)
    to <- format(to, format)
    aaaamm <- format(mois[i], "%Y_%m")
    dsn <- sprintf("%s/%s.%s", exportDir, aaaamm, tolower(Format))
    dsn <- sprintf("%s/%s %s %s %s.%s", exportDir, dpt, aaaamm, from, to, tolower(Format))
    carp("%s %s %s %s", from, to, aaaamm, dsn)
    if (! file.exists(dsn)) {
      url <- export_url_periode_fbzh(depuis = from, fin = to, tg = tg, dpt = dpt, DCa = DCa, Format = Format)
      export_post_curl(url = url, dest = dsn, force = force)
      export_fbzh_get(dest = dsn, force = TRUE)
#      stop("****")
    }
  }
}
#
# conversion en format rds et création du fichier global en format rds
# source("geo/scripts/fbzh.R");fbzh_export_donnees_mois_rds(force = FALSE)
fbzh_export_donnees_mois_rds <- function(tg = 1, DCa = 1, Format = "XLSX", force = FALSE) {
  carp("début")
  library(lubridate)
  exportDir <<- sprintf("%s/mois_tg%s_DCa%s", bioloDir, tg, DCa);
  files <- list.files(exportDir, pattern = "\\.xlsx$" , full.names = TRUE, ignore.case = TRUE) %>%
    glimpse()
  if (length(files) < 1) {
    print(exportDir)
    confess("**** pas de fichiers: %s", exportDir)
  }
# une période (année/mois) peut comporter plusieurs fichiers
# il faut choisir celui avec la date d'extraction la plus récente
# 1900_01 01.01.1900 31.01.1900
  files.df <- data.frame(files, groupe = tg) %>%
    tidyr::extract(
      "files",
      c("dpt", "periode", "from", "to"),
      regex = "/(\\d\\d)\\s([\\d_]+)\\s+([\\d\\.]+)\\s+([\\d\\.]+)\\.",
      remove = FALSE,
      convert = FALSE
    ) %>%
    mutate(to_date = dmy(to))
# un seul fichier par période
  files.df <- files.df %>%
    group_by(dpt, periode) %>%
    arrange(dpt, periode, to_date) %>%
    filter(row_number() == n()) %>%
    mutate(rds = str_replace(files, "xlsx$", "Rds")) %>%
    ungroup() %>%
    glimpse()
#  stop("****")
  df1 <- data.frame()
  for (i in 1:nrow(files.df) ) {
    dsn <- files.df[[i, "files"]]
    rds_dsn <- files.df[[i, "rds"]]
    carp("i: %s/%s dsn: %s", i, nrow(files.df), dsn)
    if ( file.exists(rds_dsn) & force == FALSE) {
      df <- readRDS(rds_dsn)
    } else {
      df <- fbzh_export_lire(dsn, force = TRUE)
      saveRDS(df, file = rds_dsn)
    }
    Carp("dsn: %s nrow: %s", dsn, nrow(df))
    if (nrow(df) == 0) {
      next
    }
    df1 <- bind_rows(df1, df)
  }
  glimpse(df1)
  dsn <- sprintf("%s/mois_tg%s_DCa%s.Rds", bioloDir, tg, DCa)
  saveRDS(df1, file = dsn)
  carp("dsn: %s", dsn)
}
# lecture d'un export en format xlsx
fbzh_export_lire <- function(dsn, force = FALSE) {
  carp("début")
  library(janitor)
  library(rio)
  library(tidyverse)
  carp("dsn: %s", dsn)
  df <- rio::import(dsn)
  df <- df[-1,] %>%
    clean_names() %>%
    remove_empty(c("rows"))
  return(invisible(df))
}

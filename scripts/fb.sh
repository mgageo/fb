#!/bin/sh
# <!-- coding: utf-8 -->
#T fb : faune-bretagne
# auteur: Marc Gauthier
[ -f ../win32/scripts/misc.sh ] && . ../win32/scripts/misc.sh
[ -f ../win32/scripts/misc_pkg.sh ] && . ../win32/scripts/misc_pkg.sh
#f CONF:
CONF() {
  LOG "CONF debut"
  ENV
  CFG="VILAINEAVAL"
  [ -d "${CFG}" ] || mkdir "${CFG}"
  varDir="/d/bvi35/CouchesVilaineAval"
  varDir="/d/web.var/geo/fb"
  LOG "CONF fin"
}

#F e: edition des principaux fichiers
e() {
  LOG "e debut"
  E scripts/fb.sh
  for f in scripts/fb*.R ; do
    E $f
  done
  ( cd "${varDir}"; explorer . &)
  LOG "e fin"
}
FB_export() {
  _ENV_R
  ( cd ..; R --vanilla -e "source('geo/scripts/fb.R'); export_donnees();" )
#  ( cd ..; R --vanilla -e "source('geo/scripts/fb.R'); fusion_donnees();" )
}
#F GIT: pour mettre à jour le dépot git
GIT() {
  LOG "GIT debut"
  _git_lst
  bash ../win32/scripts/git.sh INIT $*
  LOG "GIT fin"
}
#f _git_lst: la liste des fichiers pour le dépot
_git_lst() {
  Local="${DRIVE}/web/geo"; Depot=fb; Remote=frama
  export Local
  export Depot
  export Remote
  cat  <<'EOF' > /tmp/_git.lst
scripts/fb.sh
EOF
  _ENV_R
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/fb.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/cheveche35.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/odf.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/visionature.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/stoc.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/epoc.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/gci35.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/apivn.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/wetlands.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/oncb.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/atlas.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/maree.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/fbzh.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/meteo.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/clochouette35.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/circus35.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/rapaces.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/rpg.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/ign.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/geoca.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/bmsm_rlc.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
#
  cat /tmp/_git.lst > /tmp/git.lst
  cat  <<'EOF' > /tmp/README.md
# fb : Faune-Bretagne

Scripts en environnement Windows 11 : MinGW R Texlive

Ces scripts exploitent des données en provenance :
- de Faune-Bretagne;
- de GéoBretagne et l'IGN pour la cartographie;
- de la LPO pour les protocoles EPOC-ODF/STOC/WATERBIRD

## Scripts R
Ces scripts sont dans le dossier "scripts".
Les points d'entrée sont :
- apivn : pour la base de données PostgreSQL issue de faune-bretagne
- atlas.R : pour les atlas depuis 1970
- bmsm_rlc : pour le comptage rlc en bmsm
- cheveche35.R : pour mes données de prospection chevêche
- circus35.R : pour le groupe FALCO
- clochouette35.R : pour le groupe clochouette35 de la LPO
- epoc.R : pour le format EPOC dont EPOC-ODF
- fb.R : divers traitements
- fbzh.R : pour le site miroir
- geoca.R : pour les colonies d'hirondelles
- gci35.R : pour les gravelots à collier interrompu
- ign : les données de l'ign
- maree.R : les horaires/hauteurs/coefficients des marées
- meteo.R : les données météo, météofrance, infoclimat ...
- odf.R : pour le site atlas oiseauxdefrance.org
- oncf.R : pour le protocole oncf dans faune-france
- rapaces.R : pour le protocole rapaces de la lpo
- rpg.R : pour les données de la Politique Agricole Commune
- stoc.R : pour le protocole STOC
- visionature.R : divers traitements
- wetlands.R : pour le protocole WATERBIRD

## Tex
Le langage Tex est utilisé pour la production des documents.

Texlive est l'environnement utilisé.


EOF
}
[ $# -eq 0 ] && ( HELP )
CONF
while [ "$1" != "" ]; do
  case $1 in
    -c | --conf )
      shift
      Conf=$1
      ;;
    * )
      $*
      exit 1
  esac
  shift
done
# <!-- coding: utf-8 -->
#
# la partie apivn
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# la totale
# source("geo/scripts/wetlands.R");apivn_jour();
apivn_jour <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  apivn_extract_wetlands()
  apivn_donnees_35()
  apivn_annees_sites_35()
}
#
# les participants
# source("geo/scripts/wetlands.R");apivn_places();
apivn_places <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  library(rio)
  obs.df <- misc_lire("faune_fb_observateurs", dir = "d:/bvi35/CouchesONCB") %>%
    glimpse()
  df <- apivn_extract_table_places_json() %>%
    filter(grepl("\\(ROE\\)$", name)) %>%
    filter(grepl("^35", insee)) %>%
    filter(grepl("^2024", updated_date)) %>%
    filter(last_updated_by == "30") %>%
    arrange(updated_date) %>%
    left_join(obs.df, by = c("last_updated_by" = "universal_id_observer")) %>%
    glimpse()
  misc_print(df)
}
#
# pour umap
# source("geo/scripts/wetlands.R");apivn_places_umap();
apivn_places_umap <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  library(rio)
  elementaires.sf <- apivn_extract_table_places_json_wetlands() %>%
    st_transform(2154) %>%
    st_make_valid() %>%
    glimpse()
  dsn <- sprintf('%s/fonctionnels.geojson', varDir)
  fonctionnels.sf <- st_read(dsn, stringsAsFactors = FALSE) %>%
    select(fonct=name, -dsn) %>%
#    mutate(fonct=iconv(fonct, from="UTF-8", to='ASCII//TRANSLIT//IGNORE')) %>%
#    mutate(fonct=toupper(fonct)) %>%
    st_transform(2154) %>%
    st_make_valid() %>%
    glimpse()
  sf_use_s2(FALSE)
  nc1 <- elementaires.sf %>%
    st_join(fonctionnels.sf, largest = TRUE) %>%
    dplyr::select(name, fonct) %>%
    st_transform(4326) %>%
    glimpse()
  nc2 <- nc1 %>%
    filter(fonct != "Baie du Mont-Saint-Michel") %>%
    glimpse()
  dsn <- sprintf('%s/Sites intérieurs.geojson', varDir)
  st_write(nc2, dsn, delete_dsn = TRUE)
  nc3 <- nc1 %>%
    filter(fonct == "Baie du Mont-Saint-Michel") %>%
    glimpse()
  dsn <- sprintf('%s/Baie du Mont-Saint-Michel.geojson', varDir)
  st_write(nc3, dsn, delete_dsn = TRUE)
}
#
# les données
# source("geo/scripts/wetlands.R");apivn_donnees_get();
apivn_donnees_get <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  library(rio)
  df <- apivn_extract_wetlands()
}
#
# l'analyse par année et par site
# source("geo/scripts/wetlands.R"); df <- apivn_annees_sites_35();
apivn_annees_sites_35 <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  library(rio)
  df <- misc_lire("apivn_extract_wetlands") %>%
    filter(grepl("^35", insee)) %>%
    mutate(aaaa = strftime(date, format="%Y")) %>%
    glimpse()
  df1 <- df %>%
    group_by(place, aaaa) %>%
    summarize(nb = n(), nb_oiseaux = sum(count)) %>%
    arrange(desc(nb_oiseaux)) %>%
    ungroup() %>%
    glimpse()
  df2 <- df1 %>%
    arrange(place, aaaa) %>%
    dplyr::select(-nb) %>%
    pivot_wider(names_from = c("aaaa"), values_from = c("nb_oiseaux")) %>%
    select("place", sort(colnames(.))) %>%
    glimpse()
  misc_print(df2)
  return(invisible(df2))
}
#
# l'analyse par année
# source("geo/scripts/wetlands.R");apivn_donnees_35();
apivn_donnees_35 <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  library(rio)
  df <- misc_lire("apivn_extract_wetlands") %>%
    filter(grepl("^35", insee)) %>%
    mutate(aaaa = strftime(date, format="%Y")) %>%
    glimpse()
  df1 <- df %>%
    group_by(espece) %>%
    summarize(nb = n(), nb_oiseaux = sum(count)) %>%
    arrange(desc(nb_oiseaux)) %>%
    glimpse()
  misc_print(df1);stop("*****")
  df3 <- df %>%
    filter(grepl("Martin", place)) %>%
    dplyr::select(date_start, espece, count) %>%
    glimpse()
  misc_print(df3)
#  stop("*****")
  df1 <- df %>%
    group_by(place, aaaa) %>%
    summarize(nb = sum(count)) %>%
    pivot_wider(names_from = aaaa, values_from = nb) %>%
    ungroup() %>%
#    filter(`2024` != "NA") %>%
    adorn_totals() %>%
    glimpse()
  misc_print(df1)
  df3 <- df %>%
    filter(grepl("Martin", place)) %>%
    glimpse()
  misc_print(df3)
#  stop("*****")
  df2 <- df %>%
    filter(place == "Étang de Châtillon-en-Vendelais (ROE)") %>%
    group_by(espece, aaaa) %>%
    summarize(nb = sum(count)) %>%
    pivot_wider(names_from = aaaa, values_from = nb) %>%
    ungroup() %>%
#    filter(`2024` != "NA") %>%
    adorn_totals() %>%
    select("espece", sort(colnames(.))) %>%
    glimpse()
  misc_print(df2)
  especes <- "
Canard chipeau
Canard colvert
Canard pilet
Canard siffleur
Canard souchet
Foulque macroule
Fuligule milouin
Fuligule morillon
Sarcelle d'hiver
Tadorne de Belon
"
  especes.list <- str_split(trimws(especes), "\n")[[1]]
  df3 <- df %>%
#    filter(grepl("Martin", place)) %>%
#    filter(espece %in% especes.list) %>%
    group_by(espece, aaaa) %>%
    summarize(nb = sum(count)) %>%
    pivot_wider(names_from = aaaa, values_from = nb) %>%
    ungroup() %>%
#    filter(`2024` != "NA") %>%
    adorn_totals() %>%
    select("espece", sort(colnames(.))) %>%
    glimpse()
  misc_print(df3)
#  misc_df2xlsx(df1)

#  misc_df2xlsx(df1)
}
#
# l'analyse par groupe
# source("geo/scripts/wetlands.R");apivn_groupe(groupe = "Limicoles littoraux");
apivn_groupe <- function(groupe) {
  carp()
  library(tidyverse)
  library(janitor)
  library(rio)
  df <- misc_lire("apivn_extract_wetlands") %>%
    filter(grepl("^35", insee)) %>%
    mutate(aaaa = strftime(date, format="%Y")) %>%
    glimpse()
  dsn <- sprintf("%s/GoogleDrive/espece_groupe.xlsx", cfgDir)
  espece_groupe <- rio::import(dsn) %>%
    filter(groupe == !!groupe) %>%
    glimpse()
  df1 <- df %>%
    filter(espece %in% espece_groupe$espece) %>%
    group_by(espece, aaaa) %>%
    summarize(nb = sum(count)) %>%
    pivot_wider(names_from = aaaa, values_from = nb) %>%
    ungroup() %>%
#    filter(`2024` != "NA") %>%
    adorn_totals() %>%
    select("espece", sort(colnames(.))) %>%
    glimpse()
  misc_print(df1);stop("*****")

}
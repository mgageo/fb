# <!-- coding: utf-8 -->
#
# la partie fonds de carte
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
# source("geo/scripts/odf.R");qualitatif_jour()
qualitatif_jour <- function(force = FALSE) {
  carp()
  periode <- "breeding_new"
  qualitatif_especes(periode = periode, force = false)
}
# source("geo/scripts/odf.R");qualitatif_especes(periode = "breeding_new", force = TRUE)
qualitatif_especes <- function(periode = "breeding_new", force = FALSE) {
  library(tidyverse)
  library(raster)
  library(sf)
  carp("début")
  df <- api_especes_lire() %>%
    glimpse()
  bzh.sf <<- qualitatif_ogc_bzh_lire() %>%
    st_transform(2154)
  df1 <- data.frame()
  for (i in 1:nrow(df)) {
    df2 <- qualitatif_espece(df[i, ], periode = periode)
    if (nrow(df2) > 0 ) {
      df1 <- bind_rows(df1, df2)
#      break
    }
  }
  df1 <- df1 %>%
    glimpse() %>%
    mutate(PRE = as.integer(0)) %>%
    replace(is.na(.), 0) %>%
    dplyr::select(espece, PRE, POS, PRO, CER, mailles) %>%
    arrange(espece)
  periode <- "2019"
  dsn <- sprintf("%s/atlas%s/qualitatif%s_espece.csv", webDir, periode, periode)
  write.table(df1, dsn, sep = ";",  col.names = FALSE, row.names = FALSE, quote = FALSE)
}
qualitatif_espece <- function(df, periode = "breeding_new", force = TRUE) {
  library(tidyverse)
  periodeDir <- sprintf("%s/api_%s", varDir, periode)
  dsn <- sprintf("%s/espece_%s.json", periodeDir, df[[1, "code"]])
  carp("dsn: %s", dsn)
  df1 <- data.frame()
  if (! file.exists(dsn)) {
    carp("file.exists")
    return(invisible(df1))
  }
  nc <- st_read(dsn, quiet = TRUE) %>%
    st_transform(2154)
  nc1 <- st_join(nc, bzh.sf, left = FALSE)
  nc1 <- st_intersection(nc, bzh.sf)
  carp("nrow %s %s", nrow(nc1), df[[1, "common_name_fr"]])
  if (nrow(nc1) == 0) {
    carp("nrow")
    return(invisible(df1))
  }
  df1 <- nc1 %>%
    st_drop_geometry() %>%
    mutate(status = dplyr::recode(status,
      "Nicheur certain" = "CER",
      "Nicheur possible" = "POS",
      "Nicheur probable" = "PRO",
      "Presence" = "PRE",
    )) %>%
    group_by(status, mailles) %>%
    summarize(nb = n()) %>%
    pivot_wider(names_from = status, values_from = nb) %>%
    mutate(espece = df[[1, "common_name_fr"]])
  return(invisible(df1))
}
#
# j'utilise la carte des mailles
qualitatif_ogc_bzh_lire <- function() {
  carp()
  library(tidyverse)
  library(sf)
  dsn <- sprintf("%s/ign_bzh.geojson", varDir)
  nc <- api_area_bzh()
  nc <- nc %>%
    mutate(region = "bzh") %>%
    group_by(region) %>%
    summarize(mailles = n()) %>%
    st_transform(2154)
  return(invisible(nc))
}
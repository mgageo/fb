# <!-- coding: utf-8 -->
#
# quelques fonction pour la météo
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# metScanR : base mondaile des stations météo
# https://github.com/jaroberti/metScanR
#
# rm(list=ls()); source("geo/scripts/meteo.R"); metScanR_jour()
metScanR_jour  <- function(force=FALSE) {
  metScanR_verif()
}
# rm(list=ls()); source("geo/scripts/meteo.R"); metScanR_install()
metScanR_install  <- function(force=FALSE) {
  install.packages('metScanR')
}
# rm(list=ls()); source("geo/scripts/meteo.R"); metScanR_siten()
metScanR_site  <- function(force=FALSE) {
#  library(metScanR)
  library(tidyverse)
  updateDatabase()
# Dinard
  scenario1 <- siteFinder(lat=48.6333,lon=-2.0667,radius=100) # retourne 1 station : Rennes
  metScanR::mapSiteFinder(scenario1)
}
#
# Rennes : plus rien après 2017
# rm(list=ls()); source("geo/scripts/meteo.R"); metScanR_station()
metScanR_station  <- function(force=FALSE) {
#  library(metScanR)
  library(tidyverse)
  metScanR::getStation(siteID='FR000007130')
}
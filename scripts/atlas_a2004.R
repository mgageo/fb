# <!-- coding: utf-8 -->
#
# atlas
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
# traitements des données atlas 2004-2008
#
#
#
# setwd("d:/web"); source("geo/scripts/atlas.R");a2004_jour(force = TRUE)
a2004_jour <- function(force = TRUE) {
  carp()
  a2004_carres35_lire()
  a2004_carres_alpha_utm_lire()
}
#
#
a2004_carres35_lire <- function(force = TRUE) {
  library(tidyverse)
  library(data.table)
  carp()
  dsn <- sprintf("%s/atlas2004/GO35/carres35.csv", couchesDir)
  df <- fread(dsn, encoding = "UTF-8") %>%
    glimpse()
}
#
#
# setwd("d:/web"); source("geo/scripts/atlas.R");a2004_carres_alpha_utm_lire(force = TRUE)
a2004_carres_alpha_utm_lire <- function(force = TRUE) {
  library(tidyverse)
  library(data.table)
  carp()
  dsn <- sprintf("%s/atlas2004/GO35/carres_alpha_utm.csv", couchesDir)
  df <- fread(dsn, encoding = "UTF-8") %>%
    glimpse()
}
#
# setwd("d:/web"); source("geo/scripts/atlas.R");a2004_grille_bzh_lire()
a2004_grille_bzh_lire <- function(force = TRUE) {
  library(tidyverse)
  library(sf)
  carp()
  dsn <- sprintf("%s/atlas2004/GOB/gob_carres.geojson", couchesDir)
  nc <- st_read(dsn, stringsAsFactors = FALSE, quiet = TRUE) %>%
    dplyr::select(-tessellate, -extrude, -visibility)
  return(invisible(nc))
}
#
# setwd("d:/web"); source("geo/scripts/atlas.R");a2004_grille_d35_lire()
a2004_grille_d35_lire <- function(force = TRUE) {
  library(tidyverse)
  library(sf)
  carp()
  dsn <- sprintf("%s/atlas2004/GOB/gob_carres_35.geojson", couchesDir)
  nc <- st_read(dsn, stringsAsFactors = FALSE, quiet = TRUE)
  return(invisible(nc))
}
#
# setwd("d:/web"); source("geo/scripts/atlas.R");a2004_qualitatif_lire(force = TRUE)
# le fichier comporte les simple présence
a2004_qualitatif_lire <- function(force = TRUE) {
  library(tidyverse)
  library(data.table)
  carp()
  dsn <- sprintf("%s/atlas2004/qualitatif2004.csv", couchesDir)
  df <- fread(dsn, encoding = "UTF-8") %>%
    replace_na(list(pre = 0, pos = 0, pro = 0, cer = 0)) %>%
    filter(pre != 1) %>%
    dplyr::select(-pre) %>%
    mutate(status = pos + 2 * pro + 4 * cer) %>%
    mutate(status = dplyr::recode(status,
      "1" = "Nicheur possible",
      "2" = "Nicheur probable",
      "4" = "Nicheur certain",
      )) %>%
    mutate(couleur = dplyr::recode(status,
      "Nicheur certain" = "red",
      "Nicheur possible" = "yellow",
      "Nicheur probable" = "orange",
      "Presence" = "blue",
      ))
#
#  mailles.df <- df %>%
#    group_by(maille) %>%
#    summarize(nb = n()) %>%
#    glimpse()
#
  df <- atlas_espece_recode(df)
  return(invisible(df))
}
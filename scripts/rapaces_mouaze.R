# <!-- coding: utf-8 -->
#
# quelques fonctions génériques pour
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# traitements pour l'enquête rapaces nocturnes  sur Mouazé février 2022
#
#
# source("geo/scripts/rapaces.R"); mouaze_jour()
mouaze_jour <- function() {
  carp()
}
#
# conversion des fichiers "circuits" saisis sous geoportail
# source("geo/scripts/rapaces.R"); mouaze_points_ecrire()
mouaze_points_ecrire <- function() {
  library(tidyverse)
  library(sf)
  carp()
  circuits <- c("A", "B")
  nc1 <- data.frame()
  for (circuit in circuits) {
    dsn <- sprintf("%s/Mouazé/circuit_%s.kml", varDir, circuit)
    nc <- st_read(dsn) %>%
      filter(Description != "") %>%
      mutate(Name = Description) %>%
      glimpse()
    dsn <- sprintf("%s/Mouazé/%s.kml", varDir, circuit)
    st_write(nc, dsn, delete_dsn = TRUE)
    nc1 <- rbind(nc1, nc)
  }
  dsn <- sprintf("%s/Mouazé/%s.kml", varDir, "points")
  st_write(nc1, dsn, delete_dsn = TRUE)
}
#
# lecture de l'ensemble des points
# source("geo/scripts/rapaces.R"); mouaze_points_lire()
mouaze_points_lire <- function() {
  library(tidyverse)
  library(sf)
  carp()
  dsn <- sprintf("%s/Mouazé/%s.kml", varDir, "points")
  nc <- st_read(dsn) %>%
    st_transform(2154)
  return(invisible(nc))
}
#
# setwd("d:/web");source("geo/scripts/rapaces.R"); mouaze_points_ogc(2)
mouaze_points_ogc <- function(test = 2) {
  carp()
  library(tidyverse)
  cartesDir <- sprintf("%s/couches_communes", texDir)
  dir.create(cartesDir, showWarnings = FALSE)
  nc <- mouaze_points_lire()
  nc1 <<- nc %>%
    st_buffer(150)
  fonds.df <- mouaze_points_couches_fonds()
  ogcDir <<- sprintf("%s/ogc", varDir)
  dir.create(ogcDir, showWarnings = FALSE)
  for (i in 1:nrow(nc)) {
    site <- nc[[i, "Name"]]
    carp("i: %s site: %s", i,  site)
#    fonds_ogc_fonds(fonds.df, nc[i,], site, test)
    mouaze_points_fonds(fonds.df, nc[i,], site = site, pdfDir = cartesDir, test = test)
#    break
  }
}
#
# la configuration des cartes
mouaze_points_couches_fonds <- function(test=2) {
  carp()
  df <- read.table(text="fonction|pdf|fond|param|marge|size_max
read|img|geo_plan|%s/ogc/%s_%s.tif|500|1024
read|img|geo_photos|%s/ogc/%s_%s.tif|500|1024
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors = FALSE, quote = "") %>%
    filter(!grepl('^#', fonction)) %>%
    glimpse()
  return(invisible(df))
}
# génération des différents pdf
mouaze_points_fonds <- function(df, nc, site, pdfDir, test) {
  carp("site: %s test: %s", site, test)
  for (i in 1:nrow(df)) {
    e <- df[i, "pdf"]
    fond <- df[i, "fond"]
#    test <- df[i, "test"]
    param <- df[i, "param"]
    marge <- df[i, "marge"]
    carp("fond: %s", fond)
    dsn <- sprintf("%s/%s_%s.pdf", pdfDir, site, fond)
    if (test != 1 & file.exists(dsn)) {
      next
    }
#    carp("test: %s dsn: %s", test, dsn)
    fonction <- sprintf("mouaze_points_%s", e)
    if (! exists(fonction, mode = "function")) {
      carp("fonction inconnue: %s", fonction)
      stop("***")
    }
#    carp("fonction: %s", fonction);stop("mmmmm")
    img <- do.call(fonction, list(nc = nc, site = site, df = df[i, ]))
    dev.copy(pdf, dsn, width = par("din")[1], height = par("din")[2])
    carp("dsn: %s %sx%s", dsn, par("din")[1], height = par("din")[2])
#    stop("***")
    graphics.off()
  }
#  stop("***")
}
#
# le cas d'une image de type brick
mouaze_points_img <- function(nc, site, df, affiche = TRUE) {
  library(raster)
  Carp("site: %s", site)
  dsn <- sprintf(df[1, "param"], varDir, site, df[1, "fond"])
  Carp("dsn: %s", dsn)
  img <- brick(dsn)
  img2 <- reclassify(img, cbind(NA, 0))
  plotImg(img2)
  plot(st_geometry(nc), lwd = 3, col = "transparent", add = TRUE)
  plot(st_geometry(nc1), lwd = 1, col = "transparent", add = TRUE)
  geo_echelle()
  legend("topright", legend = site, cex = 3, text.col = "darkblue", box.lwd = 0, box.col = "transparent", bg = "transparent")
}
# la limite de la commune
# source("geo/scripts/rapaces.R"); mouaze_lire()
mouaze_lire <- function() {
  library(tidyverse)
  library(sf)
  carp()
  insee <- "35197"
  nc <- ign_adminexpress_lire_sf(layer = 'COMMUNE') %>%
    filter(INSEE_COM == insee) %>%
    glimpse()
  return(invisible(nc))
}
#
# setwd("d:/web");source("geo/scripts/rapaces.R"); mouaze_ogc(2)
mouaze_ogc <- function(test = 2) {
  carp()
  library(tidyverse)
  cartesDir <- sprintf("%s/couches_communes", texDir)
  dir.create(cartesDir, showWarnings = FALSE)
  nc <- mouaze_lire()
  fonds.df <- mouaze_couches_fonds()
  ogcDir <<- sprintf("%s/ogc", varDir)
  dir.create(ogcDir, showWarnings = FALSE)
  for (i in 1:nrow(nc)) {
    site <- nc[[i, "INSEE_COM"]]
    carp("i: %s site: %s", i,  site)
#    fonds_ogc_fonds(fonds.df, nc[i,], site, test)
    mouaze_cartes_fonds(fonds.df, nc[i,], site = site, pdfDir = cartesDir, test = test)
#    break
  }
}
#
# la configuration des cartes
mouaze_couches_fonds <- function(test=2) {
  carp()
  df <- read.table(text="fonction|pdf|fond|param|marge|size_max
read|img|geo_plan|%s/ogc/%s_%s.tif|300|3072
read|img|geo_photos|%s/ogc/%s_%s.tif|300|3072
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors = FALSE, quote = "") %>%
    filter(!grepl('^#', fonction)) %>%
    glimpse()
  return(invisible(df))
}
# génération des différents pdf
mouaze_cartes_fonds <- function(df, nc, site, pdfDir, test) {
  carp("site: %s test: %s", site, test)
  for (i in 1:nrow(df)) {
    e <- df[i, "pdf"]
    fond <- df[i, "fond"]
#    test <- df[i, "test"]
    param <- df[i, "param"]
    marge <- df[i, "marge"]
    carp("fond: %s", fond)
    dsn <- sprintf("%s/%s_%s.pdf", pdfDir, site, fond)
    if (test != 1 & file.exists(dsn)) {
      next
    }
#    carp("test: %s dsn: %s", test, dsn)
    fonction <- sprintf("mouaze_cartes_%s", e)
    if (! exists(fonction, mode = "function")) {
      carp("fonction inconnue: %s", fonction)
      stop("***")
    }
#    carp("fonction: %s", fonction);stop("mmmmm")
    img <- do.call(fonction, list(nc = nc, site = site, df = df[i, ]))
    dev.copy(pdf, dsn, width = par("din")[1], height = par("din")[2])
    carp("dsn: %s %sx%s", dsn, par("din")[1], height = par("din")[2])
#    stop("***")
    graphics.off()
  }
#  stop("***")
}
#
# le cas d'une image de type brick
mouaze_cartes_img <- function(nc, site, df, affiche = TRUE) {
  library(raster)
  Carp("site: %s", site)
  dsn <- sprintf(df[1, "param"], varDir, site, df[1, "fond"])
  Carp("dsn: %s", dsn)
  img <- brick(dsn)
  img2 <- reclassify(img, cbind(NA, 0))
  plotImg(img2)
  plot(st_geometry(nc), lwd = 3, col = "transparent", add = TRUE)
  nc1 <- mouaze_grille_pas(1000)
  plot(st_geometry(nc1), lwd = 1, col = "transparent", add = TRUE)
#  nc2 <- faune_exports_sf()
#  plot(st_geometry(nc2), lwd = 10, col = "black", add = TRUE)
#  geo_echelle()
  legend("topright", legend = site, cex = 3, text.col = "darkblue", box.lwd = 0, box.col = "transparent", bg = "transparent")
}
#
# génération de la grille
# source("geo/scripts/rapaces.R"); mouaze_grille_ecrire()
mouaze_grille_pas <- function(pas = 600) {
  nc <- mouaze_lire()
  e <- fonds_extent(nc, marge=pas, aligne=1) %>%
    glimpse()
  grille.sfc <- st_make_grid(e, cellsize=pas) %>%
    st_set_crs(2154) %>%
    glimpse()
  grille.sf <- st_as_sf(data.frame(no=c(1:length(grille.sfc)), geometry=grille.sfc)) %>%
    mutate(X=map_dbl(geometry, ~st_centroid(.x)[[1]]), Y=map_dbl(geometry, ~st_centroid(.x)[[2]])) %>%
    mutate(X=round((X-pas/2)/100, 0), Y=round((Y-pas/2)/100, 0)) %>%
    mutate(name=sprintf("E%sN%s", X, Y)) %>%
    st_set_crs(2154) %>%
    glimpse()
  carp('sf1 apres intersection')
  sf1 <- st_intersection(grille.sf, nc) %>%
    dplyr::select(no) %>%
    glimpse()
  carp('les mailles entières')
  sf2 <- grille.sf[grille.sf$no %in% sf1$no, ] %>%
    glimpse()
  sf2 <- st_transform(sf2, 4326) %>%
    st_cast("POLYGON")
  dsn <- sprintf("%s/grille_l93_%s.geojson", varDir, pas)
  st_write(sf2, dsn, delete_dsn=TRUE)
  nc1 <- sf2 %>%
    st_transform(2154)
  return(invisible(nc1))
}
mouaze_grille_lire <- function(pas = 1000) {
  dsn <- sprintf("%s/grille_l93_%s.geojson", varDir, pas)
  nc1 <- st_read(dsn) %>%
    st_transform(2154)
  return(invisible(nc1))
}

#
## les données dans faune-bretagne
#
# source("geo/scripts/rapaces.R"); mouaze_faune()
mouaze_faune <- function(force = TRUE) {
  carp()
# extraction sur faune-bretagne
  biolo_fb()
  auteurs <- c(63, 13212)
  nc <- data.frame()
  for (auteur in auteurs) {
    dsn <- sprintf("%s/Mouazé/auteur_%s.xlsx", varDir, auteur)
    biolo_export_auteur(auteur_id = auteur, auteur_name = "Marc", dsn = dsn, depuis = '25.02.2022', fin = '25.02.2022', Format = 'XLSX', tg = '1', force = force)
    nc1 <- faune_export_lire_sf(dsn, force = TRUE)
    nc <- rbind(nc, nc1)
  }
  glimpse(nc)
  dsn <- sprintf("%s/Mouazé/auteurs.geojson", varDir)
  st_write(st_transform(nc, 4326), dsn, delete_dsn = TRUE, driver = 'GeoJSON')
}
# <!-- coding: utf-8 -->
#
# la partie ora Emmanuelle Pfaff
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# la totale
# source("geo/scripts/wetlands.R");emmanuelle_jour();
emmanuelle_jour <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  emmanuelle_lire();
}
#
# le fichier de l'ora produit par Emmanuelle Pfaff
# source("geo/scripts/wetlands.R");emmanuelle_lire();
emmanuelle_lire <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  library(sf)
  dsn <- sprintf('%s/EmmanuellePfaff/entite_ora.shp', varDir)
  nc <- st_read(dsn) %>%
    filter(id_fb != "0") %>%
#    filter(grepl("/35../", fb_nat_id)) %>%
#    filter(grepl("3529", fb_nat_id)) %>%
#    filter(id == 5727) %>%
    glimpse()
  dsn <- sprintf('%s/EmmanuellePfaff/entite_ora.geojson', varDir)
  st_write(st_transform(nc, 4326), dsn, delete_dsn=TRUE, driver='GeoJSON')
  return(invisible(nc))
}
#
# les différences avec faune-bretagne
# source("geo/scripts/wetlands.R");emmanuelle_diff_fb();
emmanuelle_diff_fb <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  library(sf)
  df1 <- emmanuelle_lire() %>%
    st_drop_geometry() %>%
    filter(grepl("/3929/", fb_nat_id)) %>%
    dplyr::select(fb_nat_id) %>%
    mutate(source = "ora") %>%
    glimpse()
  df2 <- biolo_lire("biolo1441") %>%
    filter(grepl("/35../", square_number)) %>%
    filter(grepl("/3529/.*PIERRE", square_number)) %>%
    arrange(square_number) %>%
    glimpse() %>%
    dplyr::select(square_number) %>%
    mutate(source = "fb") %>%
    glimpse()

  carp("les absents d'Emmanuelle")
  df3 <- df1 %>%
    filter(fb_nat_id %notin% df2$square_number) %>%
    arrange(fb_nat_id)
#  misc_print(df3)
  carp("les absents de fb")
  df4 <- df2 %>%
    filter(square_number %notin% df2$fb_nat_id)
#  misc_print(df4)
  df5 <- df1 %>%
    full_join(df2, by = c("fb_nat_id" = "square_number")) %>%
    filter(is.na(source.x) | is.na(source.y)) %>%
    arrange(fb_nat_id) %>%
    glimpse()
  misc_print(df5)
  return()
  carp("référence en double")
  df5 <- df1 %>%
    group_by(fb_nat_id) %>%
    summarize(nb = n()) %>%
    filter(nb > 1) %>%
    glimpse()
}
#
# les différences avec faune-bretagne, via l'api visionature
# source("geo/scripts/wetlands.R");emmanuelle_diff_apivn();
emmanuelle_diff_apivn <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  library(sf)
  df1 <- emmanuelle_lire() %>%
    st_drop_geometry() %>%
    dplyr::select(id_fb, fb_nat_id) %>%
    mutate(source = "ora") %>%
    glimpse()
#  df2 <- biolo_lire("apivn_places_wetlands") %>%
  df2 <- biolo_lire("places_all_wetlands") %>%
#    glimpse() %>%
    mutate(id = sprintf("%s", id)) %>%
    dplyr::select(id, name) %>%
    mutate(source = "fb") %>%
    glimpse()
  df5 <- df1 %>%
    full_join(df2, by = c("id_fb" = "id")) %>%
    filter(is.na(source.x) | is.na(source.y)) %>%
    arrange(fb_nat_id, id_fb) %>%
    glimpse()
  misc_print(df5)
  return()
  carp("référence en double")
  df5 <- df1 %>%
    group_by(fb_nat_id) %>%
    summarize(nb = n()) %>%
    filter(nb > 1) %>%
    glimpse()
}
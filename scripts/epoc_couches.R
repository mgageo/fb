# <!-- coding: utf-8 -->
#
# la partie couches : vecteur et raster
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
# les cartes pour l'ensemble des sites, la version Tex
# source("geo/scripts/epoc.R"); couches_jour()
couches_jour <- function(ogc = FALSE, cartes = FALSE, test = 2) {
  carp()
  library(tidyverse)
  library(utf8)
  library(janitor)
  cartesDir <- sprintf("%s/couches_cartes", texDir)
  dir.create(cartesDir, showWarnings = FALSE)
  nc <- couches_osm_lire()
  fonds.df <- couches_fonds()
  tex_dftpl2fic(nc, "couches_cartes")
  if (ogc == TRUE) {
    couches_ogc(nc, fonds.df, test = test)
  }
  if (cartes == TRUE) {
    couches_cartes(nc, fonds.df, test = test)
  }
  tex_pdflatex("sites.tex")
}
#
# les cartes pour l'ensemble des communes
# source("geo/scripts/epoc.R"); couches_communes_jour()
couches_communes_jour <- function(ogc = FALSE, cartes = FALSE, test = 2) {
  carp()
  library(tidyverse)
  library(utf8)
  library(janitor)
  cartesDir <- sprintf("%s/couches_communes", texDir)
  dir.create(cartesDir, showWarnings = FALSE)
  communes <- c("29155", "56189", "29119")
  nc <- ign_adminexpress_lire_sf('COMMUNE') %>%
    filter(INSEE_COM %in% communes) %>%
    mutate(site = INSEE_COM) %>%
    glimpse()
  fonds.df <- couches_communes_fonds()
  couches_ogc(nc, fonds.df, test = test)
}
#
# les couches ogc : récupération sur internet
# source("geo/scripts/epoc.R");couches_ogc(2)
couches_ogc <- function(nc, fonds.df, test=2) {
  carp()
  library(tidyverse)
  library(sf)
  ogcDir <<- sprintf("%s/ogc", varDir)
  dir.create(ogcDir, showWarnings = FALSE)
  for ( i in 1:nrow(nc) ) {
    site <- nc[[i, "site"]]
    carp("i: %s site: %s", i,  site)
    fonds_ogc_fonds(fonds.df, nc[i,], site, test)
#    break
  }
}
#
# production du pdf à partir des images
# setwd("d:/web");source("geo/scripts/epoc.R"); couches_cartes(1)
couches_cartes <- function(nc, fonds.df, test = 2) {
  carp()
  library(tidyverse)
  cartesDir <- sprintf("%s/couches_cartes", texDir)
  dir.create(cartesDir, showWarnings = FALSE)
  for (i in 1:nrow(nc)) {
    site <- nc[[i, "site"]]
    couches_cartes_fonds(fonds.df, nc[i, ], site, cartesDir, test = test)
  }
}
#
# les fonctions génériques "fonds"
# =============================================================================
#
# génération des différents pdf
couches_cartes_fonds <- function(df, nc, site, pdfDir, test, fct = "_carres") {
  carp("site: %s", site)
  for (i in 1:nrow(df)) {
    e <- df[i, "pdf"]
    fond <- df[i, "fond"]
#    test <- df[i, "test"]
    param <- df[i, "param"]
    marge <- df[i, "marge"]
    carp("fond: %s", fond)
    dsn <- sprintf("%s/%s_%s.pdf", pdfDir, site, fond)
    if (test != 1 & file.exists(dsn)) {
      next
    }
#    carp("test: %s dsn: %s", test, dsn)
    fonction <- sprintf("couches_cartes_%s%s", e, fct)
    if (! exists(fonction, mode = "function")) {
      carp("fonction inconnue: %s", fonction)
      stop("***")
    }
    img <- do.call(fonction, list(nc = nc, site = site, df = df[i, ]))
    dev.copy(pdf, dsn, width = par("din")[1], height = par("din")[2])
    carp("dsn: %s %sx%s", dsn, par("din")[1], height = par("din")[2])
#    stop("***")
    graphics.off()
  }
#  stop("***")
}
#
# le cas d'une image de type brick
couches_cartes_img_carres <- function(nc, site, df, affiche = TRUE) {
  library(raster)
  Carp("site: %s", site)
  glimpse(df)
  dsn <- sprintf(df[1, "param"], varDir, site, df[1, "fond"])
  Carp("dsn: %s", dsn)
  img <- brick(dsn)
  plotImg(img)
  plot(st_geometry(nc), lwd = 3, col = "transparent", add = TRUE)
#
# la grille à l'intérieur du carré
  sfc1 <- st_make_grid(nc, cellsize = 2000, square = TRUE)
  diviseur <- 10000
  pas <- 0
  nc1 <- st_as_sf(data.frame(no = c(1:length(sfc1)), geometry = sfc1)) %>%
    mutate(X = map_dbl(geometry, ~st_centroid(.x)[[1]]), Y = map_dbl(geometry, ~st_centroid(.x)[[2]])) %>%
    mutate(x = floor(X / diviseur), y  = floor( Y / diviseur)) %>%
    mutate(ncarre = sprintf("E%03dN%s", x, y)) %>%
    filter(ncarre == site) %>%
    arrange(-Y, X) %>%
    mutate(no = 1:nrow(.)) %>%
    glimpse()
  plot(st_geometry(nc1), add = TRUE)
# pour mettre le numéro en bas à adroite
  df1 <- nc1 %>%
    st_drop_geometry() %>%
    mutate(X = X + 900) %>%
    mutate(Y = Y - 900)
  nc2 <- st_as_sf(df1, coords = c("X", "Y"), crs = 2154, remove=FALSE)
  text(st_coordinates(st_centroid(nc2)), labels = nc1$no, cex = 1, col = 'black', adj = c(1, 0))
#
  text(st_coordinates(st_centroid(points.sf)), labels = points.sf$reserve, cex = 2, col = 'black')
#  geo_echelle()
  legend("topright", legend = site, cex = 2, text.col = "darkblue", box.lwd = 0, box.col = "transparent", bg = "transparent")
}
#
# le cas d'une image de type brick
couches_cartes_img_points <- function(nc, site, df, affiche = TRUE) {
  library(raster)
  Carp("site: %s", site)
  dsn <- sprintf(df[1, "param"], varDir, site, df[1, "fond"])
  Carp("dsn: %s", dsn)
  img <- brick(dsn)
  plotImg(img)
  text(st_coordinates(st_centroid(nc)), labels = nc$reserve, cex = 2, col = 'black')
  nc1 <- st_buffer(nc, 100)
  plot(st_geometry(nc1), lwd = 5, col = "transparent", border = "darkred", add = TRUE)
  geo_echelle()
  geo_legende(copyright)
  legend("topright", legend = site, cex = 2, text.col = "darkblue", box.lwd = 0, box.col = "transparent", bg = "transparent")
#  stop("***")
}
#
# la configuration des cartes
couches_carres_fonds <- function(test=2) {
  carp()
  df <- read.table(text="fonction|pdf|fond|param|marge|size_max
#crop|img|gmses13|%s/ogc/%s_gmses13.tif|5000|1024
#read|img|gb_photo|%s/ogc/%s_gb_photo.tif|300|1024
#read|img|gb_ancien|%s/ogc/%s_gb_ancien.tif|300|1024
#read|img|carte|%s/ogc/%s_%s.tif|300|2048
read|img|ign_photo|%s/ogc/%s_%s.tif|300|2048
read|img|ign_carte|%s/ogc/%s_%s.tif|300|2048
#read|img|ign_carte|%s/ogc/%s_ign_carte.tif|300|2048
#crop|img|cesbio2019|%s/ogc/%s_cesbio2019.tif|300|1024
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors = FALSE, quote = "") %>%
    filter(!grepl('^#', fonction)) %>%
    glimpse()
  return(invisible(df))
}
#
# la configuration pour les communes
couches_communes_fonds <- function(test=2) {
  carp()
  df <- read.table(text="fonction|pdf|fond|param|marge|size_max
read|img|ign_photo|%s/ogc/%s_%s.tif|2000|2048
read|img|ign_carte|%s/ogc/%s_%s.tif|2000|2048
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors = FALSE, quote = "") %>%
    filter(!grepl('^#', fonction)) %>%
    glimpse()
  return(invisible(df))
}
#
# la configuration des cartes
couches_lieudits_fonds <- function(test=2) {
  carp()
  df <- read.table(text="fonction|pdf|fond|param|marge|size_max
read|img|gb_photo|%s/ogc/%s_gb_photo.tif|300|1024
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors = FALSE, quote = "") %>%
    filter(!grepl('^#', fonction)) %>%
    glimpse()
  return(invisible(df))
}
#
# la configuration des cartes
couches_points_fonds <- function(test=2) {
  carp()
  df <- read.table(text="fonction|pdf|fond|param|marge|size_max
#read|img|carte|%s/ogc/%s_carte.tif|300|1024
#read|img|gb_photo|%s/ogc/%s_gb_photo.tif|300|1024
#read|img|ign_plan|%s/ogc/%s_ign_plan.tif|300|1024
#read|img|ign_carte|%s/ogc/%s_ign_carte.tif|300|512
#read|img|carte|%s/ogc/%s_%s.tif|300|1024
read|img|ign_photo|%s/ogc/%s_%s.tif|300|1024
read|img|ign_carte|%s/ogc/%s_%s.tif|300|1024
#crop|img|cesbio2019|%s/ogc/%s_cesbio2019.tif|300|1024
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors = FALSE, quote = "") %>%
    filter(!grepl('^#', fonction)) %>%
    glimpse()
  return(invisible(df))
}
couches_osm_lire <- function(test=2) {
  carp()
  library(tidyverse)
  library(sf)
  dsn <- sprintf("%s/%s.geojson", varDir, "osm")
  carp("dsn: %s", dsn)
  nc <- st_read(dsn) %>%
    st_transform(2154) %>%
    mutate(site = sprintf("%02d", no)) %>%
#    filter(site == "23") %>%
    mutate(commune = name) %>%
    glimpse()
  return(invisible(nc))
}
# <!-- coding: utf-8 -->
#
# quelques fonctions pour le protocole stoc
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
mga  <- function() {
  source("geo/scripts/stoc.R");
}
#
# https://gis.stackexchange.com/questions/63577/joining-polygons-in-r
# http://www.nickeubank.com/wp-content/uploads/2015/10/RGIS2_MergingSpatialData_part2_GeometricManipulations.html
#
# les commandes permettant le lancement
Drive <- substr( getwd(),1,2)
baseDir <- sprintf("%s/web", Drive)
bioloDir <<- sprintf("%s/bvi35/CouchesFB", Drive)
biolo63Dir <<- sprintf("%s/bvi35/CouchesFB/63", Drive)
cbnbrestDir <- sprintf("%s/bvi35/CouchesCBNBrest", Drive);
cfgDir <<- sprintf("%s/web/geo/STOC", Drive)
copernicusDir <- sprintf("%s/bvi35/CouchesCopernicus", Drive)
ignDir <<- sprintf("%s/bvi35/CouchesIGN", Drive)
osoDir <<- sprintf("%s/bvi35/CouchesOSO", Drive);
texDir <<- sprintf("%s/web/geo/STOC", Drive)
varDir <- sprintf("%s/bvi35/CouchesStoc", Drive);
webDir <- sprintf("%s/web.heb/bv/stoceps", Drive);
dir.create(cfgDir, showWarnings = FALSE, recursive = TRUE)
dir.create(varDir, showWarnings = FALSE, recursive = TRUE)
dir.create(biolo63Dir, showWarnings = FALSE, recursive = TRUE)
setwd(baseDir)
#
# l'instance apivn
user1 <- "apivn45"
#
# l'année en cours
annee_cours <- "2023"
#
# mes modules
source("geo/scripts/mga.R");
source("geo/scripts/misc.R");
source("geo/scripts/misc_apivn.R");
source("geo/scripts/misc_biolo.R");
source("geo/scripts/misc_cartes.R");
source("geo/scripts/misc_clc.R");
source("geo/scripts/misc_couches.R");
source("geo/scripts/misc_db.R");
source("geo/scripts/misc_faune.R");
source("geo/scripts/misc_fonds.R");
source("geo/scripts/misc_gdal.R");
source("geo/scripts/misc_ggplot.R");
source("geo/scripts/misc_geo.R");
source("geo/scripts/misc_mnhn.R");
source("geo/scripts/misc_ocs.R");
source("geo/scripts/misc_psql.R");
source("geo/scripts/misc_rtrim.R");
source("geo/scripts/misc_stat.R");
source("geo/scripts/misc_tex.R");
source("geo/scripts/stoc_analyse.R");
source("geo/scripts/stoc_apivn.R");
source("geo/scripts/stoc_benjamin.R");
source("geo/scripts/stoc_carres.R");
source("geo/scripts/stoc_cartes.R");
source("geo/scripts/stoc_carto.R");
source("geo/scripts/stoc_codes.R");
source("geo/scripts/stoc_compare.R");
source("geo/scripts/stoc_couches.R");
source("geo/scripts/stoc_drive.R");
source("geo/scripts/stoc_faune.R");
source("geo/scripts/stoc_mnhn.R");
source("geo/scripts/stoc_passage.R");
source("geo/scripts/stoc_ocs.R");
source("geo/scripts/stoc_regression.R");
source("geo/scripts/stoc_rtrim.R");
source("geo/scripts/stoc_stat.R");
source("geo/scripts/stoc_stoc.R"); # les scripts du mnhn
if ( interactive() ) {
  print(sprintf("stoc.R interactif"))
# un peu de nettoyage
  graphics.off()
} else {
  print(sprintf("stoc.R console"))
}
#
# setwd("d:/web"); source("geo/scripts/stoc.R");jour(force = TRUE)
jour <- function(force = TRUE) {
  carp()
  misc_log("jour() début")
  faune_jour(force = force)
  misc_log("jour() fin")
}
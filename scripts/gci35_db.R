# <!-- coding: utf-8 -->
#
# les traitements sur les données en base mysql
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# source("geo/scripts/gci35.R");db_jour()
db_jour <- function() {
  library(DBI)
  library(RMySQL)
  carp()
}
#
# source("geo/scripts/gci35.R");db_sql_connect()
db_sql_connect <- function(user = "bv") {
  library(DBI)
  library(RMySQL)
  library(lubridate)
  carp()
  mga_docker_pg(user)
  db_MySQL_killConnections()
  db_con <<- dbConnect(MySQL(),
    dbname = db_name,
    host = db_host,
#    port = db_port,
    user = db_user,
    password = db_password
  )
  print(dbListTables(db_con))
}
#
# source("geo/scripts/gci35.R");db_table()
db_table <- function() {
  library(lubridate)
  carp()
  db_sql_connect()
  df <- dbReadTable(db_con, "bvgravelot") %>%
    mutate(combinaison = toupper(gracombinaison)) %>%
    mutate(date = ymd(gradate)) %>%
    arrange(date) %>%
    mutate(lat = as.numeric(gralatitude)) %>%
    mutate(lon = as.numeric(gralongitude)) %>%
    filter(graaction != "") %>%
    glimpse()
  return(invisible(df))
}
#
# source("geo/scripts/gci35.R");db_combinaison()
db_combinaison <- function() {
  library(lubridate)
  carp()
  df <- db_table() %>%
#    filter(grepl("Prigent", graobservateurs, ignore.case = TRUE)) %>%
    filter(grepl("^S", grabague, ignore.case = TRUE) & graaction == "B" & grepl("FBB", gracombinaison, ignore.case = TRUE)) %>%
#    filter(grepl("^MTRO/FBB", gracombinaison, ignore.case = TRUE)) %>%
    glimpse()
  df1 <- df %>%
    dplyr::select(graid, gradate, grasecteur, grasecteur2, gralieu, graobservateurs, graremarques)
#  misc_print(df1)
  nc1 <- st_as_sf(df, coords = c("lon", "lat"), crs = 4326, remove = TRUE)
  plot(st_geometry(nc1))
}
#
# source("geo/scripts/gci35.R");db_controle()
db_controle <- function() {
  carp()
  db_sql_connect()
  df <- db_table() %>%
    group_by(combinaison, graaction) %>%
    summarize(nb = n()) %>%
    glimpse() %>%
    pivot_wider(names_from = graaction, values_from = nb, values_fill = list(nb = 0)) %>%
    glimpse()

}
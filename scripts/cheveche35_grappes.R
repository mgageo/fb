# <!-- coding: utf-8 -->
#
# traitements pour les chevêches du 35
#
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
#
# les grappes de réponse
#
#
# source("geo/scripts/cheveche35.R");grappes_get(force = TRUE) %>% glimpse()
grappes_get <- function(annee = "2024", force = TRUE) {
  library(tidyverse)
  library(sf)
  library(igraph)

  nc1 <- misc_lire(sprintf("prospection_annee_%s", annee)) %>%
    filter(annee == !!annee) %>%
    st_transform(2154) %>%
    mutate(couleur = gsub("Flag, ", "", sym))
  reponses.sf <- nc1 %>%
    filter(grepl("Orange", sym))
#
# les territoires potentiels
# regroupement des réponses
  adj <- st_distance(reponses.sf)
  adj <- matrix(as.numeric(as.numeric(adj)) < 300, nrow = nrow(adj))
  g <- graph_from_adjacency_matrix(adj)
  carp("possible: %s", length(unique(components(g)$membership)))
  reponses.sf$grappe <- factor(components(g)$membership)
  nc2 <- reponses.sf %>%
    group_by(grappe) %>%
    summarize(nb = n()) %>%
    st_centroid()
  return(invisible(nc2))
}
grappes_geocode <- function(annee = "2024", force = TRUE) {
  library(tidyverse)
  library(sf)
  library(igraph)
  nc2 <- grappes_get() %>%
    st_transform(4326)
  df2 <- nc2 %>%
    mutate(lon = st_coordinates(.)[,'X']) %>%
    mutate(lat = st_coordinates(.)[,'Y']) %>%
    mutate(grappe = as.integer(grappe)) %>%
    st_drop_geometry()
  df21 <- df2 %>%
    filter(grappe == 5)
#  misc_print(df21);stop("*****")
  df3 <- geocode_reverse_openls(df2, force = force) %>%
    dplyr::select(grappe, nb, lieudit) %>%
    glimpse()
  tex_df2kable(df3, dossier = annee)
}

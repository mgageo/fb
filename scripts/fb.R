# <!-- coding: utf-8 -->
#
# quelques fonctions pour les données faune-bretagne
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# http://www.math.unicaen.fr/~chesneau/RCarte_Commandes-R.pdf
mga  <- function() {
  source("geo/scripts/fb.R");
}
#
# les variables globales
Drive <- substr( getwd(),1,2)
baseDir <- sprintf("%s/web", Drive)
dockerDir <- sprintf("%s/docker/postgres-data/", Drive)
varDir <- sprintf("%s/bvi35/CouchesFB", Drive)
fauneDir <<- sprintf("%s/bvi35/CouchesFB", Drive);
fbDir <- sprintf("%s/bvi35/CouchesFauneBretagne", Drive)
fbapiDir <- sprintf("%s/bvi35/CouchesFauneBretagne/api", Drive)
tplDir <- sprintf("%s/geo/scripts", baseDir)
cfgDir <- sprintf("%s/geo/%s", baseDir, "FB")
texDir <- sprintf("%s/geo/%s", baseDir, "FB")
dir.create(fbapiDir, showWarnings = FALSE)
credit <- "source : faune-bretagne.org @Marc Gauthier - 2023 - CC-BY-NC-SA"
#
# l'instance apivn
user1 <- "apivn45"
#
# les bibliothèques
setwd(baseDir)
source("geo/scripts/mga.R")
source("geo/scripts/misc.R")
source("geo/scripts/misc_arrow.R")
source("geo/scripts/misc_biolo.R")
source("geo/scripts/misc_couches.R")
source("geo/scripts/misc_db.R")
source("geo/scripts/misc_faune.R")
source("geo/scripts/misc_fb.R")
source("geo/scripts/misc_ftp.R")
source("geo/scripts/misc_gdal.R")
source("geo/scripts/misc_geo.R")
source("geo/scripts/misc_geocode.R")
source("geo/scripts/misc_grille.R")
source("geo/scripts/misc_maillage.R")
source("geo/scripts/misc_oauth.R")
source("geo/scripts/misc_psql.R")
source("geo/scripts/misc_stat.R")
source("geo/scripts/misc_tex.R")
source("geo/scripts/fb_api.R")
source("geo/scripts/fb_bvo.R")
source("geo/scripts/fb_choro.R")
source("geo/scripts/fb_cluster.R")
source("geo/scripts/fb_covid.R")
source("geo/scripts/fb_donnees.R"); # export des données de faune-bretagne pour les différents groupes
source("geo/scripts/fb_drive.R")
source("geo/scripts/fb_faune.R")
source("geo/scripts/fb_fbzh.R"); # pour le site miroir
source("geo/scripts/fb_fonds.R")
source("geo/scripts/fb_oiseaux.R")
source("geo/scripts/fb_pletort.R")
source("geo/scripts/fb_pression.R")
source("geo/scripts/fb_psql.R")
source("geo/scripts/fb_sql.R")
source("geo/scripts/fb_wetlands.R")

#
# les commandes permettant le lancement
DEBUG <- FALSE
if ( interactive() ) {
  Log(sprintf("fb.R interactive"))
# un peu de nettoyage
  graphics.off()
#  mares_jour()
} else {
  Log(sprintf("fb.R console"))
}

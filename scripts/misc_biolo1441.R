# <!-- coding: utf-8 -->
#
# quelques fonctions pour VisioNature
# auteur: Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
## les interrogations des lieudits du protocole wetlands
# soit à partir de la liste des lieudits (biolo60008)
# soit en incrémentation
# rm(list=ls()); source("geo/scripts/visionature.R");biolo1441_jour(force = TRUE)
biolo1441_jour <- function(force = FALSE) {
  carp()
  biolo1441_get(force = force)
  biolo1441_parse_files()
  biolo1441_extract()
}
# interrogation de l'ensemble des lieudits
# rm(list=ls()); source("geo/scripts/visionature.R");biolo1441_get(force = FALSE)
biolo1441_get <- function(force = FALSE) {
  carp()
  biolo_handle(force = TRUE)
  biolo_fb()
  df <- biolo_lire("biolo60008") %>%
    extract(editer, c("id_stoc"), "id_stoc=(\\d+)", remove = FALSE) %>%
    glimpse()
  carp("biolo60008 nrow: %s", nrow(df))
  for( i in 1:nrow(df)) {
    url <- sprintf("%s/%s", biolo_url, df[i, "editer"])
    dsn <- sprintf("%s/id_stoc_%s.html", biolo1441Dir, df[i, "id_stoc"])
    if( ! file.exists(dsn) || force == TRUE ) {
      biolo_get(url, dsn)
   }
  }
}
# rm(list=ls()); source("geo/scripts/visionature.R");biolo1441_get_v2(force = FALSE)
biolo1441_get_v2 <- function(force = FALSE) {
  carp()
  biolo_handle(force = TRUE)
  biolo_fb()
  for( i in 1:1200) {
    url <- sprintf("%s/index.php?m_id=1441&id_stoc=%s", biolo_url, i)
    dsn <- sprintf("%s/id_stoc_%s.html", biolo1441Dir, i)
    if( ! file.exists(dsn) || force == TRUE ) {
      biolo_get(url, dsn)
   }
  }
}
#
# pour récupérer l'ensemble des lieudits
# rm(list=ls()); source("geo/scripts/visionature.R");df <- biolo1441_parse_files()
biolo1441_parse_files <- function() {
  carp()
  library(tidyverse)
  biolo_handle(force = TRUE)
  files <- list.files(biolo1441Dir, pattern = 'id_stoc_.*.html$', full.names = TRUE, ignore.case = TRUE)
  carp("nb files: %s", length(files))
  df <- data.frame()
  for (dsn in files ) {
    df2 <- biolo1441_parse_file(dsn)
    if (! is(df2,"data.frame")) {
      carp("*** data.frame dsn: %s", dsn)
      next;
    }
    df <- bind_rows(df, df2)
  }
  biolo_ecrire(df, "biolo1441")
  carp("dsn: %s nrow: %s", dsn, nrow(df))
  return(invisible(df))
}
# rm(list=ls()); source("geo/scripts/visionature.R");df <- biolo1441_parse_file('D:/bvi35/CouchesFB/biolo1441/id_stoc_728.html') %>% glimpse()
biolo1441_parse_file <- function(dsn) {
  library(tidyverse)
  library(rvest)
  library(tidyr)
  carp("dsn: %s", dsn)
  pg <- read_html(dsn)
  stoc_form <- html_node(pg, "#stoc_form")
  if (is.na(stoc_form) ) {
    carp("**** #stoc_form %s", dsn)
    return(FALSE);
    stop("*****")
    file.remove(dsn)
    return(FALSE);
  }
# les infos sont dans les 3 premières div du formulaire
  stoc_divs <- html_children(stoc_form)[1:3]
  input_els <- html_elements(stoc_divs, "input")
  if ( length(input_els) < 2 ) {
    carp("**** input_els %s", dsn)
    stop("****")
    file.remove(dsn)
    return(FALSE);
  }
  df <- data.frame()
  for (i in 1:length(input_els)) {
    input_el <- input_els[i]
    df[i, "name"] <- html_attr(input_el, "name") %>%
      trimws()
    df[i, "value"] <- html_attr(input_el, "value") %>%
      trimws()
  }
  df <- df %>%
    pivot_wider(names_from = c("name"), values_from = c("value"))
  return(invisible(df))
}
# source("geo/scripts/visionature.R");biolo_fb(); df <- biolo1441_extract() %>% glimpse()
biolo1441_extract <- function() {
  carp()
  library(tidyverse)
  dsn <- sprintf("%s/biolo1441_files.Rds", biolo1441Dir)
  df <- readRDS(file = dsn)
  df <- df %>%
    glimpse() %>%
    tidyr::extract(
      row,
      c("nom_reference", "nom_personnalise", "lieudit_reference", "commune", "altitude"),
      "([^\\t]*)\\t([^\\t]*)\\t([^\\t]*)\\t([^\\t]*)\\t([^\\t]*)\\t",
      remove = FALSE
    ) %>%
    tidyr::extract(
      nom_reference,
      c("site_fonctionnel", "code_fonctionnel", "site_elementaire"),
      "([^/]*)/([^/]*)/([^/]*)",
      remove = FALSE
    )
  biolo_ecrire(df, "biolo1441")
  carp("dsn: %s nrow: %s", dsn, nrow(df))
  df1 <- df %>%
    filter(is.na(code_fonctionnel)) %>%
    dplyr::select(nom_reference)
  misc_print(df1)
  return(invisible(df))
}
# <!-- coding: utf-8 -->
#
# la partie données biolovision faune-bretagne
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# détail de l'observation
#
mediasDir <- sprintf("%s/medias", varDir)
dir.create(mediasDir, showWarnings = FALSE, recursive = TRUE)
#
#  source("geo/scripts/martidelle.R");biolo54_export_detail()
biolo54_export_detail <- function(id = "4376166", force = FALSE) {
  fic <- sprintf("%s/%s.Rds", mediasDir, id)
  if ( force == FALSE & file.exists(fic)) {
    page <- readRDS(fic)
  } else {
    session <- biolo_session(force = FALSE)
    url <- sprintf("%s/index.php?m_id=54&id=%s", biolo_url, id)
    carp("url: %s", url)
    page <- session_jump_to(session, url)
    saveRDS(page, fic)
  }
  imgs <- page %>%
    html_node("#media_display_container") %>%
    html_nodes("img") %>%
    html_attr("src")
  df <- data.frame(img = imgs) %>%
    filter(grepl("cdnmedia", img)) %>%
    mutate(url = gsub("/xsmall", "", img)) %>%
    mutate(dsn = gsub("^https\\://[^/]+/", "", url)) %>%
    mutate(dsn = gsub("/", "_", dsn))
#  carp("id: %s nrow: %s", id, nrow(df))
  for (i in 1:nrow(df)) {
    url <- df[i, "url"]
    fic <- sprintf("%s/%s", mediasDir, df[i, "dsn"])
    if (file.exists(fic)) {
      next
    }
# https://stackoverflow.com/questions/36202414/r-download-image-using-rvest
    img <- session_jump_to(session, url)
    writeBin(img$response$content, fic)
#    break
  }
  return(invisible(df))
}

# <!-- coding: utf-8 -->
#
# la partie automatisation navigation internet
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
#
# ==================================================================================================
#
#
# http://thatdatatho.com/2019/01/22/tutorial-web-scraping-rselenium/
# https://github.com/ropensci/RSelenium/issues/203
# https://cran.r-project.org/web/packages/RSelenium/vignettes/basics.html
# https://towardsdatascience.com/mapping-physical-activity-with-r-selenium-and-leaflet-ac3002886728
# https://github.com/ropensci/RSelenium/blob/master/R/rsDriver.R
#
# l'initialisation
selenium_init <- function() {
  library(RSelenium)
  binman::list_versions("geckodriver")
  rD <<- rsDriver(browser = "firefox")
#  rD <<- rsDriver(browser = "chrome", chromever = "98.0.4758.102")
  remDr <<- rD[["client"]]
  return()
}
#
selenium_init2 <- function() {
  library(RSelenium)
  binman::list_versions("chromedriver")
  rD <<- rsDriver(browser = "chrome", chromever="79.0.3945.36")
  remDr <<- remoteDriver(remoteServerAddr = "127.0.0.1", browserName= "chrome", port=4567L)
# Sets how long the driver should wait when searching for elements.
#  remDr$setTimeout(type = "implicit", milliseconds = 100000)
# Sets how long it should wait for the page to load.
#  remDr$setTimeout(type = "page load", milliseconds = 100000)
  remDr$open()
  return()
}
#
# pour tuer le process selenium en écoute
# source("geo/scripts/wetlands.R");selenium_kill();
selenium_kill <- function() {
  liste <- system('netstat -aon -p tcp', intern=T)
  port <- grep('4567', liste, perl=TRUE, value=TRUE)
  if( length(port) == 0 ) {
    print("selenium absent")
    return()
  }
  pids <- stringr::str_extract(string = port, pattern = '\\d+$')
  for (pid in pids) {
    kill <- sprintf('taskkill /F /PID %s', pid)
    system(kill, intern=T)
  }
}
selenium_close <- function() {
  remDr$close()
  rD$server$stop()
  rm(remDr, rD)
}
#
# les cartes d'un compte umap
# source("geo/scripts/wetlands.R");selenium_umap();
selenium_umap <- function(dsn="http://umap.openstreetmap.fr/fr/user/BretagneVivanteRennes/") {
  library(tidyverse)
  library(stringr)
  carp('page')
  remDr$navigate(dsn)
  Sys.sleep(3)
  for (i in 1:10) {
    carp('suite: %d', i)
#
# contournement de NoSuchElement
    html <- remDr$getPageSource()[[1]]
    html <- unlist(strsplit(html, '\n'))
    plus <- grep('class="button more_button.*?', html, perl=TRUE, value=TRUE)
    carp('plus: %s', plus)
    if( length(plus) < 1 ) {
      break
    }
    element <- remDr$findElement(using = 'class', value = 'more_button')
    if( length(element) == 0 ) {
      break
    }
    if (element$isElementDisplayed()[[1]]) {
      print("suite est visible")
    } else {
      print("suite n'est pas visible")
      break;
    }
    element$clickElement()
    Sys.sleep(3)
  }
  links <- remDr$findElements(using = "xpath", value = "//*[@class='legend']/a")
# récupération de l'url et du texte, c'est hard !
  links.list <- sapply(links, function(x){
    return(list(
      'href'=x$getElementAttribute('href')[[1]],
      'text'=x$getElementText()[[1]]
    ))
  })
  df <- data.frame(t(links.list)) %>%
    glimpse()
#  return(invisible(df))
  for(i in 1:nrow(df)) {
#    remDr$navigate(df[i, 'href'])
#    Sys.sleep(3)
    selenium_umap_map(unlist(df[i, 'href']))
  }
}
#
# chargement d'une carte pour trouver son fichier de configuration
# source("geo/scripts/wetlands.R");selenium_umap_map()
selenium_umap_map <- function(dsn="http://umap.openstreetmap.fr/fr/map/etang-de-daniel_399140") {
  library(tidyverse)
  library(curl)
  carp('dsn: %s', dsn)
  req <- curl_fetch_memory(dsn)
  texte <- rawToChar(req$content)
  txt <- gsub("\\s", " ", texte, perl=TRUE)
  json <- sub(".*(datalayers.*?]).*", "\\1", txt, perl=TRUE)
  id <- sub(".* (\\d+),.*", "\\1", json, perl=TRUE)
  datalayer <- sprintf('http://umap.openstreetmap.fr/fr/datalayer/%s/', id)
  site <- sub('.*/(.*?)_\\d+$', '\\1', dsn, perl=TRUE)
  dest <- sprintf('%s/cartes/%s.umap', varDir, site)
  curl_download(datalayer, dest)
#  jsonlite::prettify(readLines(dest))
}
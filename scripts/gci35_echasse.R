# <!-- coding: utf-8 -->
#
# quelques fonctions pour le suivi gravelot à collier interrompu
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
# les données gci35 sur BMSM
# source("geo/scripts/gci35.R");echasse_jour()
echasse_jour <- function() {
  library(tidyverse)
  carp()
  espece <<- "echasse35"
  donneesFic <<- "echasse35.xlsx"
  donneesDsn <<-  sprintf("%s/%s" , varDir, donneesFic)
#  echasse_faune_export()
#  echasse_faune_export_lire()
#  echasse_carte()
  echasse_faune_liste()
}
# lecture de l'export de faune-bretagne
echasse_faune_export_lire <- function() {
  library(tidyverse)
  carp()
  nc <- faune_export_lire_sf(donneesDsn, force=TRUE)
  date_from <- as.Date("10/06/2020", "%d/%m/%Y")
  nc1 <- nc %>%
    filter(grepl('2020', DATE_YEAR)) %>%
    filter(grepl('^35', INSEE)) %>%
#    filter(d == date_from) %>%
#    filter(NAME == 'Gauthier') %>%
    arrange(TIMING) %>%
    glimpse()
  return(invisible(nc1))
  nc2 <- nc1 %>%
    dplyr::select(TOTAL_COUNT, COMMENT, TIMING) %>%
    glimpse()
  dsn <- sprintf('%s/%s.geojson', varDir, espece)
  st_write(nc2, dsn, delete_dsn = TRUE)
  carp('dsn: %s', dsn)
  df1 <- nc1 %>%
    st_drop_geometry() %>%
    dplyr::select(ID_SIGHTING, TIMING, TOTAL_COUNT, COMMENT) %>%
    print(n=30, na.print = "")
  export_df2xlsx(df1)
}
# l'export de faune-bretagne
echasse_faune_export <- function() {
  library(tidyverse)
  carp()
  biolo_export_espece(espece=254, dsn=donneesDsn, force=TRUE)
  biolo_export_get(donneesDsn, force=TRUE)
}
# lecture de l'export de faune-bretagne
echasse_faune_lire <- function() {
  library(tidyverse)
  library(sf)
  dsn <- sprintf('%s/%s.geojson', varDir, espece)
  nc <- st_read(dsn, stringsAsFactors=FALSE)
  carp('dsn: %s', dsn)
  return(invisible(nc))
}
# lecture de l'export de faune-bretagne
# source("geo/scripts/gci35.R");echasse_carte()
echasse_carte <- function() {
  library(tidyverse)
  library(sf)
  nc <- echasse_faune_lire() %>%
    st_transform(2154) %>%
    mutate(couleur=ifelse(TOTAL_COUNT > 0, 'green', 'black')) %>%
    mutate(id = 1:nrow(.)) %>%
    glimpse()
  couche <- "ign_gm"
  couche <- "gb_scan"
#  couche <- "ign_photo_hr"
  fonds_ogc_ecrire(nc, espece, couches = c(couche))
#  stop('***')
  imgs <- fonds_ogc_lire(espece, couches = c(couche))
  plotImg(imgs[[couche]])
  plot(st_geometry(nc), pch=19, cex=2, col=nc$couleur, add=TRUE)
  text(st_coordinates(st_centroid(nc)), labels = nc$TOTAL_COUNT, cex=0.8, col='black')
  scalebar(500, type='bar', divs=2)
#  north("topleft", cex=2)
#  echasse_ogc_ecrire(nc)
  dev2pdf()
}
#
# liste des observations par commune
echasse_faune_liste <- function() {
  carp("liste des observations")
  library(tidyverse)
  library(sf)
  df <- echasse_faune_export_lire() %>%
    st_drop_geometry() %>%
    glimpse() %>%
    mutate(observateur = sprintf("%s %s", SURNAME, NAME)) %>%
    mutate(commune = sprintf("%s %s", INSEE, MUNICIPALITY)) %>%
    glimpse()
 carp("nrow: %d", nrow(df))
#
# les communes
  df1 <- df %>%
    group_by(commune) %>%
    summarize(nb = n())
  texFic <- sprintf("%s/echasse_faune_liste.tex", texDir)
  TEX <- file(texFic, encoding="UTF-8")
  tex <- "% <!-- coding: utf-8 -->"
  for ( i in 1:nrow(df1) ) {
    n <- df1[[i, "commune"]]
    df2 <- df[df$commune == n,]
    tex <- append(tex, sprintf("\\subsection*{%s}", n))
#    print(head(df2));stop("***")
    for ( j in 1:nrow(df2) ) {
      D <- df2[j, "DATE"]
      d <- as.Date(as.numeric(as.character(D)), origin="1899-12-30")
      jour <- strftime(d, format="%a %d/%m/%Y")
      obs <- df2[j, "observateur"]
#      tex <- append(tex, sprintf("\\subsection*{%s %s}", jour, obs))
      tex <- append(tex, sprintf("\\subsubsection*{\\href{http://www.faune-bretagne.org/index.php?m_id=54&id=%s}{fb }%s(%s)}", df2[j, "ID_SIGHTING"], df2[j, "NAME_SPECIES"], df2[j, "TOTAL_COUNT"]))
      tex <- append(tex, sprintf("%s;%s;%s\\\\", strftime(d, format="%d/%m/%Y"), obs, df2[j, "PLACE"]))
      mortalite <- df2[j, "HAS_DEATH_INFO"]
      commune <- df2[j, "MUNICIPALITY"]
      type <- df2[j, "type"]
      lieu <- df2[j, "NUMERO"]
#      tex <- append(tex, sprintf("%s:%s(%s m)\\\\", type, lieu, distance))
#        tex <- append(tex,"\\begin{lstlisting}")
      tex <- append(tex, sprintf("%s", df2[j, "COMMENT"]))
#        tex <- append(tex,"\\end{lstlisting}")
    }
  }
  write(tex, file = TEX, append = FALSE)
  carp("texFic: %s", texFic)
  tex_pdflatex("echasse35.tex")
}
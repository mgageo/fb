# <!-- coding: utf-8 -->
#
# quelques fonctions pour extraire de oiseaux.net
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# source("geo/scripts/gob2004.R");oiseaux_get()
oiseaux_get <- function(force = FALSE) {
  library(tidyverse)
  library(rvest)
  library(httr)
  url <- "https://www.oiseaux.net/oiseaux/famille.france.html"
  dsn <- sprintf("%s/famille.france.html", varDir)
  if (! file.exists(dsn) | force == TRUE) {
    res <- httr::GET(url = url, verbose(), httr::write_disk(dsn, overwrite = TRUE))
    stop_for_status(res)
  }
  content <- read_html(dsn, encoding = "UTF-8")
  tables.list <- content %>% html_table(fill = TRUE) %>%
    glimpse()
  oiseaux.df <- tables.list[[1]]
  dsn <- sprintf("%s/famille.france.xlsx", varDir)
  rio::export(oiseaux.df, dsn)
  carp("dsn: %s", dsn)
}
# source("geo/scripts/odf.R");oiseaux_lire()
oiseaux_lire <- function(force = FALSE) {
  library(tidyverse)
  dsn <- sprintf("%s/famille.france_v2.xlsx", couchesDir)
  df <- rio::import(dsn)
  carp("dsn: %s", dsn)
  return(invisible(df))
}
#
# deux noms dans la même cellule
# source("geo/scripts/odf.R");oiseaux_deux()
oiseaux_deux <- function(force = FALSE) {
  library(tidyverse)
  carp("avant")
  df <- oiseaux_lire()
  df %>%
    glimpse() %>%
    filter(grepl("\\r\\n", Français)) %>%
    glimpse()
  df1 <- data.frame()
  for ( i in 1:nrow(df) ) {
    if (grepl("\\r\\n", df[i, "Français"]) == TRUE) {
      especes <- str_split(df[i, "Français"], "\\r\\n")
      df2 <- data.frame()
      df2 <- bind_rows(df2, df[i, ])
      df2 <- bind_rows(df2, df[i, ])
      df2[1, "Français"] <- especes[[1]][1]
      df2[2, "Français"] <- especes[[1]][2]
      df1 <- bind_rows(df1, df2)
    } else {
      df1 <- bind_rows(df1, df[i, ])
    }
#    stop("****")
  }
  carp("après")
  df1 %>%
    glimpse() %>%
    filter(grepl("\\r\\n", Français)) %>%
    glimpse()
  dsn <- sprintf("%s/famille.france_v2.xlsx", varDir)
  rio::export(df1, dsn)
  carp("doublons")
  df3 <- df1 %>%
    group_by(Français) %>%
    summarize(nb = n()) %>%
    filter(nb > 1) %>%
    glimpse()
}
# source("geo/scripts/odf.R");oiseaux_api_diff()
oiseaux_api_diff <- function(force = FALSE) {
  library(tidyverse)
  dsn <- sprintf("%s/nicheurs_bzh_area.geojson", webDir)
  api.df <- api_mailles_lire(fic = "taxa") %>%
    glimpse() %>%
    filter(breeding.new_count > 0) %>%
    rename(espece = common_name_fr) %>%
    group_by(espece) %>%
    summarize(nb = n()) %>%
    glimpse()
  oiseaux.df <- oiseaux_lire() %>%
    rename(espece = Français) %>%
    mutate(i = 1:nrow(.)) %>%
    filter(! grepl(":", espece)) %>%
    glimpse()
  carp("les oiseaux absents de oiseaux.net")
  df1 <- api.df %>%
    filter(espece %notin% oiseaux.df$espece) %>%
    glimpse()
  if ( nrow(df1) > 0) {
    misc_print(df1)
  }
}
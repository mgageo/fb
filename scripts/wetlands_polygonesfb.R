# <!-- coding: utf-8 -->
#
# quelques fonctions pour les couches Wetlands
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# les polygones dans faune-bretagne
#
# rm(list=ls()); source("geo/scripts/wetlands.R");polygonesfb_get()
polygonesfb_get <- function() {
  carp()
  url <- 'https://www.faune-bretagne.org/index.php?m_id=63&backlink=skip&content=bbox&garden=1&query=-1.995349,47.965577,-1.875876,48.03323&id_place=-1'
  dsn <- sprintf("%s/polygonesfb.xml", cfgDir);
  biolo63_get(url, dsn)
}
#
# rm(list=ls()); source("geo/scripts/wetlands.R");polygonesfb_shp_lire()
polygonesfb_shp_lire <- function() {
  carp()
  library(sf)
  library(tidyverse)
  dsn <- sprintf("%s/FB/51_1573033855.shp", wetlandsDir);
  nc <- st_read(dsn, stringsAsFactors=FALSE) %>%
#    filter(grepl('\\/35', NAT_ID)) %>%
    glimpse()
  return(invisible(nc))
}
# rm(list=ls()); source("geo/scripts/wetlands.R");polygonesfb_diff()
polygonesfb_diff <- function() {
  carp()
  library(sf)
  library(tidyverse)
  shp.sf <- polygonesfb_shp_lire()
  bbox.sf <- polygonesfb_bbox_lire()
  shp.df <- st_drop_geometry(shp.sf)
  bbox.df <- st_drop_geometry(bbox.sf) %>%
    mutate(id=as.integer(id))
  df <- full_join(bbox.df, shp.df, by=c('id'='ID')) %>%
    glimpse()
  df %>%
    filter(is.na(NAT_ID)) %>%
    glimpse()
  df %>%
    filter(is.na(commune)) %>%
    glimpse()
}
# rm(list=ls()); source("geo/scripts/wetlands.R");polygonesfb_liste()
polygonesfb_liste <- function() {
  carp()
  library(sf)
  library(tidyverse)
  df <- polygonesfb_shp_lire() %>%
    st_drop_geometry() %>%
    dplyr::select(NAME, NAT_ID) %>%
    mutate(toto=NAT_ID) %>%
    separate(toto, into=c('fonct_nom', 'fonct_code', 'elem_nom'), sep='/') %>%
    glimpse()
  View(df)
  export_df2xlsx(df)
}
#
# interrogation par carré Lambert 93 5x5km
# rm(list=ls()); source("geo/scripts/wetlands.R");biolo63_bbox_get()
biolo63_bbox_get <- function(force = FALSE) {
  carp()
  library(sf)
  dsn <- sprintf("%s/bzh_l93.kml", bioloDir);
  dsn <- sprintf("%s/dpt35_l93_5000.geojson", bioloDir);
  nc <- st_read(dsn, stringsAsFactors=FALSE) %>%
#    filter(Name == 'E032N678') %>%
    glimpse()
  format <- 'https://www.faune-bretagne.org/index.php?m_id=63&backlink=skip&content=bbox&garden=1&query=%s,%s,%s,%s&id_place=-1'
  for(i in 1:nrow(nc)) {
    carp("i: %s nrow: %s", i, nrow(nc))
    query <- nc[[i, 'Name']]
    bbox <-  as.vector(st_bbox(nc[i, ]))
    url <- sprintf(format, bbox[1],  bbox[2], bbox[3], bbox[4]) %>%
      glimpse()
    writeLines(url)
    stop('***')
    dsn <- sprintf("%s/biolo63_bbox_%s.xml", biolo63Dir, query);
    if( ! file.exists(dsn) | force == TRUE) {
      biolo_get(url, dsn)
    }
  }
}
# rm(list=ls()); source("geo/scripts/wetlands.R");biolo63_bbox_parse()
biolo63_bbox_parse <- function() {
  library(tidyverse)
  biolo_fb()
  carp("biolo63Dir: %s", biolo63Dir)
  files <- list.files(biolo63Dir, pattern = 'biolo63_bbox_.*.xml$', full.names = TRUE, ignore.case = TRUE)
  carp("nb files: %s", length(files))
  df <- biolo63_parse_files(files)
  dsn <- sprintf("%s/bbox.Rds", biolo63Dir)
  saveRDS(df, file=dsn)
}
#
# extraction des polygones du protocole WATERBIRD, d'Ille-et-Vilaine
# rm(list=ls()); source("geo/scripts/wetlands.R");biolo63_bbox_wetlands()
biolo63_bbox_wetlands <- function() {
  carp()
  library(tidyverse)
  library(sf)
  library(rio)
  dsn <- sprintf("%s/bbox.Rds", biolo63Dir)
  df <- readRDS(dsn) %>%
    filter(stoc_pattern == 'WATERBIRD') %>%
    filter(grepl('35', commune)) %>%
    distinct() %>%
    glimpse()
  nc <- st_as_sf(df, geometry=st_as_sfc(df$geometry), crs = 4326, remove=FALSE) %>%
    dplyr::select(name=textes, commune, id) %>%
    glimpse()
  plot(st_geometry(nc))
  dsn <- sprintf("%s/bbox_wetlands.geojson", biolo63Dir)
  st_write(nc, dsn, delete_dsn=TRUE, driver='GeoJSON')
  carp("dsn: %s", dsn)
  df <- st_drop_geometry(nc)
  dsn <- sprintf("%s/bbox_wetlands.xlsx", biolo63Dir)
  rio::export(df, dsn)
  carp("dsn: %s", dsn)
}
polygonesfb_bbox_lire <- function() {
  carp()
  library(sf)
  library(tidyverse)
  dsn <- sprintf("%s/bbox_wetlands.geojson", biolo63Dir)
  nc <- st_read(dsn, stringsAsFactors=FALSE) %>%
    glimpse()
  return(invisible(nc))
}
#
# liste des observations du protocole
# source("geo/scripts/wetlands.R");biolo63_bbox_wetlands_liste()
Biolo63_bbox_wetlands_liste <- function(dpt='35') {
  carp()
  carp("début")
  library(janitor)
  library(rio)
  library(tidyverse)
  dsn <- sprintf("%s/proto_%s.xlsx", varDir, dpt)
  df <- import(dsn)
  df <- df[-1,]
  df1 <- df %>%
    clean_names() %>%
    remove_empty(c("rows")) %>%
    glimpse()
  sites.df <- df1 %>%
    group_by(place, id_place, grid_name) %>%
    summarise(nb=n()) %>%
    glimpse()
  dsn <- sprintf("%s/bbox_wetlands.xlsx", biolo63Dir)
  bbox.df <- rio::import(dsn) %>%
    glimpse()
  df <- sites.df %>%
    left_join(bbox.df, by=c('place'='name')) %>%
    filter(is.na(id)) %>%
    glimpse()
  View(df)
}
# https://www.faune-bretagne.org/index.php?m_id=63&backlink=skip&content=mapcommune&query=a
# rm(list=ls()); source("geo/scripts/wetlands.R");biolo63_communes_get()
biolo63_communes_get <- function() {
  carp()
  format <- 'https://www.faune-bretagne.org/index.php?m_id=63&backlink=skip&content=mapcommune&query=%s'
  query <- 'a'
  for(q1 in letters) {
    for(q2 in letters) {
      query <- sprintf('%s%s', q1, q2)
      url <- sprintf(format, query)
      dsn <- sprintf("%s/biolo63_communes_%s.xml", biolo63Dir, query);
      if( ! file.exists(dsn) ) {
        biolo63_get(url, dsn)
      }
    }
  }
}
# rm(list=ls()); source("geo/scripts/wetlands.R");biolo63_communes_parse()
biolo63_communes_parse <- function() {
  carp()
  library(tidyverse)
  files <- list.files(biolo63Dir, pattern = 'biolo63_communes_\\w\\w\\.xml$', full.names = TRUE, ignore.case = TRUE)
  carp("nb files: %s", length(files))
  biolo63_parse_files(files)
}
# rm(list=ls()); source("geo/scripts/wetlands.R");biolo63_parse_files()
biolo63_parse_files <- function(files) {
  carp()
  library(xml2)
  library(tidyverse)
  for (i in 1:length(files) ) {
    dsn <- files[[i]]
    df2 <- biolo63_parse_file(dsn)
    if(exists('df1')) {
      df1[setdiff(names(df2), names(df1))] <- NA
      df2[setdiff(names(df1), names(df2))] <- NA
      df1 <- rbind(df1, df2)
#      df1 <- bind_rows(df1, df2)
    } else {
      df1 <- df2
    }
  }
  glimpse(df1)
  return(invisible(df1))
}
# rm(list=ls()); source("geo/scripts/wetlands.R");biolo63_parse_file('D:/bvi35/CouchesFB/63/biolo63_communes_ke.xml')
biolo63_parse_file <- function(dsn) {
  carp()
  library(xml2)
  library(tidyverse)
  carp("dsn: %s", dsn)
  doc <- read_xml(dsn, asText = TRUE, useInternal = TRUE, getDTD = FALSE)
  rows <- xml2::xml_find_all(doc, "//name")
  if(length(rows) < 1) {
    return()
  }
  if(length(rows) > 499) {
    carp('*** 499')
  }
  rows.df <- rows %>%
    map(xml_attrs) %>%
    map_df(~as.list(.))
  textes <- rows %>%
    map(xml_text) %>%
    unlist()
  rows.df <- cbind(rows.df, textes)
  return(invisible(rows.df))
}
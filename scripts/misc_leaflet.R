# <!-- coding: utf-8 -->
#
# la partie leaflet
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ==========================================================================
#
#
# source("geo/scripts/rapaces.R"); mga <- leaflet_point() %>% glimpse()
leaflet_point <- function(lon=-1.55036, lat=48.16081) {
  library(sf)
  library(leaflet)
  library(fontawesome)
  sfc <- st_sfc(st_point(c(lon,lat)))
  points.sf <- st_sf(a = 1, geom = sfc)
  st_crs(points.sf) <- 4326
  map <- leaflet_points(points.sf); print(map); return(invisible(map))
  cercle.sfg <- points.sf %>%
    st_transform(2154) %>%
    st_buffer(500) %>%
    st_transform(4326)
  url <- leaflet_tiles()
  leaflet() %>%
    addTiles(url) %>%
    addMarkers(
      data = points.sf
    )
#    %>%
#    addPolygons(
#      data = cercle.sfg
#    )
}
leaflet_tiles_planignv2 <- function() {
  query <- list(
    SERVICE="WMTS",
    REQUEST="GetTile",
    VERSION="1.0.0",
    STYLE="normal",
    TILEMATRIXSET="PM",
    FORMAT="image/png",
    LAYER="GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2",
    TILEMATRIX="{z}",
    TILEROW="{y}",
    TILECOL="{x}"
  )
  query <- paste(names(query), query, sep = "=", collapse = "&")
  url <- sprintf("https://data.geopf.fr/wmts?%s", query)
  return(invisible(url))
}
leaflet_tiles_photos2023 <- function() {
  query <- list(
    SERVICE="WMTS",
    REQUEST="GetTile",
    VERSION="1.0.0",
    STYLE="normal",
    TILEMATRIXSET="PM",
    FORMAT="image/jpeg",
    LAYER="ORTHOIMAGERY.ORTHOPHOTOS.ORTHO-EXPRESS.2023",
    TILEMATRIX="{z}",
    TILEROW="{y}",
    TILECOL="{x}"
  )
  query <- paste(names(query), query, sep = "=", collapse = "&")
  url <- sprintf("https://data.geopf.fr/wmts?%s", query)
  return(invisible(url))
}
#
# https://bookdown.org/nicohahn/making_maps_with_r5/docs/leaflet.html
#
leaflet_points <- function(point.sf) {
  library(sf)
  library(leaflet)
  library(htmltools)
  library(fontawesome)
  url <- leaflet_tiles()
#  names(nc1$geometry) <- NULL
#  print(head(nc1))
  map <- leaflet() %>%
    addTiles(leaflet_tiles_photos2023(), group = "photos2023") %>%
    addTiles(leaflet_tiles_planignv2(), group = "planignv2") %>%
    addLayersControl(
      baseGroups = c(
        "photos2023",
        "planignv2"
      ),
      position = "topright"
    ) %>%
    addMarkers(
      data = point.sf,
      popup = ~htmlEscape(name)
    )
  return(invisible(map))
}
#
# source("geo/scripts/cheveche35.R"); leaflet_polygons()
leaflet_polygons <- function(nc1) {
  library(sf)
  library(leaflet)
  library(fontawesome)
  nc1 <- nc1 %>%
    st_transform(4326) %>%
    st_cast("POLYGON") %>%
    dplyr::select(name) %>%
    glimpse()
  query <- list(
    SERVICE="WMTS",
    REQUEST="GetTile",
    VERSION="1.0.0",
    STYLE="normal",
    TILEMATRIXSET="PM",
    FORMAT="image/png",
    LAYER="GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2",
    TILEMATRIX="{z}",
    TILEROW="{y}",
    TILECOL="{x}"
  )
  query <- paste(names(query), query, sep = "=", collapse = "&")
  url <- sprintf("https://data.geopf.fr/wmts?%s", query)
#  names(nc1$geometry) <- NULL
  print(head(nc1))
  leaflet() %>%
    addTiles(url) %>%
    addPolygons(
      data = nc1
    )
  carp("fin")
}
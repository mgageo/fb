# <!-- coding: utf-8 -->
#
# la partie cartes
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# la totale
# source("geo/scripts/wetlands.R");cartes_jour();
cartes_jour <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  polygones_sites()
  couches_ogc_ecrire()
  cartes_sites_tex()
  cartes_sites_cartes()
}
#
# les communes des parcours
# source("geo/scripts/wetlands.R");cartes_communes();
cartes_communes <- function() {
  carp()
  library(tidyverse)
  library(janitor)
  commune.sf <- fonds_commune_lire_sf() %>%
    glimpse() %>%
    dplyr::select(NOM_COM, INSEE_COM)
  nc <- couches_sites_lire()
  nc$id <- 1:nrow(nc)
  carp('nom en double')
  df <- nc %>%
    st_set_geometry(NULL)
  df %>%
    get_dupes(name) %>%
    print(n = 20)
  carp("determination des communes")
  nc1 <- st_join(nc, commune.sf, join = st_intersects) %>%
    glimpse() %>%
    print(n=10)
  carp('regroupement communes')
  df1 <- nc1 %>%
    st_set_geometry(NULL)
  df2 <- df1 %>%
    group_by(id) %>%
    summarize(communes = paste0(NOM_COM, collapse = ", ")) %>%
    glimpse()
  carp('ajout communes')
  df3 <- left_join(df, df2)
  carp('nom en double')
  df3 %>%
    get_dupes(name) %>%
    print(n=20)
  return(invisible(nc))
}
#
# les cartes pour l'ensemble des sites, la version Tex
# source("geo/scripts/wetlands.R"); cartes_sites_tex()
cartes_sites_tex <- function(test=1) {
  carp()
  library(tidyverse)
  library(utf8)
  library(janitor)
  texFic <- sprintf("%s/%s", texDir, "cartes_sites.tex")
  TEX <- file(texFic)
  tex <- "% <!-- coding: utf-8 -->"
  cartesDir <- sprintf("%s/cartes", texDir)
  dir.create(cartesDir, showWarnings = FALSE)
# http://www.tablesgenerator.com/
#
# le template tex
  dsn <- sprintf("%s/cartes_sites_tpl.tex", texDir)
  template <- readLines(dsn)
  sites.sf <- couches_sites_lire()
  sites.df <- st_set_geometry(sites.sf, NULL)
  sites.df$name <- as.character(sites.df$name)
  Encoding(sites.df$name) <- 'UTF-8'
#  Encoding(sites.df$commune) <- 'latin1'

#  stop("***")
#  test <- 2
  for ( i in 1:nrow(sites.sf) ) {
    if ( i > 4 ) {
#      break
    }
    sf1 <- sites.sf[i,]
    sites <- sites.df[i, 'name']
    sitesDir <- sprintf("%s/sites/%s", texDir, sites)
    dir.create(sitesDir, showWarnings = FALSE, recursive = TRUE)
    tpl <- template
    tpl <- tex_df2tpl(sites.df, i, tpl)
    tex <- append(tex, tpl)
  }
  write(tex, file = TEX, append = FALSE)
  carp(" texFic: %s", texFic)
}
#
# génération des différentes cartes
# source("geo/scripts/wetlands.R"); cartes_wetlands_cartes()
cartes_wetlands_cartes <- function() {
  carp()
  library(tidyverse)
  library(sf)
  dsn <- sprintf("%s/bbox_wetlands.geojson", biolo63Dir)
  nc <- st_read(dsn) %>%
    st_transform(2154)
  couches_ogc_ecrire(nc)
  cartesDir <<- sprintf("%s/wetlands", texDir)
  dir.create(cartesDir, showWarnings = FALSE)
  for ( i in 4:nrow(nc) ) {
    if ( i > 4 ) {
#      break
    }
    nc1 <- nc[i,]
    cartes_site_cartes(nc1, 'satellite', lwd=5)
  }
}
# source("geo/scripts/wetlands.R"); cartes_sites_cartes()
cartes_sites_cartes <- function() {
  carp()
  library(tidyverse)
  sites.sf <- couches_sites_lire()
  sites.df <- st_set_geometry(sites.sf, NULL)
  cartesDir <- sprintf("%s/cartes", texDir)
  dir.create(cartesDir, showWarnings = FALSE)
  for ( i in 1:nrow(sites.sf) ) {
    if ( i > 4 ) {
#      break
    }
    sf1 <- sites.sf[i,]
    cartes_site_cartes(sf1, 'sc1000', lwd=20)
    cartes_site_cartes(sf1, 'carte', lwd=3)
    cartes_site_cartes(sf1, 'openstreetmap', lwd=8)
  }
}
cartes_site_cartes <- function(nc, couche='sc1000', lwd=3, test=2) {
  library(raster)
  dsn <- sprintf('%s/%s_%s.pdf', cartesDir, nc$id[[1]], couche)
  if (file.exists(dsn)) {
    return()
  }
  carp('dsn: %s', dsn)
  rasterFic <- sprintf('%s/ogc/%s_%s.tif', varDir, nc$id[[1]], couche)
  carp("rasterFic: %s", rasterFic)
  img <- brick(rasterFic)
#  img1 <- projectRaster(img, crs=CRS("+init=epsg:2154"))
  plotImg(img)
  plot(st_geometry(nc), lwd=lwd, border='darkred', add=TRUE)
  dev.copy(pdf, dsn, width=par("din")[1], height=par("din")[2])
  graphics.off()
}
#
# génération des différentes cartes pour la bmsm
# source("geo/scripts/wetlands.R"); cartes_bmsm()
cartes_bmsm <- function(force = TRUE) {
  carp()
  library(tidyverse)
  library(sf)
  dsn <- sprintf("%s/MGA/BMSM_vanneaux.shp", varDir)
  dsn <- sprintf("%s/BMSM_vanneaux.geojson", varDir)
  nc <- st_read(dsn) %>%
    st_transform(2154) %>%
    glimpse()
  ogcDir <<- sprintf("%s/ogc", varDir)
  dir.create(ogcDir, showWarnings = FALSE)
  fonds.df <- cartes_ogc_fonds_bmsm()
  for (i in 1:nrow(nc)) {
    site <- nc[[i, "secteur"]]
    carp("i: %s site: %s", i,  site)
    fonds_ogc_fonds(fonds.df, nc[i,], site, force = force)
#    break
  }
}
#
# la configuration des cartes
cartes_ogc_fonds_bmsm <- function() {
  df <- read.table(text="fonction|pdf|fond|param|marge|size_max
read|img|geo_photos_hr|%s/ogc/%s_%s.tif|250|4096
read|img|geo_plan|%s/ogc/%s_%s.tif|250|4096
read|img|geo_planj1|%s/ogc/%s_%s.tif|250|4096
", header = TRUE, sep = "|", blank.lines.skip = TRUE, stringsAsFactors = FALSE, quote = "") %>%
    filter(!grepl("^#", fonction))
  return(invisible(df))
}

# <!-- coding: utf-8 -->
#
# la partie données faune-bretagne
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d"Utilisation Commerciale - Partage des Conditions Initiales à l"Identique 2.0 France
# ===============================================================
#
# les zones d'observation
#
# https://cdnfiles1.biolovision.net/www.faune-paca.org/userfiles/FPPfevrier2012n11-2.pdf
#
# source("geo/scripts/fb.R"); covid_jour()
covid_jour <- function(force = TRUE) {
  carp()
  library(tidyverse)
  covid_faune_export(force = force)
  covid_faune_zones()
}
#
# source("geo/scripts/fb.R");covid_faune_export()
covid_faune_export <- function(force = TRUE) {
  carp()
  library(tidyverse)
  carp("début")
  dsn <- sprintf("%s/covid_faune_export.xlsx", fauneDir)
  biolo_export_region(dsn = dsn, force = force, debut = "06.04.2021")
  carp("dsn: %s", dsn)
}
#
# source("geo/scripts/fb.R");covid_faune_lire()
covid_faune_lire <- function(force = TRUE) {
  carp()
  library(tidyverse)
  dsn <- sprintf("%s/covid_faune_export.xlsx", fauneDir)
  carp("dsn: %s", dsn)
  nc <- faune_export_lire_sf(dsn, force = TRUE)
  return(invisible(nc))
}
#
# source("geo/scripts/fb.R");covid_faune_zones()
covid_faune_zones <- function(force = FALSE) {
  carp()
  library(tidyverse)
  library(sf)
  nc <- covid_faune_lire(force = force) %>%
    st_transform(2154) %>%
    st_buffer(100)
  nc1 <- nc %>%
    group_by(observateur) %>%
    summarize(nb = n()) %>%
    st_convex_hull()
  nc2 <- st_cast(nc1, "LINESTRING")
#  stop("***")
  nc3 <- nc2 %>%
    mutate(perimetre=as.numeric(round(st_length(.), 0))) %>%
    filter(perimetre > 40000)
  plot(st_geometry(nc3))
  text(st_coordinates(st_centroid(nc3)), labels = nc3$observateur, cex = 0.8, font = 2, col = "black")
  df3 <- nc3 %>%
    st_drop_geometry()
  print(knitr::kable(df3, format = "pipe"))
}

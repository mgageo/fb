# <!-- coding: utf-8 -->
#
# la partie données biolovision faune-bretagne
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# la partie base de données Postgresql
# une base par groupe d'espèces
#
#
# source("geo/scripts/fb.R");psql_jour()
psql_jour <- function() {
  psql_init()
  psql_extensions_init()
  psql_fb_init()
}
#
psql_stat <- function() {
  psql_connect(user = "visio_fb")
  psql_tables_liste(schema = "xlsx")
}
# source("geo/scripts/fb.R");psql_init()
psql_init <- function() {
  psql_connect(user = "visio_root")
  psql <- "
DROP DATABASE IF EXISTS fb;
DROP USER IF EXISTS fb;
CREATE DATABASE fb ENCODING='UTF8';
CREATE USER fb WITH ENCRYPTED PASSWORD 'fb2021';
GRANT ALL PRIVILEGES ON DATABASE fb TO fb;
ALTER ROLE fb WITH SUPERUSER;
"
  res <- db_SendQueries(db_con, psql)
}
# source("geo/scripts/fb.R");psql_fb_init()
psql_fb_init <- function() {
  psql_connect(user = "visio_fb")
  psql <- "
CREATE SCHEMA IF NOT EXISTS api;
CREATE SCHEMA IF NOT EXISTS import;
CREATE SCHEMA IF NOT EXISTS json;
CREATE SCHEMA IF NOT EXISTS xlsx;
CREATE TABLE IF NOT EXISTS import.files (dsn VARCHAR, size VARCHAR, ctime VARCHAR);
TRUNCATE TABLE import.files;
"
  res <- db_SendStatements(db_con, psql)
}
#
# import des fichiers de données
# source("geo/scripts/fb.R");psql_import_donnees(groupe = "chauves_souris", periode = "annee", Format = 'JSON', DCa = 1, tg = 2)
psql_import_donnees <- function(groupe = "reptiles", periode = "annee", Format = 'XLSX', DCa = 1, tg = 6) {
  library(tidyverse)
  library(lubridate)
  carp()
  format <- tolower(Format)
  path <- donnees_dir(periode = periode, DCa = DCa, tg = tg)
  files <- list.files(path, pattern = sprintf('\\.%s$', format) , full.names = TRUE, ignore.case = TRUE) %>%
    glimpse()
  if (length(files) < 1) {
    stop("****")
  }
# une période (année/mois) peut comporter plusieurs fichiers
# il faut choisir celui avec la date d'extraction la plus récente
# find("extract")
  files.df <- data.frame(files) %>%
    tidyr::extract(
      "files",
      c("periode", "from", "to"),
      regex = "/([\\d_]+)\\s+(\\S+)\\s+([\\d\\.]+)\\.",
      remove = FALSE,
      convert = FALSE
    ) %>%
    mutate(to_date = dmy(to))
  if (periode == "annee") {
    files.df <- files.df %>%
    mutate(start_date = sprintf("%s0101", periode)) %>%
    mutate(end_date = ceiling_date(ymd(start_date), 'year') - 1) %>%
    mutate(end_date = format(end_date, "%d.%m.%Y"))
  }
  if (periode == "mois") {
    files.df <- files.df %>%
    mutate(start_date = sprintf("%s_01", periode)) %>%
    mutate(end_date = ceiling_date(ym(start_date), 'month') - 1) %>%
    mutate(end_date = format(end_date, "%d.%m.%Y"))
  }
  files.df <- files.df %>%
    group_by(periode) %>%
    arrange(periode, to_date) %>%
    filter(row_number() == n()) %>%
    ungroup() %>%
    glimpse()
  files <- pull(files.df, files)
#  stop("******")
  psql_connect(user = "visio_fb")
  import.df <- dbReadTable(db_con, SQL("import.files")) %>%
    glimpse()
#  stop("****")
  psql <- "
DROP TABLE IF EXISTS %s.%s;
"
  psql <- sprintf(psql, format, groupe)
#  res <- db_SendStatement(db_con, psql)
  if (format == "json") {
    psql <- "
CREATE TABLE %s.%s(data jsonb);
"
    psql <- sprintf(psql, format, groupe)
    res <- db_SendStatement(db_con, psql)
  }
  for (i in 1:nrow(files.df) ) {
    dsn <- files.df[[i, "files"]]
    carp("dsn: %s", dsn)
# le fichier a-t-il déjà été importé ?
# comparaison nom, date de modif, taille
    finf <- file.info(dsn, extra_cols = FALSE)
    finf.df <- tibble(dsn = !!dsn, size = as.character(finf[, "size"]), ctime = as.character(finf[, "ctime"]))
    inter.df <- dplyr::intersect(import.df, finf.df)
    if ( nrow(inter.df) > 0 ) {
      next
    }
    psql_import(dsn, groupe, format, files.df[[i, "periode"]])
    import.df <- rbind(import.df, finf.df)
  }
  glimpse(import.df)
  dbWriteTable(db_con, SQL("import.files"), import.df, row.names = FALSE, append = FALSE, overwrite = TRUE)
}
#
# lecture d'un fichier de données et ajout à la base
# https://db.rstudio.com/best-practices/schema/
psql_import <- function(file, groupe, format, periode) {
  library(janitor)
  library(rio)
  library(tidyverse)
  library(DBI)
  carp( "file: %s", file)
  table <- sprintf("%s.%s", format, groupe)
  if (format == "xlsx") {
    df <- rio::import(file)
    df <- df[-1,] %>%
      clean_names() %>%
      remove_empty(c("rows")) %>%
      mutate(periode = !!periode) %>%
      glimpse()
    if ( nrow(df) < 1 ) {
      return
    }
    psql <- "
DELETE FROM %s.%s WHERE periode = '%s';
"
    psql <- sprintf(psql, format, groupe, periode)
    res <- db_SendQueries(db_con, psql)
    dbWriteTable(db_con, SQL(table), df, row.names = FALSE, append = TRUE)
  }
# import en format json
# psql -U fb "CREATE TABLE temp (data jsonb);"
# cat /tmp/fb.json | psql -U fb -c "COPY temp (data) FROM STDIN;"
# psql -U fb -c "copy temp from '/tmp/fb.json';"
# pb avec le format visionature et l'encodage \u123
# pas mieux après conversion
#
# https://www.postgresqltutorial.com/postgresql-json/
  if (format == "json") {
    dest <- sprintf("%s/fb.json", dockerDir)
    json_data <- read_json(path = file, simplifyVector = TRUE)
    json_txt <- toJSON(json_data)
    write(json_txt, dest)
    psql <- "
drop table if exists temp_json;
create table temp_json (values text);
copy temp_json  '/tmp/fb.json';
"
#    psql <- sprintf(psql, table)
    res <- db_SendQueries(db_con, psql)
    stop("*****")
  }
}
#
# source("geo/scripts/fb.R"); dtab <- psql_stat_groupe_periode()
psql_stat_groupe_periode <- function(groupe = "oiseaux", format = "xlsx") {
  library(tidyverse)
  library(DBI)
  table <- sprintf("%s.%s", format, groupe)
    psql <- "
SELECT date_year, COUNT(*) FROM %s GROUP BY date_year;
"
  psql <- sprintf(psql, table)
  dtab <- dbGetQuery(db_con, psql)
  return(invisible(dtab))
}
#
# source("geo/scripts/fb.R"); dtab <- psql_groupe_lire()
psql_groupe_lire <- function(groupe = "oiseaux", format = "xlsx") {
  library(tidyverse)
  library(DBI)
  table <- sprintf("%s.%s", format, groupe)
    psql <- "
SELECT * FROM %s;
"
  psql <- sprintf(psql, table)
  dtab <- dbGetQuery(db_con, psql)
  return(invisible(dtab))
}
#
# source("geo/scripts/fb.R"); dtab <- psql_doublon_groupe()
psql_doublon_groupe <- function(groupe = "oiseaux", format = "xlsx") {
  library(tidyverse)
  library(DBI)
  table <- sprintf("%s.%s", format, groupe)
    psql <- "
SELECT a.*
FROM %s a
JOIN (SELECT id_sighting, COUNT(*)
FROM %s
GROUP BY id_sighting
HAVING count(*) > 1 ) b
ON a.id_sighting = b.id_sighting
ORDER BY a.id_sighting
"
  psql <- sprintf(psql, table, table)
  dtab <- dbGetQuery(db_con, psql) %>%
    glimpse()
}

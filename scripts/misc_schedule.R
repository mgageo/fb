# <!-- coding: utf-8 -->
#
# quelques fonctions pour lancer des traitements type crontab / scheduler
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
#
## les traitements journaliers
#
#
# mise en tâche programmée
# source("geo/scripts/rapaces.R");atlas_schedule()
# schtasks.exe /query /fo LIST /v /TN rapaces
misc_schedule <- function(script = "rapaces", arg = "atlas_jour()") {
  library(taskscheduleR)
  rscript <- sprintf("d:/web/geo/scripts/%s.R", script)
  runon <- format(Sys.time() + 62, "%H:%M")
  carp("runon: %s", runon)
  taskscheduler_delete(
    taskname = script
  )
  taskscheduler_create(
    taskname = script,
    rscript = rscript,
    schedule = "ONCE",
    rscript_args = arg,
    starttime = runon
  )
}
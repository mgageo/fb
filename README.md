# fb : Faune-Bretagne

Scripts en environnement Windows 11 : MinGW R Texlive

Ces scripts exploitent des données en provenance :
- de Faune-Bretagne;
- de GéoBretagne et l'IGN pour la cartographie;
- de la LPO pour les protocoles EPOC-ODF/STOC/WATERBIRD

## Scripts R
Ces scripts sont dans le dossier "scripts".
Les points d'entrée sont :
- apivn : pour la base de données PostgreSQL issue de faune-bretagne
- atlas.R : pour les atlas depuis 1970
- bmsm_rlc : pour le comptage rlc en bmsm
- cheveche35.R : pour mes données de prospection chevêche
- circus35.R : pour le groupe FALCO
- clochouette35.R : pour le groupe clochouette35 de la LPO
- epoc.R : pour le format EPOC dont EPOC-ODF
- fb.R : divers traitements
- fbzh.R : pour le site miroir
- geoca.R : pour les colonies d'hirondelles
- gci35.R : pour les gravelots à collier interrompu
- ign : les données de l'ign
- maree.R : les horaires/hauteurs/coefficients des marées
- meteo.R : les données météo, météofrance, infoclimat ...
- odf.R : pour le site atlas oiseauxdefrance.org
- oncf.R : pour le protocole oncf dans faune-france
- rapaces.R : pour le protocole rapaces de la lpo
- rpg.R : pour les données de la Politique Agricole Commune
- stoc.R : pour le protocole STOC
- visionature.R : divers traitements
- wetlands.R : pour le protocole WATERBIRD

## Tex
Le langage Tex est utilisé pour la production des documents.

Texlive est l'environnement utilisé.


